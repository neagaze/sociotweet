package com.github.mikephil.charting.utils;

import com.github.mikephil.charting.data.Entry;

import java.text.DecimalFormat;

/**
 * Created by ekbana on 7/8/15.
 */
public class DegreeFormatter implements ValueFormatter{

    protected DecimalFormat mFormat;

    public DegreeFormatter() {
        mFormat = new DecimalFormat("###,###,##0.0");
    }

    @Override
    public String getFormattedValue(float value) {
        return mFormat.format(value) + ""+(char)0x00B0+" C";
    }

    @Override
    public String getFormattedValue(Entry entry) {
        return entry.getData().toString();
    }
}
