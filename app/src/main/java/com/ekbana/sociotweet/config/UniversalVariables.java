package com.ekbana.sociotweet.config;

/**
 * Created by nigesh on 6/2/15.
 */
public interface UniversalVariables {

    public static String TWEETS_API = "http://uat.ekbana.info/social/api_gettweets.php"; //  ?handle=aakarpost
    public static String WEATHER_LISTS_API = "http://uat.ekbana.info/social/api_weatherlocation.php";
    public static String WEATHER_API = "http://uat.ekbana.info/social/api_getweather.php";  // ?id=13
    public static String PINNED_USERS_API = "http://uat.ekbana.info/social/api_getpinnedusers.php";
    public static String PINNED_TWEETS_API = "http://uat.ekbana.info/social/api_getpinned_tweet.php"; // ?handle=aakarpost
    public static final String OTHER_APPS_URI = "http://flightstatsnepal.com/api_ekbana_apps.php";

    /**developer account key for this app*/
    public final static String TWIT_KEY = "ZtLTZgyYO7cs5TCDno5gwBMvy";
    /**developer secret for the app*/
    public final static String TWIT_SECRET = "cTsejzqriVAUv3fJoKX0xwBhiTYOEobFKVc9ZhcYGoFkjmHgyY";
    /**app url*/
    public final static String TWIT_URL = "sociotweet-android:///";
    public final static String PINNED_TWIT_URL = "sociotweet2-android:///";


    public static String WEATHER_PREFERENCES = "api_weatherlocation";
    public static String PINNED_TWEETS_PREFERENCES = "api_getpinnedusers";

    public static int LATEST_TWEET_ID = 0;
    public static int PINNED_TWEET_ID = 1;

    public static String[] defPinnedTweetHandles = {"graphnepal"};
    public static String[] defTweetHandles = {"BBCNews"};

}
