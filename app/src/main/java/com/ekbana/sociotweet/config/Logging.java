package com.ekbana.sociotweet.config;


/**
 * Created by nigesh on 7/9/15.
 */
public interface Logging {
    public static String LOG_XAxisRenderer = "XAxisRenderer";
    public static String LOG_TwitterFragment = "TwitterFragment";
    public static String LOG_TwitterListsFragment = "TwitterListsFragment";
    public static String LOG_DashboardActivity = "DashboardActivity";
    public static String LOG_ParseJSON = "ParseJSON";
    public static String LOG_WeatherConfigurationActivity = "WeatherConfigurationActivity";

    public static boolean DashboardLogging = true;
    public static boolean MPChartLibLogging = false;
    public static boolean TwitterListsFragmentLogging = false;
    public static boolean TwitterFragmentLogging = true;
    public static boolean ParseJSONLogging = false;
    public static boolean WeatherConfigActivityLogging = true;
    public static boolean EndlessRecylerOnScrollListenerLogging = false;
}
