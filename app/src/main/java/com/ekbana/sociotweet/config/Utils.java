package com.ekbana.sociotweet.config;

/**
 * Created by nigesh on 7/10/15.
 */
public abstract class Utils {

    /**
     * For checking if the pinned tweets are default
     * @param handle
     * @return
     */
    public static boolean checkDefPinnedTweetHandle(String handle){
        boolean isDefaultPinnedhandle = false;
        for(String defPinnedTweetHandle : UniversalVariables.defPinnedTweetHandles)
            if(defPinnedTweetHandle.equals(handle))
                isDefaultPinnedhandle = true;

        return isDefaultPinnedhandle;
    }


    /**
     * For checking if the tweets handles are by default
     * @param handle
     * @return
     */
    public static boolean checkDefTweetHandle(String handle){
        boolean isDefaultTweetHandle = false;
        for(String defTweetHandle : UniversalVariables.defTweetHandles)
            if(defTweetHandle.equals(handle))
                isDefaultTweetHandle = true;

        return isDefaultTweetHandle;
    }
}
