package com.ekbana.sociotweet.customui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ekbana.sociotweet.app.R;

/**
 * Created by nigesh on 6/7/15.
 */
public class BottomTabLinearLayout extends LinearLayout implements View.OnClickListener{

    private Context context;

    private LinearLayout dashboardLinearLayout;
    private LinearLayout weatherLinearLayout;
    private LinearLayout tweetLinearLayout;
    private LinearLayout pinnedTweetsLinearLayout;

    private static String LOG_TEXT = "BottomTabLinearLayout -> ";

    private ImageView dasboardImageView, weatherImageView, tweetImageView, pinnedTweetImageView;
    private TextView dasboardTextView, weatherTextView, tweetTextView, pinnedTweetTextView;

    public BottomTabLinearLayout(Context context) {
        super(context);
        this.context = context;
        //    customFunctions();
    }

    public BottomTabLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        //    customFunctions();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //    customFunctions();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    //    Log.e(""+LOG_TEXT+" onMeasure","onMeasure()");
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    //    Log.e("" + LOG_TEXT + " onLayout", "onLayout()");
        customFunctions();
    }


    /**
     * for working with custom views
     */
    private void customFunctions() {
        if(dashboardLinearLayout == null) {
            dashboardLinearLayout = (LinearLayout) findViewById(R.id.dashboardLayout);
            weatherLinearLayout = (LinearLayout) findViewById(R.id.weatherLayout);
            tweetLinearLayout = (LinearLayout) findViewById(R.id.tweetLayout);
            pinnedTweetsLinearLayout = (LinearLayout) findViewById(R.id.pinnedTweetLayout);

            if (dashboardLinearLayout == null)
                Log.e("" + LOG_TEXT, "dashboardLinearLayout is null");

            dasboardImageView = (ImageView) findViewById(R.id.dashboardImageView);
            weatherImageView = (ImageView) findViewById(R.id.weatherImageView);
            tweetImageView = (ImageView) findViewById(R.id.tweetImageView);
            pinnedTweetImageView = (ImageView) findViewById(R.id.pinnedTweetImageView);

            dasboardTextView = (TextView) findViewById(R.id.dashboardTextView);
            weatherTextView = (TextView) findViewById(R.id.weatherTextView);
            tweetTextView = (TextView) findViewById(R.id.tweetTextView);
            pinnedTweetTextView = (TextView) findViewById(R.id.pinnedTweetTextView);

            Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;

            Log.e("" + LOG_TEXT + " customFunctions()", "width:" + width);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width / 4, LinearLayout.LayoutParams.WRAP_CONTENT);
            dashboardLinearLayout.setLayoutParams(lp);
            weatherLinearLayout.setLayoutParams(lp);
            tweetLinearLayout.setLayoutParams(lp);
            pinnedTweetsLinearLayout.setLayoutParams(lp);

            dashboardLinearLayout.setOnClickListener(this);
            weatherLinearLayout.setOnClickListener(this);
            tweetLinearLayout.setOnClickListener(this);
            pinnedTweetsLinearLayout.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        if(view.equals(dashboardLinearLayout)){
            dashboardLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            dasboardImageView.setImageResource(R.drawable.dashboard_red);
            dasboardTextView.setTextColor(getContext().getResources().getColor(R.color.red_color));

            weatherLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            weatherImageView.setImageResource(R.drawable.weather);
            weatherTextView.setTextColor(getContext().getResources().getColor(R.color.white));

            tweetLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            tweetImageView.setImageResource(R.drawable.twitter);
            tweetTextView.setTextColor(getContext().getResources().getColor(R.color.white));

            pinnedTweetsLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            pinnedTweetImageView.setImageResource(R.drawable.pinned);
            pinnedTweetTextView.setTextColor(getContext().getResources().getColor(R.color.white));

        } else if(view.equals(weatherLinearLayout)){

            dashboardLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            dasboardImageView.setImageResource(R.drawable.dashboard);
            dasboardTextView.setTextColor(getContext().getResources().getColor(R.color.white));

            weatherLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            weatherImageView.setImageResource(R.drawable.weather_red);
            weatherTextView.setTextColor(getContext().getResources().getColor(R.color.red_color));

            tweetLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            tweetImageView.setImageResource(R.drawable.twitter);
            tweetTextView.setTextColor(getContext().getResources().getColor(R.color.white));

            pinnedTweetsLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            pinnedTweetImageView.setImageResource(R.drawable.pinned);
            pinnedTweetTextView.setTextColor(getContext().getResources().getColor(R.color.white));

        } else if(view.equals(tweetLinearLayout)){

            dashboardLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            dasboardImageView.setImageResource(R.drawable.dashboard);
            dasboardTextView.setTextColor(getContext().getResources().getColor(R.color.white));

            weatherLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            weatherImageView.setImageResource(R.drawable.weather);
            weatherTextView.setTextColor(getContext().getResources().getColor(R.color.white));

            tweetLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            tweetImageView.setImageResource(R.drawable.twitter_red);
            tweetTextView.setTextColor(getContext().getResources().getColor(R.color.red_color));

            pinnedTweetsLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            pinnedTweetImageView.setImageResource(R.drawable.pinned);
            pinnedTweetTextView.setTextColor(getContext().getResources().getColor(R.color.white));
        } else if(view.equals(pinnedTweetsLinearLayout)){
            dashboardLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            dasboardImageView.setImageResource(R.drawable.dashboard);
            dasboardTextView.setTextColor(getContext().getResources().getColor(R.color.white));

            weatherLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            weatherImageView.setImageResource(R.drawable.weather);
            weatherTextView.setTextColor(getContext().getResources().getColor(R.color.white));

            tweetLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.gray6));
            tweetImageView.setImageResource(R.drawable.twitter);
            tweetTextView.setTextColor(getContext().getResources().getColor(R.color.white));

            pinnedTweetsLinearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            pinnedTweetImageView.setImageResource(R.drawable.pinned_red);
            pinnedTweetTextView.setTextColor(getContext().getResources().getColor(R.color.red_color));

        }
    }
}
