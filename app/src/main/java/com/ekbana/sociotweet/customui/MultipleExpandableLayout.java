package com.ekbana.sociotweet.customui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.*;
import com.astuetz.PagerSlidingTabStrip;
import com.ekbana.sociotweet.app.DashboardActivity;
import com.ekbana.sociotweet.app.R;
import com.ekbana.sociotweet.app.SocioTweet;
import com.ekbana.sociotweet.app.TwitterFragment;
import com.ekbana.sociotweet.config.UniversalVariables;

/**
 * Created by nigesh on 6/16/15.
 */
public class MultipleExpandableLayout extends LinearLayout {

    private Context context;
    private OnLayoutCallback onBackgroundDoCallback;

    private ExpandableLayout firstLinearLayout;
    private ExpandableLayout secondLinearLayout;

    public MultipleExpandableLayout(Context context) {
        super(context);
        this.context = context;
    //    init();
    }

    public MultipleExpandableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    //    init();
    }

    public MultipleExpandableLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    //    init();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        firstLinearLayout = (ExpandableLayout) inflater.inflate(R.layout.expandable_layout, null);  // new ExpandableLayout(context);
        secondLinearLayout = (ExpandableLayout) inflater.inflate(R.layout.expandable_layout, null); // new ExpandableLayout(context);


        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        firstLinearLayout.setLayoutParams(lp);
        secondLinearLayout.setLayoutParams(lp);

        addView(firstLinearLayout, 0);
        addView(secondLinearLayout, 1);

    }

    public ExpandableLayout getFirstLayout(){
        return firstLinearLayout;
    }

    public ExpandableLayout getSecondLayout(){
        return secondLinearLayout;
    }


    public static interface OnLayoutCallback{
        public void setFirstLayout();
        public void setSecondLayout();
    }

    public void setOnBackgroundListener(OnLayoutCallback onBackgroundDoCallback){
        this.onBackgroundDoCallback = onBackgroundDoCallback;
    }

    public OnLayoutCallback getLayoutCallback(){
        return onBackgroundDoCallback;
    }

}
