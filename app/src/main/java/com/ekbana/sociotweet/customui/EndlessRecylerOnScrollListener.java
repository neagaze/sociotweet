package com.ekbana.sociotweet.customui;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.ekbana.sociotweet.config.Logging;

/**
 * Created by nigesh on 7/2/15.
 */
public abstract class EndlessRecylerOnScrollListener extends RecyclerView.OnScrollListener {
    public static String TAG = EndlessRecylerOnScrollListener.class.getSimpleName();

    private int previousTotal = 0; // The total number of items in the dataset after the last load
    private boolean loading = true; // True if we are still waiting for the last set of data to load.
    private int visibleThreshold = 3; // The minimum amount of items to have below your current scroll position before loading more.
    int firstVisibleItem, visibleItemCount, totalItemCount;

    private int current_page = 1;
    private int loadingItemInstallments = 10;

    private LinearLayoutManager mLinearLayoutManager;

    public EndlessRecylerOnScrollListener(LinearLayoutManager linearLayoutManager) {
        this.mLinearLayoutManager = linearLayoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mLinearLayoutManager.getItemCount();
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if(Logging.EndlessRecylerOnScrollListenerLogging) {
            Log.e("" + TAG, "totalItemCount: " + totalItemCount + ", visibleItemCount: " + visibleItemCount);
            Log.e("" + TAG, "firstVisibleItem: " + firstVisibleItem + ", visibleThreshold: " + visibleThreshold);
        }
        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            // End has been reached

            // Do something
            current_page++;

            loadingItemInstallments += 11;
            onLoadMore(recyclerView, totalItemCount, loadingItemInstallments);

            if(Logging.EndlessRecylerOnScrollListenerLogging)
                Log.e(""+TAG,"installments: "+loadingItemInstallments);
            loading = true;
        }
    }

    public abstract void onLoadMore(RecyclerView recyclerView, int current_page, int installments);
}
