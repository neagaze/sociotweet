package com.ekbana.sociotweet.customui;

import android.content.Context;
import android.util.AttributeSet;
import com.ekbana.sociotweet.app.R;


/**
 * Created by neagaze on 2/25/15.
 *
 * This is the extension of the library file ExpandableLayoutItem.java of the library 'ExpandableLayout'.
 * It includes the addition of callback functionality for item click listener.
 *
 */
public class NewExpandableLayoutItem extends ExpandableLayoutItem{

    public NewExpandableLayoutItem(Context context) {
        super(context);
    }

    public NewExpandableLayoutItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NewExpandableLayoutItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void show() {
        // @neaGaze additions
        if(listItemClickFinishCallback != null)
            listItemClickFinishCallback.onListItemClickStart(this);

        this.setBackgroundColor(getResources().getColor(R.color.testColor1));
        super.show();
    }

    @Override
    public void hide() {
        super.hide();

        // @neaGaze additions
        if(listItemClickFinishCallback != null)
            listItemClickFinishCallback.onListItemClickFinish(this);

        this.setBackgroundColor(getResources().getColor(R.color.white));
    }

    /**
     * My modifications
     * ***/
    private ListItemClickFinishCallback listItemClickFinishCallback = null;

    public void setOnListItemClickFinishListener(ListItemClickFinishCallback listener){
        listItemClickFinishCallback = listener;
    }

    public static interface ListItemClickFinishCallback {
        /**
         * Called when an item in the list has been finished clicked
         */
        void onListItemClickFinish(NewExpandableLayoutItem item);
        void onListItemClickStart(NewExpandableLayoutItem item);
    }
}
