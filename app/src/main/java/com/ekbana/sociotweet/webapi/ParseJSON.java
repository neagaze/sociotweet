package com.ekbana.sociotweet.webapi;

import android.content.Context;
import android.util.Log;
import com.ekbana.sociotweet.app.SocioTweet;
import com.ekbana.sociotweet.config.Logging;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.config.Utils;
import com.ekbana.sociotweet.db.*;
import com.ekbana.sociotweet.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by nigesh on 6/2/15.
 */
public class ParseJSON {
    /*****************************************************************************
     * To check teh validity of JSONObject of JSONArray
     *
     * @param test
     * @return
     ****************************************************************************/
    public static boolean isJSONValid(String test) {
        if(test == null)
            return false;
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    /*****************************************************************************
     * To check the validity of JSONObject of JSONArray
     *
     * @param test
     * @return
     ******************************************************************************/
    public static boolean isValidationCorrect(String test) {
        if(test == null)
            return false;
        try {
            JSONObject json = new JSONObject(test);

            if(json.has("msg")){
                Log.e("parseJSON -> isValidationCorrect()", "msg: " + json.getString("msg"));
                return false;
            }

        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    /*******************************************************************************
     * parse weather
     *
     * @param jsonString
     *************************************************************************************/
    public static void parseWeather(String jsonString){
        Weather weather = null;
        try {
            JSONArray baseArr = new JSONArray(jsonString);
            int arrSize = baseArr.length();
            WeatherDBHelper weatherDBHelper = WeatherDBHelper.getInstance(SocioTweet.getContext());

            if(arrSize > 0) {
                weatherDBHelper.deleteWeather();
                ForecastDBHelper.getInstance(SocioTweet.getContext()).deleteForecast();
                ForecastHourlyDBHelper.getInstance(SocioTweet.getContext()).deleteForecastHourly();
            }

            for(int j = 0 ; j < arrSize; j++){
                JSONObject allTemp = baseArr.getJSONObject(j);

                weather = new Weather();
                JSONObject currentJSONObj = allTemp.getJSONObject("current");
                String location = currentJSONObj.getString("location");
                String temp = currentJSONObj.getString("temp");

                if(location == null || temp == null || location.equals("null") || temp.equals("null"))
                    throw new NullPointerException("value is null");

                Log.e("ParseJSON","location: "+location);

                weather.setLocation(location);
                weather.setTemp(temp);

                weather.setWeatherLocation(Integer.parseInt(allTemp.getString("id")));

                // for forcast element
                JSONArray forcastJSONArr = allTemp.getJSONArray("forcast");
                int forecastSize = forcastJSONArr.length();
                ArrayList<Forecast> forecasts = new ArrayList<Forecast>();
                for(int i = 0 ; i < forecastSize; i++){
                    JSONObject forecastJSON = forcastJSONArr.getJSONObject(i);
                    Forecast forecast = new Forecast();
                    forecast.setDay(forecastJSON.getString("day"));
                    forecast.setHigh(forecastJSON.getString("high"));
                    forecast.setLow(forecastJSON.getString("low"));
                    forecast.setCondition(forecastJSON.getString("condition"));
                    forecast.setIcon(forecastJSON.getString("icon"));

                    forecasts.add(forecast);
                }

                weather.setForecasts(forecasts);

                // for forcast element
                JSONArray forcastHourlyJSONArr = allTemp.getJSONArray("forcasthourly");
                int forecastHourlySize = forcastHourlyJSONArr.length();
                ArrayList<ForecastHourly> forecastsHourly = new ArrayList<ForecastHourly>();
                for(int i = 0 ; i < forecastHourlySize; i++){
                    JSONObject forecastHourlyJSON = forcastHourlyJSONArr.getJSONObject(i);
                    ForecastHourly forecastHourly = new ForecastHourly();
                    forecastHourly.setTime(forecastHourlyJSON.getString("time"));
                    forecastHourly.setTemperature(forecastHourlyJSON.getString("temperature"));
                    forecastHourly.setCondition_title(forecastHourlyJSON.getString("condition_title"));
                    forecastHourly.setCondition_icon(forecastHourlyJSON.getString("condition_icon"));
                    forecastHourly.setTemprature_feelslike(forecastHourlyJSON.getString("temprature_feelslike"));
                    forecastHourly.setPop(forecastHourlyJSON.getString("pop"));

                    forecastsHourly.add(forecastHourly);
                }

                weather.setForecastHourlies(forecastsHourly);

                // save in db
                WeatherDBHelper.getInstance(SocioTweet.getContext()).createEntry(weather);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("ParseJSON -> JSONException()",""+e.getMessage());
        }  catch (NullPointerException e) {
            e.printStackTrace();
            Log.e("ParseJSON -> NullPointerException()",""+e.getMessage());
        } finally {
        }
    }

    /**************************************************************************
     * parse tweets
     *
     * @param jsonString
     *************************************************************************/
    public static boolean parseTweets(String jsonString, boolean isPinnedTweet){
        boolean isValid = true;
        try {
            JSONArray baseArr = new JSONArray(jsonString);
            int arrSize = baseArr.length();
            TweetDBHelper tweetDBHelper = TweetDBHelper.getInstance(SocioTweet.getContext());
            TweetUsersDBHelper tweetUsersDBHelper = TweetUsersDBHelper.getInstance(SocioTweet.getContext());

            if(Logging.ParseJSONLogging) Log.e("ParseJSON","json format: "+jsonString);
            //    if(arrSize > 0)
            //        tweetDBHelper.deleteTweet();

            for(int j = 0 ; j < arrSize; j++){

                JSONObject allTemp = baseArr.getJSONObject(j);
                String handle = allTemp.getString("tweethandle");
                int handleId = tweetUsersDBHelper.getIdFromhandle(handle);
                //    if(handleId < 0)
                if(Logging.ParseJSONLogging) Log.e(Logging.LOG_ParseJSON, "allTemp:  "+allTemp.toString());
                String userProfilePic = allTemp.getString("user_profile_pic");
                String userDisplayName = allTemp.getString("user_display_name");

                if(userProfilePic == null || userDisplayName == null || userProfilePic.equals("null") || userDisplayName.equals("null"))
                    //    throw new NullPointerException("value is null");
                    continue;

                JSONArray tweetsJSONArr = allTemp.getJSONArray("tweets");

                int tweetCount = tweetsJSONArr.length();

                for(int i = 0; i < tweetCount; i++){
                    JSONObject tweetJSONObj = tweetsJSONArr.getJSONObject(i);

                    Tweet tweet = new Tweet();
                    tweet.setPinnedUserId(handleId);
                    tweet.setTweetId(tweetJSONObj.getString("tweetid"));
                    tweet.setDate(tweetJSONObj.getString("date"));
                    tweet.setTweet(tweetJSONObj.getString("tweet"));
                    tweet.setUser_profile_pic(userProfilePic);
                    tweet.setUser_display_name(userDisplayName);
                    tweet.setUser_handle(handle);
                    tweet.setPinnedUserId(handleId);
                    tweet.setPinned(isPinnedTweet);

                    // save in db
                    int tweetId = tweetDBHelper.checkTweetExists(tweet);
                    //  Log.e("ParseJSON","tweetId: "+tweetId);
                    if(tweetId < 0) {
                        tweetDBHelper.createEntry(tweet);
                    } else if(isPinnedTweet) {
                        //    Log.e("ParseJSON -> parseTweets()","is_pinned tweet is true");
                        tweetDBHelper.setKeyIsPinned(tweet, tweetId);
                    } else {}
                    //        Log.e("ParseJSON -> parseTweets()","tweet is present and is not pinned tweet so nothing done.");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            isValid = false;
            Log.e("ParseJSON -> JSONException()",""+e.getMessage());
        } catch (NullPointerException e) {
            e.printStackTrace();
            isValid = false;
            Log.e("ParseJSON -> NullPointerException()",""+e.getMessage());
        } finally {
            return isValid;
        }
    }

    /**************************************************************************
     * @param jsonString
     * @return
     *************************************************************************/
    public static boolean setWeatherPreferences(String jsonString, Context context) {

        //    Context context = SocioTweet.getContext();
        if(context == null)
            Log.e("App context is null","null");
        //    SharedPreferences.Editor editor = null;
        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            int len = jsonArray.length();
            for(int i = 0; i < len; i++){
                JSONObject obj = jsonArray.getJSONObject(i);

                if(obj.has("id") && obj.has("title")) {

                    WeatherLocation weatherLocation = new WeatherLocation();
                    weatherLocation.setId(Integer.parseInt(obj.getString("id")));
                    weatherLocation.setTitle(obj.getString("title"));
                    if(obj.getString("id").equals("12") || obj.getString("id").equals("13"))
                        weatherLocation.setSelected(true);
                    else
                        weatherLocation.setSelected(false);

                    WeatherLocationDBHelper weatherLocationDBHelper = WeatherLocationDBHelper.getInstance(context);
                    if(!weatherLocationDBHelper.isValueStored(Integer.parseInt(obj.getString("id"))))
                        weatherLocationDBHelper.createEntry(weatherLocation);

                } else if(obj.has("handle") && obj.has("displayname") && obj.has("profile_image")){

                    TweetUser tweetUser = new TweetUser();
                    tweetUser.setHandle(obj.getString("handle"));
                    tweetUser.setDisplayname(obj.getString("displayname"));
                    tweetUser.setProfile_image(obj.getString("profile_image"));

                    boolean isDefaultPinnedHandle = Utils.checkDefPinnedTweetHandle(obj.getString("handle"));
                    boolean isDefaultTweetHandle = Utils.checkDefTweetHandle(obj.getString("handle")) && isDefaultPinnedHandle;

                    //    if(/*obj.getString("handle").equals("gpokhrel")*/
                    //            isDefaultPinnedHandle)
                    //        tweetUser.setIsPinnedUser(false);
                    //    else
                    tweetUser.setIsPinnedUser(true);

                    if(/*obj.getString("handle").equals("graphnepal") || obj.getString("handle").equals("gpokhrel")*/
                        //     isDefaultTweetHandle || isDefaultPinnedHandle
                            Utils.checkDefTweetHandle(obj.getString("handle"))
                                    || Utils.checkDefPinnedTweetHandle(obj.getString("handle")))
                        tweetUser.setSelected(true);
                    else
                        tweetUser.setSelected(false);

                    TweetUsersDBHelper tweetUsersDBHelper = TweetUsersDBHelper.getInstance(context);
                    if(!tweetUsersDBHelper.isValueStored(obj.getString("handle")))
                        tweetUsersDBHelper.createEntry(tweetUser);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * get the new Tweet User
     * @param jsonString
     * @return
     */
    public static TweetUser setNewTweetUser(String jsonString){
        Context context = SocioTweet.getContext();
        TweetUser tweetUser = null;
        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            int len = jsonArray.length();
            for(int i = 0; i < len; i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                if(Logging.ParseJSONLogging) Log.e(""+Logging.LOG_ParseJSON,"new tweet handle: "+obj.toString());
                if(obj.has("tweethandle") && obj.has("user_profile_pic") && obj.has("user_display_name")){

                    tweetUser = new TweetUser();
                    tweetUser.setHandle(obj.getString("tweethandle"));
                    tweetUser.setDisplayname(obj.getString("user_display_name"));
                    tweetUser.setProfile_image(obj.getString("user_profile_pic"));
                    tweetUser.setSelected(true);
                    tweetUser.setIsPinnedUser(false);

                    if(tweetUser.getDisplayname() == null || tweetUser.getDisplayname().equals("null"))
                        return null;

                    TweetUsersDBHelper tweetUsersDBHelper = TweetUsersDBHelper.getInstance(context);
                    if(!tweetUsersDBHelper.isValueStored(obj.getString("tweethandle")))
                        tweetUsersDBHelper.createEntry(tweetUser);
                } else
                    return null;
                break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return tweetUser;
    }

    /**
     * For parsing the json resulting from "http://flightstatsnepal.com/api_ekbana_apps.php"
     *
     * @param jsonString
     * @return
     */
    public static ArrayList<OtherApps> getOtherApps(String jsonString){

        ArrayList<OtherApps> otherAppsList = null;

        if(jsonString != null) {
            try {
                JSONArray jsonArray = new JSONArray(jsonString);
                otherAppsList = new ArrayList<OtherApps>();

                for(int i = 0; i < jsonArray.length(); i++) {

                    JSONObject json = jsonArray.getJSONObject(i);
                    OtherApps otherApps = new OtherApps();

                    otherApps.setTitle(json.getString("title"));
                    otherApps.setDesc(json.getString("desc"));
                    otherApps.setImage(json.getString("image"));
                    otherApps.setUrl(json.getString("url"));

                    otherAppsList.add(otherApps);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("ParseJSON -> getOtherApps()", "JSONException: " + e.getMessage());
            }
        }
        return otherAppsList;
    }
}
