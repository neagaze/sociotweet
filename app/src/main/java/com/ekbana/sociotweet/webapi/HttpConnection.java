package com.ekbana.sociotweet.webapi;

/**
 * Created by neagaze on 2/18/15.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class HttpConnection {
    //  private String mainUrl = "http://flightstatsnepal.com/apiv0-0-1/";

    private HttpClient httpclient;
    private HttpPost httppost;
    private static HttpConnection httpConnection;
    private static int ConnCount = 0;
    private static int timeoutConnection = 15000;
    private static int timeoutSocket = 15000;

    public HttpConnection() {

        HttpParams httpParameters = new BasicHttpParams();
        // Set the timeout in milliseconds until a connection is established.
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnection);
        // Set the default socket timeout (SO_TIMEOUT)in milliseconds which is
        // the timeout for waiting for data.
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

        httpclient = new DefaultHttpClient(httpParameters);
    }

    public static HttpConnection getSingletonConn() {

        if (httpConnection == null)
            httpConnection = new HttpConnection();
        Log.v("Connection count:", "" + ConnCount++);
        return httpConnection;
    }

    /*****************************************************************************************
     * read json data from the given URL
     * ************************************************************************************/
    public String getJSONFromUrl(JSONObject jsonForm, String serviceName) {
        // initialize
        //    String url = new StringBuilder().append(mainUrl).append(serviceName)
        //            .toString();
        String url = serviceName;
        InputStream is = null;
        String result = "";

        try {
            httppost = new HttpPost(url);
            httppost.setHeader(HTTP.CONTENT_TYPE,
                    "application/json; charset=utf-8");
            String tempNetworkJson = jsonForm.toString();
            StringEntity se = new StringEntity(tempNetworkJson);
            httppost.setEntity(se);

            HttpResponse response = httpclient.execute(httppost);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                Log.d("Web Service available", "OK to Go");
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                Log.v("Successful Connection", ":D");
            } else {
                Log.e("Web service unavailable", "Go Local");
            }

        } catch (Exception e) {
            Log.e("Error in http connection:", "" + e.toString());
        }
        try {
            // convert response to string
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();

        } catch (Exception e) {
            Log.e("Error converting result ", "" + e.toString());
        }
        return result;

    }

    /**
     * To get the JSON using get method with params
     *
     * @param urlStr
     * @return
     */
    public String getJSONusingGET(String urlStr, String paramName, String paramValue){
        String uri = urlStr;
        List<NameValuePair> params = new LinkedList<NameValuePair>();
        params.add(new BasicNameValuePair(paramName, String.valueOf(paramValue)));
        uri +=  URLEncodedUtils.format(params, "utf-8");

        return getJSONusingGET(uri);
    }
    /**
     * To get the JSON using get method
     *
     * @param urlStr
     * @return
     */
    public String getJSONusingGET(String urlStr){
        URL url;
        BufferedReader br = null;
        String string = null;
        try {
            url = new URL(urlStr);

            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();

            InputStream in = urlConnection.getInputStream();

            InputStreamReader isw = new InputStreamReader(in);
            br = new BufferedReader(isw);
            StringBuilder sb = new StringBuilder();
            while((string = br.readLine()) != null){
                sb.append(string);
            }
            string = sb.toString();
        } catch (Exception e) {
            Log.e("HttpConnection -> getJSONusingGET()", "exception: " + e.getMessage());
            e.printStackTrace();
            string = null;
        }finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("HttpConnection -> getJSONusingGET()", "exception: " + e.getMessage());
                }
            }
        }
        return string;
    }

    /*****************************************************************************************
     * get Connection Availability
     * ************************************************************************************/
    public static boolean getConnectionAvailable(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isAvailable() && netInfo.isConnected()) {

            Log.e("Connection is good dude", ":D :D");
            return true;
        } else {
            if (netInfo == null)
                Log.e("Connection is null", "It's empty buddy :( :(");
            else if (!netInfo.isAvailable())
                Log.e("Connection is not Available",
                        "No Connection available :( :(");
            else
                Log.e("Connection is not Connected",
                        "No Connection buddy :( :(");
            return false;
        }
    }

}