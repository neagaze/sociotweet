package com.ekbana.sociotweet.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.ekbana.sociotweet.model.Forecast;
import com.ekbana.sociotweet.model.ForecastHourly;
import com.ekbana.sociotweet.model.Weather;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekbana on 6/4/15.
 */
public class ForecastDBHelper extends SQLiteOpenHelper{

    private Context context;
    private static ForecastDBHelper forecastDBHelper;

    // Table Names
    public static final String TABLE_FORECAST = "FORECAST";

    // Common column names
    public static final String KEY_ID = "id";
    public static final String KEY_DAY = "day";
    public static final String KEY_HIGH = "high";
    public static final String KEY_LOW = "low";
    public static final String KEY_CONDITION = "condition";
    public static final String KEY_ICON = "icon";
    public static final String KEY_WEATHER_ID = "weather_id";

    public static final String CREATE_FORECAST = "CREATE TABLE "
            + TABLE_FORECAST + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_DAY + " TEXT, "
            + KEY_HIGH + " TEXT, "
            + KEY_LOW + " TEXT, "
            + KEY_CONDITION + " TEXT, "
            + KEY_ICON + " TEXT, "
            + KEY_WEATHER_ID + " INTEGER, "
            + "FOREIGN KEY("+KEY_WEATHER_ID+") REFERENCES "+WeatherDBHelper.TABLE_WEATHER+"("+WeatherDBHelper.KEY_ID+") "
            + ") ";


    public ForecastDBHelper(Context context) {
        super(context, WeatherLocationDBHelper.DATABASE_NAME, null, WeatherLocationDBHelper.DATABASE_VERSION);
        this.context = context;
    }

    public static ForecastDBHelper getInstance(Context context){
        if(forecastDBHelper == null)
            forecastDBHelper = new ForecastDBHelper(context);

        return forecastDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_FORECAST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            switch (oldVersion) {
                case 1:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORECAST);
                    onCreate(db);
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORECAST);
                    onCreate(db);
                default:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORECAST);
                    onCreate(db);
            }
        }
    }

    /***
     * Return all entry of Forecast
     * ******/
    public List<Forecast> getAllEntry() {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_FORECAST;

        return genericGetEntry(selectQuery);
    }

    /********
     * Return all the single entry of Forecast
     * ******/
    public List<Forecast> getForecastFromId(int id) {

        String selectQuery = "SELECT  * FROM " + TABLE_FORECAST+" WHERE "+KEY_ID+"="+id;

        return genericGetEntry(selectQuery);
    }

    /********
     * Return all the single entry of Forecast
     * ******/
    public List<Forecast> getForecastFromWeatherId(int weatherId) {

        String selectQuery = "SELECT  * FROM " + TABLE_FORECAST+" WHERE "+KEY_WEATHER_ID+"="+weatherId;

        return genericGetEntry(selectQuery);
    }

    /**
     * create a forecast entry
     *
     * @param forecast
     */
    public long createEntry(Forecast forecast, long weatherId) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DAY, forecast.getDay());
        values.put(KEY_HIGH, forecast.getHigh());
        values.put(KEY_LOW, forecast.getLow());
        values.put(KEY_CONDITION, forecast.getCondition());
        values.put(KEY_ICON, forecast.getIcon());
        values.put(KEY_WEATHER_ID, weatherId);

        long insert_id = db.insert(TABLE_FORECAST, null, values);
    //    Log.e("ForecastDBHelper -> createEntry()",""+insert_id+" for weatherID: "+weatherId);
        db.close();
        return insert_id;
    }

    /****
     * Generic function to retrieve the arraylist of Forecast
     * ***/
    private List<Forecast> genericGetEntry(String selectQuery) {

        List<Forecast> forecastList = null;
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0) {
                forecastList = new ArrayList<Forecast>();
            }

            if (cursor.moveToFirst()) {
                do {
                    Forecast forecast = new Forecast();

                    forecast.setDay(cursor.getString(1));
                    forecast.setHigh(cursor.getString(2));
                    forecast.setLow(cursor.getString(3));
                    forecast.setCondition(cursor.getString(4));
                    forecast.setIcon(cursor.getString(5));

                    forecastList.add(forecast);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e("Exception [ForecastDBHelper]", e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();

            if(db != null)
                db.close();
        }
        return forecastList;

    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public void deleteForecast(long id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_FORECAST, KEY_WEATHER_ID+"="+id, null);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteForecast() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_FORECAST, null, null);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Getting Forecast Count
    public int getEntryCount() {
        String countQuery = "SELECT  * FROM " + TABLE_FORECAST;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
    //    Log.e("ForecastDBHelper -> getEntryCount()","count:"+count);
        return count;
    }

    // Getting airline Count
    public int getEntryCount(long id) {
        String countQuery = "SELECT  * FROM " + TABLE_FORECAST + " WHERE " + KEY_WEATHER_ID + "=" + id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
    //    Log.e("ForecastDBHelper -> getEntryCount()","count:"+count);
        return count;
    }
}
