package com.ekbana.sociotweet.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.ekbana.sociotweet.model.Tweet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nigesh on 6/9/15.
 */
public class TweetDBHelper extends SQLiteOpenHelper {

    private Context context;
    private static TweetDBHelper tweetDBHelper;

    private static String LOG_TEXT = "TweetDBHelper -> ";
    // Table Names
    public static final String TABLE_TWEET = "TWEET";

    // Common column names
    public static final String KEY_ID = "id";
    public static final String KEY_TWEET_ID = "tweet_id";
    public static final String KEY_TWEET_USER_ID = "tweet_user_id";
    public static final String KEY_DATE = "date";
    public static final String KEY_TWEET = "tweet";
    public static final String KEY_IS_PINNED = "is_pinned";

    public static final String CREATE_TWEET = "CREATE TABLE "
            + TABLE_TWEET + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_TWEET_ID + " TEXT, "
            + KEY_TWEET_USER_ID + " INTEGER, "
            + KEY_DATE + " TEXT, "
            + KEY_TWEET + " TEXT, "
            + KEY_IS_PINNED + " INTEGER, "
            + "FOREIGN KEY(" + KEY_TWEET_USER_ID + ") REFERENCES "+ TweetUsersDBHelper.TABLE_TWEET_USERS +"("+ TweetUsersDBHelper.KEY_ID+")"
            +") ";

    public TweetDBHelper(Context context) {
        super(context, WeatherLocationDBHelper.DATABASE_NAME, null, WeatherLocationDBHelper.DATABASE_VERSION);
        this.context = context;
    }

    public static TweetDBHelper getInstance(Context context){
        if(tweetDBHelper == null)
            tweetDBHelper = new TweetDBHelper(context);

        return tweetDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TWEET);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            switch (oldVersion) {
                case 1:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_TWEET);
                    onCreate(db);
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_TWEET);
                    onCreate(db);
                default:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_TWEET);
                    onCreate(db);
            }
        }
    }

    /*******
     * Return all entry of Twitter
     ********/
    public List<Tweet> getAllEntry() {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TWEET;

        return genericGetEntry(selectQuery);
    }

    /****************************************************************
     * Return specific entry of Twitter
     *****************************************************************/
    public Tweet getSpecificEntry(int id) {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TWEET + " WHERE " + KEY_TWEET_USER_ID + "=" + id;
        List<Tweet> tweets = genericGetEntry(selectQuery);
        if(tweets != null && tweets.size() != 0) {
            //    Log.e(""+LOG_TEXT+" getSpecificEntry()","temp: "+weathers.get(0).getTemp());

            return tweets.get(0);
        }else
            return null;
    }

    /********************************************************************
     * Return all the single entry of Tweet
     ******************************************************************/
    public List<Tweet> getTweetFromId(int id) {

        String selectQuery = "SELECT  * FROM " + TABLE_TWEET + " WHERE " + KEY_ID + "=" + id;

        return genericGetEntry(selectQuery);
    }

    /**********************************************************************************
     * return the selected tweet for displaying in the list
     ************************************************************************/
    public Tweet getSelectedTweet(int userHandleId, int isPinned, int offset, int count){
        String query = "SELECT  * FROM " + TABLE_TWEET + " WHERE " + KEY_TWEET_USER_ID +"="+userHandleId
                + " AND " + KEY_IS_PINNED + "=" + isPinned + " ORDER BY datetime("+ KEY_DATE+") DESC " +
                " LIMIT " + /*getEntryCount(userHandleId, isPinned)*/ count + " OFFSET "+offset;

        ArrayList<Tweet> tweets =  (ArrayList<Tweet>)genericGetEntry(query);
        if(tweets != null && tweets.size() != 0)
            return tweets.get(0);
        else
            return null;
    }

    /**
     * createEntry
     * @param tweet
     */
    public void createEntry(Tweet tweet) {

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, tweet.getDate());
        values.put(KEY_TWEET, tweet.getTweet());
        values.put(KEY_TWEET_ID, tweet.getTweetId());
        values.put(KEY_TWEET_USER_ID, tweet.getPinnedUserId());
        values.put(KEY_IS_PINNED, tweet.isPinned());

        SQLiteDatabase db = this.getWritableDatabase();
        long insertId = db.insert(TABLE_TWEET, null, values);
        //  Log.e("TweetDBHelper -> createEntry()","insertId: "+insertId);
    }


    /*********************************************************************************
     * Check if the tweet exists in db or not
     *********************************************************************************/
    public int checkTweetExists(Tweet tweet){

        int tweetId = -1;
/*
        String query = "SELECT * FROM " + TABLE_TWEET + " WHERE " + KEY_TWEET_USER_ID + "=" + tweet.getPinnedUserId()
                + " AND " + KEY_DATE + "='" + tweet.getDate() + "' AND " + KEY_TWEET + "=?";
*/
        String query = "SELECT * FROM " + TABLE_TWEET + " WHERE " + KEY_TWEET_ID + "='"+tweet.getTweetId()+"'";

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, /*new String[]{tweet.getTweet()}*/null);

        if (cursor.moveToFirst()) {
            do{
                tweetId = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        cursor.close();
       // db.close();

        return tweetId;
    }

    /**
     * to get the tweet of the user who has pinned tweets
     * @param isPinned
     * @return
     */
    public Tweet getDistinctEntryId(int isPinned, int position){

        // String abc = "SELECT DISTINCT b.id, a.tweet_id, b.handle, b.display_name, b.profile_image, a.date, a.tweet, a.is_pinned from TWEET a inner join TWEET_USER b where a.user_id=b.id and a.is_pinned=1 group by b.id";
        String query = "SELECT DISTINCT b."+ TweetUsersDBHelper.KEY_ID+", a." + KEY_TWEET_ID + ", b."+ TweetUsersDBHelper.KEY_HANDLE+", b."
                + TweetUsersDBHelper.KEY_DISPLAY_NAME+", b."+ TweetUsersDBHelper.KEY_PROFILE_IMAGE+", a."+KEY_DATE+", a."
                + KEY_TWEET+", a."+KEY_IS_PINNED+", b."+TweetUsersDBHelper.KEY_SELECTED+" "
                + " FROM "+TABLE_TWEET +" a INNER JOIN " + TweetUsersDBHelper.TABLE_TWEET_USERS + " b WHERE a."
                + KEY_TWEET_USER_ID + "=b.id and a." + KEY_IS_PINNED + "=" + isPinned
                +" AND b."+TweetUsersDBHelper.KEY_SELECTED+"=1 "
                + " GROUP BY b."+TweetUsersDBHelper.KEY_ID;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int pos = 0;
        Tweet tweet = null;
        if (cursor.moveToFirst()) {
            tweet = new Tweet();
            do{
                if(pos == position) {
                    tweet.setPinnedUserId(cursor.getInt(0));
                    tweet.setTweetId(cursor.getString(1));
                    tweet.setUser_handle(cursor.getString(2));
                    tweet.setUser_display_name(cursor.getString(3));
                    tweet.setUser_profile_pic(cursor.getString(4));
                    tweet.setDate(cursor.getString(5));
                    tweet.setTweet(cursor.getString(6));
                    tweet.setPinned(cursor.getInt(7) == 1 ? true : false);
                    //    tweetUserId = cursor.getInt(0);
                    break;
                }
                pos++;
            } while (cursor.moveToNext());
        }

        cursor.close();
        //db.close();

        if(tweet != null)
            return tweet;
        else
            return null;
    }

    /**************************************************************************************
     * Edit the isPinned status
     **************************************************************************************/
    public void setKeyIsPinned(Tweet tweet, int tweetId){
        try {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(KEY_IS_PINNED, tweet.isPinned());

            long updateId = db.update(TABLE_TWEET, cv, KEY_ID + "=" + tweetId, null);

        //    db.close();
        }catch (Exception e){
            Log.e("TweetDBHelper -> setKeyIsPinned()",""+e.getMessage());
        }
    }

    /********
     * Generic function to retrieve the arraylist of Tweets
     ******/
    private List<Tweet> genericGetEntry(String selectQuery) {

        List<Tweet> tweetList = null;

        Cursor cursor = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0) {
                tweetList = new ArrayList<Tweet>();
            }

            if (cursor.moveToFirst()) {
                do {
                    TweetUsersDBHelper tweetUsersDBHelper = TweetUsersDBHelper.getInstance(context);
                    Tweet tweet = new Tweet();

                    tweet.setTweetId(cursor.getString(1));
                    tweet.setPinnedUserId(cursor.getInt(2));
                    tweet.setDate(cursor.getString(3));
                    tweet.setTweet(cursor.getString(4));
                    tweet.setUser_display_name(tweetUsersDBHelper.getEntry(cursor.getInt(2)).getDisplayname());
                    tweet.setUser_profile_pic(tweetUsersDBHelper.getEntry(cursor.getInt(2)).getProfile_image());
                    tweet.setUser_handle(tweetUsersDBHelper.getEntry(cursor.getInt(2)).getHandle());
                    tweet.setPinned(cursor.getInt(5) == 1 ? true : false);

                    tweetList.add(tweet);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e("Exception [TweetDBHelper]", e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return tweetList;

    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public void deleteTweet() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_TWEET, null, null);
        //    db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Getting Tweet Count
    public int getEntryCount(int userHandleId, int isPinned, int limit) {
        String countQuery = "SELECT  * FROM " + TABLE_TWEET + " WHERE " + KEY_TWEET_USER_ID +"="+userHandleId
                + " AND " + KEY_IS_PINNED + "=" + isPinned + " ORDER BY datetime("+ KEY_DATE+") DESC " + " LIMIT "+limit;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
    //    db.close();
        return count;
    }

    // Getting Tweet Count
    public int getAllEntryCount(int userHandleId, int isPinned) {
        String countQuery = "SELECT  * FROM " + TABLE_TWEET + " WHERE " + KEY_TWEET_USER_ID +"="+userHandleId
                + " AND " + KEY_IS_PINNED + "=" + isPinned + " ORDER BY datetime("+ KEY_DATE+") DESC LIMIT 10";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        //    db.close();
        return count;
    }

    public int getDistinctEntryCount(int isPinned){
    //    String realQuery = "SELECT distinct a.tweet_user_id,a.is_pinned,b.selected from TWEET a inner join TWEET_USER b " +
    //            "where a.is_pinned=1 and a.tweet_user_id=b.id and b.selected=1 group by a.tweet_user_id ";
        String countQuery = "SELECT  DISTINCT " + KEY_TWEET_USER_ID + ", " + KEY_IS_PINNED + " FROM " + TABLE_TWEET
                + " WHERE " + KEY_IS_PINNED + "=" + isPinned;

        countQuery = "SELECT distinct a."+KEY_TWEET_USER_ID+", a."+KEY_IS_PINNED+",b."+TweetUsersDBHelper.KEY_SELECTED
        +" FROM "+TABLE_TWEET +" a INNER JOIN "+TweetUsersDBHelper.TABLE_TWEET_USERS+" b "+ " WHERE a."+KEY_IS_PINNED
                +"="+isPinned +" AND a."+KEY_TWEET_USER_ID+"=b."+TweetUsersDBHelper.KEY_ID+" AND b."
                +TweetUsersDBHelper.KEY_SELECTED+"=1" +" GROUP BY a."+KEY_TWEET_USER_ID;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
    //    db.close();
        //    Log.e("TweetDBHelper -> getDistinctEntryCount()",""+countQuery);
        //    Log.e("TweetDBHelper -> getDistinctEntryCount()","count: "+count);
        return count;
    }

}
