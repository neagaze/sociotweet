package com.ekbana.sociotweet.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.ekbana.sociotweet.model.OtherApps;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekbana on 5/14/15.
 */
public class EkbanaAppsDBHelper extends SQLiteOpenHelper {

    // Table Names
    public static final String TABLE_OTHER_APPS = "OTHER_APPS";

    // Common column names
    public static final String OTHER_APPS_KEY_ID = "_id";
    public static final String KEY_OTHER_APPS_TITLE = "title";
    public static final String KEY_OTHER_APPS_DESC = "desc";
    public static final String KEY_OTHER_APPS_IMAGE = "image";
    public static final String KEY_OTHER_APPS_URL = "url";

    public static final String CREATE_TABLE_OTHER_APPS = "CREATE TABLE "
            + TABLE_OTHER_APPS + "("
            + OTHER_APPS_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_OTHER_APPS_TITLE + " TEXT, "
            + KEY_OTHER_APPS_DESC + " TEXT, "
            + KEY_OTHER_APPS_IMAGE + " TEXT, "
            + KEY_OTHER_APPS_URL + " TEXT"
            + ")";

    private static EkbanaAppsDBHelper otherAppsDBHelper;

    public EkbanaAppsDBHelper(Context context) {
        super(context, WeatherLocationDBHelper.DATABASE_NAME, null, WeatherLocationDBHelper.DATABASE_VERSION);
    }

    public static EkbanaAppsDBHelper getInstance(Context context){
        if(otherAppsDBHelper == null)
            otherAppsDBHelper = new EkbanaAppsDBHelper(context);

        return otherAppsDBHelper;
    }

    public EkbanaAppsDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_OTHER_APPS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion, int newVersion) {
        // on upgrade drop older tables
        Log.e("EkbanaAppsDBHelper -> onUpgrade()", "old: " + oldVersion + ", new: " + newVersion);
        if (oldVersion != newVersion) {
            switch (oldVersion) {
                case 1:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_OTHER_APPS+";");
                    onCreate(db);
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_OTHER_APPS+";");
                    onCreate(db);
                default:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_OTHER_APPS+";");
                    onCreate(db);
            }
        }
    }

    /**************************************************************************************************************
     * To get the total number of rows returned by the given query
     *
     * @return
     *************************************************************************************************************/
    // Getting Count of rows
    public int getEntryCount() {
        String countQuery = "SELECT * FROM "+TABLE_OTHER_APPS;
        int count = 0;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(countQuery, null);
            Log.e("EkbanaAppsDBHelper -> getEntryCount()", "count:" + cursor.getCount());
            count = cursor.getCount();
            cursor.close();
        }catch (Exception e){
            Log.e("EkbanaAppsDBHelper -> getEntryCount()", "exception: " + e.getMessage());
        }
        return count;
    }

    /*****
     * Create a new date for Ekbana other apps
     *******/
    public void createAllEntry(ArrayList<OtherApps> otherApps) {

        if(otherApps != null) {
            deleteOtherApps();

            for (OtherApps otherApp : otherApps) {
                createEntry(otherApp);
            }
        }
    }

    /*****
     * Create a new date for Ekbana other apps
     *******/
    public long createEntry(OtherApps otherApps) {

        if(otherApps != null) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_OTHER_APPS_TITLE, otherApps.getTitle());
            values.put(KEY_OTHER_APPS_DESC, otherApps.getDesc());
            values.put(KEY_OTHER_APPS_IMAGE, otherApps.getImage());
            values.put(KEY_OTHER_APPS_URL, otherApps.getUrl());

            // insert row
            long insert_id = db.insert(TABLE_OTHER_APPS, null, values);
            //    db.close();
            return insert_id;
        }else
            return -1;
    }

    /*****
     * Create a new date for Ekbana other apps
     *******/
    public long createEntry(String title, String airline_name, String image, String url) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_OTHER_APPS_TITLE, title);
        values.put(KEY_OTHER_APPS_DESC, airline_name);
        values.put(KEY_OTHER_APPS_IMAGE, image);
        values.put(KEY_OTHER_APPS_URL, url);

        // insert row
        long insert_id = db.insert(TABLE_OTHER_APPS, null, values);
        //    db.close();
        return insert_id;
    }

    /***
     * Return all entry of Ekbana apps
     * ******/
    public List<OtherApps> getAllEntry() {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_OTHER_APPS;

        return genericGetOtherApps(selectQuery);
    }

    /****
     * Generic function to retrieve the  list of other ekbana apps
     * ***/
    private List<OtherApps> genericGetOtherApps(String selectQuery) {

        List<OtherApps> appsList = null;

        Cursor cursor = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0) {
                appsList = new ArrayList<OtherApps>();
            }

            if (cursor.moveToFirst()) {
                do {
                    OtherApps otherApps = new OtherApps();
                    otherApps.setTitle(cursor.getString(1));
                    otherApps.setDesc(cursor.getString(2));
                    otherApps.setImage(cursor.getString(3));
                    otherApps.setUrl(cursor.getString(4));

                    appsList.add(otherApps);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e("Exception [EkbanaOtherAppsDBHelper]", e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        // return contact list
        return appsList;

    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public void deleteOtherApps() {

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_OTHER_APPS, null, null);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
