package com.ekbana.sociotweet.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.model.TweetUser;
import com.ekbana.sociotweet.model.WeatherLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nigesh on 6/8/15.
 */
public class TweetUsersDBHelper extends SQLiteOpenHelper {

    private static String LOG_TEXT = "TweetUsersDBHelper -> ";
    // Table Names
    public static final String TABLE_TWEET_USERS = "TWEET_USER";

    // Common column names
    public static final String KEY_ID = "id";
    public static final String KEY_HANDLE = "handle";
    public static final String KEY_DISPLAY_NAME = "displayname";
    public static final String KEY_PROFILE_IMAGE = "profile_image";
    public static final String KEY_SELECTED = "selected";
    public static final String KEY_PINNED_USER = "is_pinned_user";

    public static final String CREATE_TWEET_USERS = "CREATE TABLE "
            + TABLE_TWEET_USERS + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_HANDLE + " TEXT, "
            + KEY_DISPLAY_NAME + " TEXT, "
            + KEY_PROFILE_IMAGE + " TEXT, "
            + KEY_SELECTED + " INTEGER, "
            + KEY_PINNED_USER + " INTEGER"
            +") ";

    private Context context;
    private static TweetUsersDBHelper tweetUsersDBHelper;

    public TweetUsersDBHelper(Context context) {
        super(context, WeatherLocationDBHelper.DATABASE_NAME, null, WeatherLocationDBHelper.DATABASE_VERSION);
        this.context = context;
    }

    public static TweetUsersDBHelper getInstance(Context context){
        if(tweetUsersDBHelper == null)
            tweetUsersDBHelper = new TweetUsersDBHelper(context);

        return tweetUsersDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TWEET_USERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            switch (oldVersion) {
                case 1:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_TWEET_USERS);
                    onCreate(db);
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_TWEET_USERS);
                    onCreate(db);
                default:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_TWEET_USERS);
                    onCreate(db);
            }
        }
    }

    /*******************************************************************************************
     * Create a new date for TweetUser
     ***********************************************************************************************/
    public void createEntry(TweetUser tweetUser) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_HANDLE, tweetUser.getHandle());
        values.put(KEY_DISPLAY_NAME, tweetUser.getDisplayname());
        values.put(KEY_PROFILE_IMAGE, tweetUser.getProfile_image());
        values.put(KEY_SELECTED, tweetUser.isSelected() ? 1 : 0);
        values.put(KEY_PINNED_USER, tweetUser.isPinnedUser() ? 1 : 0);

        long insert_id = db.insert(TABLE_TWEET_USERS, null, values);
        //    Log.e(""+LOG_TEXT+" createEntry()","insertID: "+insert_id);
    }

    /**
     * change the check state
     * @param id
     * @param checkState
     */
    public void editCheckState(int id, boolean checkState){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SELECTED, checkState ? 1 : 0);

        db.update(TABLE_TWEET_USERS, values, KEY_ID+"="+id, null);
    }

    /*********************************************************************
     * Return all PinnedUsers in TweetUser Table
     * ************************************************************************/
    public TweetUser getEntry(int id) {

        String selectQuery = "SELECT * FROM " + TABLE_TWEET_USERS +" WHERE "+KEY_ID+"="+id;
        ArrayList<TweetUser> tweetUsers = (ArrayList<TweetUser>) genericGet(selectQuery);
        if(tweetUsers != null && tweetUsers.size() != 0)
            return tweetUsers.get(0);
        else
            return null;
    }

    /**********************************************************************************************
     * get selected Count
     *******************************************************************************************/
    public int getEnabledUsersCount(int isPinnedInt){
        String query = "SELECT * FROM " + TABLE_TWEET_USERS + " WHERE " + KEY_SELECTED + "=" + 1 + " AND "+KEY_PINNED_USER+"=" + isPinnedInt;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();

        //    Log.e("TweetUsersDBHelper -> getEnabledUsersCount()",""+query);
        //    Log.e("TweetUsersDBHelper -> getEnabledUsersCount()","count: "+count);
        return count;
    }

    /****************************************************************************************
     * get the EnabledIds String for api params
     * @return
     *****************************************************************************************/
    public String getEnabledIds(int isPinnedInt){
        ArrayList<TweetUser> tweetUsers = (ArrayList<TweetUser>)getEnabledEntry(isPinnedInt);
        String returnStr = "";

        if(tweetUsers == null ||  tweetUsers.size() == 0){
            return "?handle=neaGaze"; // + UniversalVariables.defTweetHandles[0];
        }

        if(tweetUsers != null && tweetUsers.size() != 0){
            returnStr = "?handle=";
            int count = 0;
            for(TweetUser tu : tweetUsers){
                count++;
                if(count == tweetUsers.size())
                    returnStr += tu.getHandle();
                else
                    returnStr += tu.getHandle()+",";
            }
        }
        return returnStr;
    }

    /*********************************************************************
     * Return all the tweet users in TweetUser Table
     * ************************************************************************/
    public List<TweetUser> getEnabledEntry(int isPinnedInt) {

        String selectQuery = "SELECT * FROM " + TABLE_TWEET_USERS +" WHERE "+KEY_SELECTED+"=1" + " AND "+KEY_PINNED_USER+"=" + isPinnedInt;

        return genericGet(selectQuery);
    }

    /*********************************************************************
     * Return the enabled value from TweetUser Table
     * ************************************************************************/
    public TweetUser getEnabledEntry(int isPinnedInt, int offsetValue) {

        String selectQuery = "SELECT * FROM " + TABLE_TWEET_USERS +" WHERE "+KEY_SELECTED+"=1" + " AND "
                + KEY_PINNED_USER+"=" + isPinnedInt + " LIMIT 1 OFFSET " + offsetValue;

        ArrayList<TweetUser> tweetUsers = (ArrayList<TweetUser>)genericGet(selectQuery);
        if(tweetUsers != null && tweetUsers.size() != 0)
            return tweetUsers.get(0);
        else
            return null;
    }

    /*********************************************************************
     * Return all the values from TweetUser Table
     * ************************************************************************/
    public TweetUser getEntry(int isPinnedInt, int offsetValue) {

        String selectQuery = "SELECT * FROM " + TABLE_TWEET_USERS +" WHERE "
                + KEY_PINNED_USER+"=" + isPinnedInt + " LIMIT 1 OFFSET " + offsetValue;

        ArrayList<TweetUser> tweetUsers = (ArrayList<TweetUser>)genericGet(selectQuery);
        if(tweetUsers != null && tweetUsers.size() != 0)
            return tweetUsers.get(0);
        else
            return null;
    }

    /*********************************************************************
     * Return all the values in TweetUser Table
     * ************************************************************************/
    public List<TweetUser> getAllEntry() {

        String selectQuery = "SELECT * FROM " + TABLE_TWEET_USERS;

        return genericGet(selectQuery);
    }

    /**********************************************************************
     * Generic function to retrieve the arraylist of TweetUser
     **********************************************************************/
    public List<TweetUser> genericGet(String selectQuery) {

        List<TweetUser> tweetUsers = null;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0) {
                tweetUsers = new ArrayList<TweetUser>();
            }
            if (cursor.moveToFirst()) {
                do {
                    TweetUser tweetUser = new TweetUser();

                    tweetUser.setId(cursor.getInt(0));
                    tweetUser.setHandle(cursor.getString(1));
                    tweetUser.setDisplayname(cursor.getString(2));
                    tweetUser.setProfile_image(cursor.getString(3));
                    tweetUser.setSelected(cursor.getInt(4) > 0);
                    tweetUser.setIsPinnedUser(cursor.getInt(5) > 0);

                    tweetUsers.add(tweetUser);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e("Exception [TweetUsersDBHelper]", "err:" + e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return tweetUsers;
    }

    /**
     * delete Tweet User
     * @param handle
     */
    public boolean deleteTweetUser(String handle) {

        String whereClause = KEY_HANDLE+"='" + handle + "' AND " + KEY_PINNED_USER + "=" + 0;
        String query = "SELECT * FROM " + TABLE_TWEET_USERS + " WHERE "
                + KEY_PINNED_USER + "=" + 0;

        if(getEntryCount(query) <= 1)
            return false;

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_TWEET_USERS, whereClause , null);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * To check if the value is already stored in the db
     * @param handle
     * @return
     */
    public boolean isValueStored(String handle){
        String query = "SELECT * FROM "+ TABLE_TWEET_USERS + " WHERE "+KEY_HANDLE+"='"+handle+"'";
        int count = getEntryCount(query);
        if(count > 0)
            return true;
        else
            return false;
    }

    /**
     * get handleId from handle
     *
     * @param handle
     * @return
     */
    public int getIdFromhandle(String handle){
        String query = "SELECT " + KEY_ID + " FROM "+ TABLE_TWEET_USERS +" WHERE "+KEY_HANDLE + "='"+handle+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            return cursor.getInt(0);
        } else
            return -1;
    }

    /**************************************************************************************************************
     * To get the total number of rows returned by the given query
     *
     * @param countQuery
     * @return
     *************************************************************************************************************/
    public int getEntryCount(String countQuery) {

        int count = 0;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            if(db != null) {
                Cursor cursor = db.rawQuery(countQuery, null);
                //    Log.e("WeatherLocationDBHelper -> getEntryCount()", "count:" + cursor.getCount());
                count = cursor.getCount();
                cursor.close();
            }
        }catch (Exception e){
            Log.e(""+LOG_TEXT+" getEntryCount()","exception: "+e.getMessage());
        }
        return count;
    }

    /**************************************************************************************************************
     * To get the total number of rows returned by the given query
     *
     * @return
     *************************************************************************************************************/
    public int getAllEntryCount(int isPinnedInt) {

        int count = 0;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            if(db != null) {
                String countQuery = "SELECT * FROM "+ TABLE_TWEET_USERS +" WHERE "+ KEY_PINNED_USER + "=" + isPinnedInt;
                Cursor cursor = db.rawQuery(countQuery, null);
                //    Log.e("WeatherLocationDBHelper -> getAllEntryCount()", "count:" + cursor.getCount());
                count = cursor.getCount();
                cursor.close();
            }
        }catch (Exception e){
            Log.e(""+LOG_TEXT+" getAllEntryCount()","exception: "+e.getMessage());
        }
        return count;
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
}
