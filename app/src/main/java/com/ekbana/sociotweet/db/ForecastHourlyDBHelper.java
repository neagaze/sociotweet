package com.ekbana.sociotweet.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.ekbana.sociotweet.model.Forecast;
import com.ekbana.sociotweet.model.ForecastHourly;
import com.ekbana.sociotweet.model.Weather;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekbana on 6/4/15.
 */
public class ForecastHourlyDBHelper extends SQLiteOpenHelper{

    private Context context;
    private static ForecastHourlyDBHelper forecastHourlyDBHelper;

    // Table Names
    public static final String TABLE_FORECAST_HOURLY = "FORECAST_HOURLY";

    // Common column names
    public static final String KEY_ID = "id";
    public static final String KEY_TIME = "time";
    public static final String KEY_TEMPERATURE = "temperature";
    public static final String KEY_CONDITION_TITLE = "condition_title";
    public static final String KEY_CONDITION_ICON = "condition_icon";
    public static final String KEY_TEMPERATURE_FEELSLIKE = "temprature_feelslike";
    public static final String KEY_POP = "pop";
    public static final String KEY_WEATHER_ID = "weather_id";

    public static final String CREATE_FORECAST_HOURLY = "CREATE TABLE "
            + TABLE_FORECAST_HOURLY + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_TIME + " TEXT, "
            + KEY_TEMPERATURE + " TEXT, "
            + KEY_CONDITION_TITLE + " TEXT, "
            + KEY_CONDITION_ICON + " TEXT, "
            + KEY_TEMPERATURE_FEELSLIKE + " TEXT, "
            + KEY_POP + " TEXT, "
            + KEY_WEATHER_ID + " INTEGER, "
            + "FOREIGN KEY("+KEY_WEATHER_ID+") REFERENCES "+WeatherDBHelper.TABLE_WEATHER+"("+WeatherDBHelper.KEY_ID+") "
            +") ";


    public ForecastHourlyDBHelper(Context context) {
        super(context, WeatherLocationDBHelper.DATABASE_NAME, null, WeatherLocationDBHelper.DATABASE_VERSION);
        this.context = context;
    }

    public static ForecastHourlyDBHelper getInstance(Context context){
        if(forecastHourlyDBHelper == null)
            forecastHourlyDBHelper = new ForecastHourlyDBHelper(context);

        return forecastHourlyDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_FORECAST_HOURLY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            switch (oldVersion) {
                case 1:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORECAST_HOURLY);
                    onCreate(db);
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORECAST_HOURLY);
                    onCreate(db);
                default:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORECAST_HOURLY);
                    onCreate(db);
            }
        }
    }

    /***
     * Return all entry of ForecastHourly
     * ******/
    public List<ForecastHourly> getAllEntry() {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_FORECAST_HOURLY;

        return genericGetEntry(selectQuery);
    }

    /********
     * Return all the single entry of ForecastHourly
     * ******/
    public List<ForecastHourly> getForecastHourlyFromId(int id) {

        String selectQuery = "SELECT  * FROM " + TABLE_FORECAST_HOURLY+" WHERE "+KEY_ID+"="+id;

        return genericGetEntry(selectQuery);
    }


    /********
     * Return all the single entry of ForecastHourly
     * ******/
    public List<ForecastHourly> getForecastHourlyFromWeatherId(int weatherId) {

        String selectQuery = "SELECT  * FROM " + TABLE_FORECAST_HOURLY+" WHERE "+KEY_WEATHER_ID+"="+weatherId;

        return genericGetEntry(selectQuery);
    }

    /**
     * create a forecast entry
     *
     * @param forecastHourly
     */
    public long createEntry(ForecastHourly forecastHourly, long weatherId) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TIME, forecastHourly.getTime());
        values.put(KEY_TEMPERATURE, forecastHourly.getTemperature());
        values.put(KEY_CONDITION_TITLE, forecastHourly.getCondition_title());
        values.put(KEY_CONDITION_ICON, forecastHourly.getCondition_icon());
        values.put(KEY_TEMPERATURE_FEELSLIKE, forecastHourly.getTemprature_feelslike());
        values.put(KEY_POP, forecastHourly.getPop());
        values.put(KEY_WEATHER_ID, weatherId);

        long insert_id = db.insert(TABLE_FORECAST_HOURLY, null, values);
    //    Log.e("ForecastHourlyDBHelper -> createEntry()",""+insert_id+" for weatherID: "+weatherId);
        db.close();
        return insert_id;
    }

    /****
     * Generic function to retrieve the arraylist of ForecastHourly
     * ***/
    private List<ForecastHourly> genericGetEntry(String selectQuery) {

        List<ForecastHourly> forecastHourlyList = null;
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0) {
                forecastHourlyList = new ArrayList<ForecastHourly>();
            }

            if (cursor.moveToFirst()) {
                do {
                    ForecastHourly forecastHourly = new ForecastHourly();

                    forecastHourly.setTime(cursor.getString(1));
                    forecastHourly.setTemperature(cursor.getString(2));
                    forecastHourly.setCondition_title(cursor.getString(3));
                    forecastHourly.setCondition_icon(cursor.getString(4));
                    forecastHourly.setTemprature_feelslike(cursor.getString(5));
                    forecastHourly.setPop(cursor.getString(6));

                    forecastHourlyList.add(forecastHourly);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e("Exception [ForecastHourlyDBHelper]", e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
            if(db != null)
                db.close();
        }
        return forecastHourlyList;

    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public void deleteForecastHourly(long id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_FORECAST_HOURLY, KEY_WEATHER_ID+"="+id, null);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteForecastHourly() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_FORECAST_HOURLY, null, null);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Getting airline Count
    public int getEntryCount() {
        String countQuery = "SELECT  * FROM " + TABLE_FORECAST_HOURLY;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
    //    Log.e("ForecastHourlyDBHelper -> getEntryCount()","count:"+count);
        return count;
    }

    // Getting airline Count
    public int getEntryCount(long id) {
        String countQuery = "SELECT  * FROM " + TABLE_FORECAST_HOURLY +" WHERE "+KEY_WEATHER_ID+"="+id;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
    //    Log.e("ForecastHourlyDBHelper -> getEntryCount()","count:"+count);
        return count;
    }

}
