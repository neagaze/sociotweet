package com.ekbana.sociotweet.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.ekbana.sociotweet.model.Forecast;
import com.ekbana.sociotweet.model.ForecastHourly;
import com.ekbana.sociotweet.model.Weather;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nigesh on 6/4/15.
 */
public class WeatherDBHelper extends SQLiteOpenHelper {

    private Context context;
    private static WeatherDBHelper weatherDBHelper;

    private static String LOG_TEXT = "WeatherDBHelper -> ";
    // Table Names
    public static final String TABLE_WEATHER = "WEATHER";

    // Common column names
    public static final String KEY_ID = "id";
    public static final String KEY_WEATHER_LOCATION_ID = "weather_location_id";
    public static final String KEY_LOCATION = "location";
    public static final String KEY_TEMP = "temp";
    public static final String KEY_FORECAST_ID = "forecast_id";
    public static final String KEY_FORECAST_HOURLY_ID = "forecast_hourly_id";

    public static final String CREATE_WEATHER = "CREATE TABLE "
            + TABLE_WEATHER + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_WEATHER_LOCATION_ID + " INTEGER, "
            + KEY_LOCATION + " TEXT, "
            + KEY_TEMP + " TEXT, "
            + KEY_FORECAST_ID + " INTEGER, "
            + KEY_FORECAST_HOURLY_ID + " INTEGER "
            +") ";


    public WeatherDBHelper(Context context) {
        super(context, WeatherLocationDBHelper.DATABASE_NAME, null, WeatherLocationDBHelper.DATABASE_VERSION);
        this.context = context;
    }

    public static WeatherDBHelper getInstance(Context context){
        if(weatherDBHelper == null)
            weatherDBHelper = new WeatherDBHelper(context);

        return weatherDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_WEATHER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            switch (oldVersion) {
                case 1:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEATHER);
                    onCreate(db);
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEATHER);
                    onCreate(db);
                default:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_WEATHER);
                    onCreate(db);
            }
        }
    }

    /*******
     * Return all entry of Weather
     ********/
    public List<Weather> getAllEntry() {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_WEATHER;

        return genericGetEntry(selectQuery);
    }

    /*******
     * Return specific entry of Weather
     ********/
    public Weather getSpecificEntry(int id) {

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_WEATHER + " WHERE "+KEY_WEATHER_LOCATION_ID+"="+id;
        List<Weather> weathers = genericGetEntry(selectQuery);
        if(weathers != null && weathers.size() != 0) {
        //    Log.e(""+LOG_TEXT+" getSpecificEntry()","temp: "+weathers.get(0).getTemp());
        //    if(weathers.get(0).getForecasts() == null || weathers.get(0).getForecasts().size() == 0)
        //        Log.e(""+LOG_TEXT+" getSpecificEntry()","forecasts is null or empty");

            return weathers.get(0);
        }else
            return null;
    }

    /********
     * Return all the single entry of Forecast
     * ******/
    public List<Weather> getForecastFromId(int id) {

        String selectQuery = "SELECT  * FROM " + TABLE_WEATHER + " WHERE " + KEY_ID + "=" + id;

        return genericGetEntry(selectQuery);
    }

    /**
     * createEntry
     * @param weather
     */
    public void createEntry(Weather weather) {

        ContentValues values = new ContentValues();
        values.put(KEY_LOCATION, weather.getLocation());
        values.put(KEY_TEMP, weather.getTemp());
        values.put(KEY_WEATHER_LOCATION_ID, weather.getWeatherLocation());

        SQLiteDatabase db = this.getWritableDatabase();
        long insertId = db.insert(TABLE_WEATHER, null, values);
        //  Log.e("weatherDBHelper -> createEntry()","insertId: "+insertId);
        //     for storing forecasts
        ForecastDBHelper forecastDBHelper = ForecastDBHelper.getInstance(context);
        ArrayList<Forecast> forecasts = weather.getForecasts();
        if(forecasts != null && !forecasts.isEmpty()) {

            forecastDBHelper.deleteForecast(insertId);
            for (Forecast forecast : forecasts) {
                //    if(forecastDBHelper.getEntryCount(insertId) <= 0)
                forecastDBHelper.createEntry(forecast, insertId);
                //    else
                //        Log.e(LOG_TEXT + "createEntry()", "this forecast is already present");
            }
        } else
            Log.e(LOG_TEXT + "createEntry()","forecasts is null or empty");

        // for storing forecastHourlies
        ForecastHourlyDBHelper forecastHourlyDBHelper = ForecastHourlyDBHelper.getInstance(context);
        ArrayList<ForecastHourly> forecastHourlies = weather.getForecastHourlies();
        if(forecastHourlies != null && !forecastHourlies.isEmpty()) {

            forecastHourlyDBHelper.deleteForecastHourly(insertId);

            for (ForecastHourly forecastHourly : forecastHourlies) {
                //    if(forecastHourlyDBHelper.getEntryCount(insertId) <= 0)
                forecastHourlyDBHelper.createEntry(forecastHourly, insertId);
                //    else
                //        Log.e(LOG_TEXT + "createEntry()","this forecastHourly is already present");
            }
        } else
            Log.e(LOG_TEXT + "createEntry()","forecastHourlies is null or empty");
    }

    /****
     * Generic function to retrieve the arraylist of Forecast
     * ***/
    private List<Weather> genericGetEntry(String selectQuery) {

        List<Weather> weatherList = null;

        Cursor cursor = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0) {
                weatherList = new ArrayList<Weather>();
            }

            if (cursor.moveToFirst()) {
                do {
                    Weather weather = new Weather();

                    weather.setId(cursor.getInt(0));
                    weather.setWeatherLocation(cursor.getInt(1));
                    weather.setLocation(cursor.getString(2));
                    weather.setTemp(cursor.getString(3));
                    weather.setForecasts((ArrayList<Forecast>)ForecastDBHelper.getInstance(context)
                            .getForecastFromWeatherId(cursor.getInt(0)));
                    weather.setForecastHourlies((ArrayList<ForecastHourly>)ForecastHourlyDBHelper.getInstance(context)
                            .getForecastHourlyFromWeatherId(cursor.getInt(0)));

                    weatherList.add(weather);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e("Exception [WeatherDBHelper]", e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return weatherList;

    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public void deleteWeather() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_WEATHER, null, null);

            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Getting Weather Count
    public int getEntryCount() {
        String countQuery = "SELECT  * FROM " + TABLE_WEATHER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
    //    Log.e(LOG_TEXT + " getEntryCount()","count:"+cursor.getCount());
        return cursor.getCount();
    }
}
