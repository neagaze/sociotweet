package com.ekbana.sociotweet.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.ekbana.sociotweet.model.WeatherLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ekbana on 6/3/15.
 *
 * for backing up database in sdcard
 *
 * for unrooted phone:
 *      $ ./adb shell "run-as com.ekbana.sociotweet.app cat /data/data/com.ekbana.sociotweet.app/databases/SocioTweet.db > /mnt/sdcard/SocioTweet.db"
 *
 * for rooted phone:
 *
 *      $ ./adb -d shell
 *      $ su
 *      $ cat /data/data/com.ekbana.sociotweet.app/databases/SocioTweet.db > /sdcard/SocioTweet.db
 */
public class WeatherLocationDBHelper extends SQLiteOpenHelper {

    // Database Version
    public static final int DATABASE_VERSION = 1;

    // Database Name
    public static final String DATABASE_NAME = "SocioTweet.db";

    // Table Names
    public static final String TABLE_SOCIOTWEETS = "WEATHER_LOCATION";

    // Common column names
    public static final String KEY_ID = "id";
    public static final String KEY_TWEET_ID = "tweet_id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_SELECTED = "selected";

    public static final String CREATE_SOCIOTWEETS = "CREATE TABLE "
            + TABLE_SOCIOTWEETS + "("
            + KEY_TWEET_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + KEY_ID + " INTEGER, "
            + KEY_TITLE + " TEXT, "
            + KEY_SELECTED + " INTEGER) ";

    private Context context;
    private static WeatherLocationDBHelper weatherLocationDBHelper;
    private WeatherDBHelper weatherDBHelper;
    private ForecastDBHelper forecastDBHelper;
    private ForecastHourlyDBHelper forecastHourlyDBHelper;
    private TweetUsersDBHelper tweetUsersDBHelper;

    public WeatherLocationDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;

        forecastDBHelper = ForecastDBHelper.getInstance(context);
        forecastHourlyDBHelper = ForecastHourlyDBHelper.getInstance(context);
        weatherDBHelper = WeatherDBHelper.getInstance(context);
        tweetUsersDBHelper = (TweetUsersDBHelper.getInstance(context));
    }

    public static WeatherLocationDBHelper getInstance(Context context){
        if(weatherLocationDBHelper == null)
            weatherLocationDBHelper = new WeatherLocationDBHelper(context);

        return weatherLocationDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ForecastDBHelper.CREATE_FORECAST);
        db.execSQL(ForecastHourlyDBHelper.CREATE_FORECAST_HOURLY);
        db.execSQL(WeatherDBHelper.CREATE_WEATHER);
        db.execSQL(TweetUsersDBHelper.CREATE_TWEET_USERS);
        db.execSQL(TweetDBHelper.CREATE_TWEET);
        db.execSQL(CREATE_SOCIOTWEETS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            switch (oldVersion) {
                case 1:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_SOCIOTWEETS);
                    db.execSQL("DROP TABLE IF EXISTS " + TweetDBHelper.TABLE_TWEET);
                    db.execSQL("DROP TABLE IF EXISTS " + ForecastDBHelper.TABLE_FORECAST);
                    db.execSQL("DROP TABLE IF EXISTS " + ForecastHourlyDBHelper.TABLE_FORECAST_HOURLY);
                    db.execSQL("DROP TABLE IF EXISTS " + WeatherDBHelper.TABLE_WEATHER);
                    db.execSQL("DROP TABLE IF EXISTS " + TweetUsersDBHelper.TABLE_TWEET_USERS);
                    onCreate(db);
                case 2:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_SOCIOTWEETS);
                    db.execSQL("DROP TABLE IF EXISTS " + TweetDBHelper.TABLE_TWEET);
                    db.execSQL("DROP TABLE IF EXISTS " + ForecastDBHelper.TABLE_FORECAST);
                    db.execSQL("DROP TABLE IF EXISTS " + ForecastHourlyDBHelper.TABLE_FORECAST_HOURLY);
                    db.execSQL("DROP TABLE IF EXISTS " + WeatherDBHelper.TABLE_WEATHER);
                    db.execSQL("DROP TABLE IF EXISTS " + TweetUsersDBHelper.TABLE_TWEET_USERS);
                    onCreate(db);
                default:
                    db.execSQL("DROP TABLE IF EXISTS " + TABLE_SOCIOTWEETS);
                    db.execSQL("DROP TABLE IF EXISTS " + TweetDBHelper.TABLE_TWEET);
                    db.execSQL("DROP TABLE IF EXISTS " + ForecastDBHelper.TABLE_FORECAST);
                    db.execSQL("DROP TABLE IF EXISTS " + ForecastHourlyDBHelper.TABLE_FORECAST_HOURLY);
                    db.execSQL("DROP TABLE IF EXISTS " + WeatherDBHelper.TABLE_WEATHER);
                    db.execSQL("DROP TABLE IF EXISTS " + TweetUsersDBHelper.TABLE_TWEET_USERS);
                    onCreate(db);
            }
        }
    }

    /*******************************************************************************************
     * Create a new date for WeatherLocation
     ***********************************************************************************************/
    public void createEntry(WeatherLocation weatherLocation) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, weatherLocation.getId());
        values.put(KEY_TITLE, weatherLocation.getTitle());
        values.put(KEY_SELECTED, weatherLocation.isSelected() ? 1 : 0);

        long insert_id = db.insert(TABLE_SOCIOTWEETS, null, values);
        //    Log.e("WeatherLocationDBHelper -> createEntry()","insertID: "+insert_id);
    }

    /**
     * edit the checked state of the given id
     *
     * @param id
     * @param checkState
     */
    public void editCheckState(int id, boolean checkState){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KEY_SELECTED, checkState ? 1 : 0);
        db.update(TABLE_SOCIOTWEETS, cv, ""+KEY_ID+"="+id, null);
    }

    /****************************************************************************************
     * get the EnabledIds String for api params
     * @return
     *****************************************************************************************/
    public String getEnabledIds(){
        ArrayList<WeatherLocation> weatherLocations = (ArrayList<WeatherLocation>)getEnabledEntry();
        String returnStr = "";

        if(weatherLocations == null ||  weatherLocations.size() == 0){
            editCheckState(2, true);
            weatherLocations = (ArrayList<WeatherLocation>)getEnabledEntry();
        }

        if(weatherLocations != null && weatherLocations.size() != 0){
            returnStr = "?id=";
            int count = 0;
            for(WeatherLocation wl : weatherLocations){
                count++;
                if(count == weatherLocations.size())
                    returnStr += wl.getId();
                else
                    returnStr += wl.getId()+",";
            }
        }
        return returnStr;
    }

    /*********************************************************************
     * Return all the dates in WeatherLocation Table
     * ************************************************************************/
    public List<WeatherLocation> getEnabledEntry() {

        String selectQuery = "SELECT * FROM " + TABLE_SOCIOTWEETS +" WHERE "+KEY_SELECTED+"=1";

        return genericGet(selectQuery);
    }

    /*******************************************************************
     * getSingleEntry of WeatherLocation
     *
     * @param offset
     * @return
     *****************************************************************/
    public WeatherLocation getSingleEntry(int offset){
        String query = "SELECT * FROM "+TABLE_SOCIOTWEETS + " LIMIT 1 OFFSET "+offset;

        Cursor cursor = null;
        WeatherLocation weatherLocation = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0)
                weatherLocation = new WeatherLocation();

            if (cursor.moveToFirst()) {
                weatherLocation.setId(cursor.getInt(1));
                weatherLocation.setTitle(cursor.getString(2));
                weatherLocation.setSelected(cursor.getInt(3) > 0);
            }
        } catch (Exception e) {
            Log.e("Exception [WeatherLocationDBHelper]", "err:" + e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return weatherLocation;
    }

    /*********************************************************************
     * Return all the dates in WeatherLocation Table
     * ************************************************************************/
    public List<WeatherLocation> getAllEntry() {

        String selectQuery = "SELECT * FROM " + TABLE_SOCIOTWEETS;

        return genericGet(selectQuery);
    }

    /**********************************************************************
     * Generic function to retrieve the arraylist of weatherLocation
     **********************************************************************/
    private List<WeatherLocation> genericGet(String selectQuery) {

        List<WeatherLocation> weatherLocations = null;
        Cursor cursor = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0) {
                weatherLocations = new ArrayList<WeatherLocation>();
            }
            if (cursor.moveToFirst()) {
                do {
                    WeatherLocation weatherLocation = new WeatherLocation();

                    weatherLocation.setId(cursor.getInt(1));
                    weatherLocation.setTitle(cursor.getString(2));
                    weatherLocation.setSelected(cursor.getInt(3) > 0);

                    weatherLocations.add(weatherLocation);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e("Exception [WeatherLocationDBHelper]", "err:" + e.getMessage());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return weatherLocations;
    }

    public void deleteWeather() {

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_SOCIOTWEETS, null, null);
            db.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * To check if the value is already stored in the db
     * @param id
     * @return
     */
    public boolean isValueStored(int id){
        String query = "SELECT * FROM "+TABLE_SOCIOTWEETS + " WHERE "+KEY_ID+"="+id;
        int count = getEntryCount(query);
        if(count > 0)
            return true;
        else
            return false;
    }

    /**************************************************************************************************************
     * To get the total number of rows returned by the given query
     *
     * @param countQuery
     * @return
     *************************************************************************************************************/
    public int getEntryCount(String countQuery) {

        int count = 0;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            if(db != null) {
                Cursor cursor = db.rawQuery(countQuery, null);
                //    Log.e("WeatherLocationDBHelper -> getEntryCount()", "count:" + cursor.getCount());
                count = cursor.getCount();
                cursor.close();
            }
        }catch (Exception e){
            Log.e("WeatherLocationDBHelper -> getEntryCount()","exception: "+e.getMessage());
        }
        return count;
    }

    /**************************************************************************************************************
     * To get the total number of rows returned by the given query
     *
     * @return
     *************************************************************************************************************/
    public int getAllEntryCount() {

        int count = 0;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            if(db != null) {
                String countQuery = "SELECT * FROM "+TABLE_SOCIOTWEETS;
                Cursor cursor = db.rawQuery(countQuery, null);
                //    Log.e("WeatherLocationDBHelper -> getAllEntryCount()", "count:" + cursor.getCount());
                count = cursor.getCount();
                cursor.close();
            }
        }catch (Exception e){
            Log.e("WeatherLocationDBHelper -> getAllEntryCount()","exception: "+e.getMessage());
        }
        return count;
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
}
