package com.ekbana.sociotweet.model;

/**
 * Created by nigesh on 6/9/15.
 */
public class Tweet {
    private int pinnedUserId;
    private String tweetId, tweet, date, user_handle, user_display_name, user_profile_pic;
    private boolean isPinned;

    public String getTweet() {
        return tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser_handle() {
        return user_handle;
    }

    public void setUser_handle(String user_handle) {
        this.user_handle = user_handle;
    }

    public String getUser_display_name() {
        return user_display_name;
    }

    public void setUser_display_name(String user_display_name) {
        this.user_display_name = user_display_name;
    }

    public String getUser_profile_pic() {
        return user_profile_pic;
    }

    public void setUser_profile_pic(String user_profile_pic) {
        this.user_profile_pic = user_profile_pic;
    }

    public int getPinnedUserId() {
        return pinnedUserId;
    }

    public void setPinnedUserId(int pinnedUserId) {
        this.pinnedUserId = pinnedUserId;
    }

    public boolean isPinned() {
        return isPinned;
    }

    public void setPinned(boolean isPinned) {
        this.isPinned = isPinned;
    }

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    public void setIsPinned(boolean isPinned) {
        this.isPinned = isPinned;
    }
}
