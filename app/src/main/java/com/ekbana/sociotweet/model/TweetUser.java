package com.ekbana.sociotweet.model;

/**
 * Created by nigesh on 6/3/15.
 */
public class TweetUser {
    private int id;
    private String handle, displayname, profile_image;

    private boolean selected, isPinnedUser;

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPinnedUser() {
        return isPinnedUser;
    }

    public void setIsPinnedUser(boolean isPinnedUser) {
        this.isPinnedUser = isPinnedUser;
    }
}
