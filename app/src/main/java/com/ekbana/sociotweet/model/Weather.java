package com.ekbana.sociotweet.model;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by ekbana on 6/4/15.
 */
public class Weather {
    private int id, weatherLocation;
    private String location, temp;
    private ArrayList<Forecast> forecasts;
    private ArrayList<ForecastHourly> forecastHourlies;

    public ArrayList<ForecastHourly> getForecastHourlies() {
        return forecastHourlies;
    }

    public void setForecastHourlies(ArrayList<ForecastHourly> forecastHourlies) {
        this.forecastHourlies = forecastHourlies;
    }

    public ArrayList<Forecast> getForecasts() {
        return forecasts;
    }

    public void setForecasts(ArrayList<Forecast> forecasts) {
        this.forecasts = forecasts;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getWeatherLocation() {
        return weatherLocation;
    }

    public void setWeatherLocation(int weatherLocation) {
        this.weatherLocation = weatherLocation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
