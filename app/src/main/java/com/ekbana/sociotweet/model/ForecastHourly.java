package com.ekbana.sociotweet.model;

/**
 * Created by ekbana on 6/3/15.
 */
public class ForecastHourly {
    private String time, temperature, condition_title, condition_icon, temprature_feelslike, pop;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getCondition_title() {
        return condition_title;
    }

    public void setCondition_title(String condition_title) {
        this.condition_title = condition_title;
    }

    public String getCondition_icon() {
        return condition_icon;
    }

    public void setCondition_icon(String condition_icon) {
        this.condition_icon = condition_icon;
    }

    public String getTemprature_feelslike() {
        return temprature_feelslike;
    }

    public void setTemprature_feelslike(String temprature_feelslike) {
        this.temprature_feelslike = temprature_feelslike;
    }

    public String getPop() {
        return pop;
    }

    public void setPop(String pop) {
        this.pop = pop;
    }
}
