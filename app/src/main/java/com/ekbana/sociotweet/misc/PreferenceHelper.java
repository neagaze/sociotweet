package com.ekbana.sociotweet.misc;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.ekbana.sociotweet.app.SocioTweet;

/**
 * Created by ekbana on 6/7/15.
 */
public class PreferenceHelper {

    private static PreferenceHelper self;
    private SharedPreferences preferences = null;
    private SharedPreferences.Editor editor = null;

    public static final String WEATHER_MAPPER = "WEATHER_MAPPER";
    private String key = null;
    private String value = null;

    public static PreferenceHelper getInstance() {
        if (self == null) {
            self = new PreferenceHelper();
        }
        return self;
    }

    private PreferenceHelper() {
    }

    public void initialise(Context context, String preferenceTable) {
        preferences = context.getSharedPreferences(preferenceTable, Context.MODE_PRIVATE);
    }

    public void setEditable(){
        editor = preferences.edit();
    }

    public void setKeyValue(String key, String value) {
        this.key = key;
        this.value = value;
        savePreferences();
    }

    private void savePreferences() {
        editor.putString(key, value);
    }

    public void commit(){
        editor.commit();
    }

    public String getValue(String key) {
        return preferences.getString(key, null);
    }

    public static String parse(String iconName){
        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance();
        preferenceHelper.initialise(SocioTweet.getContext(), PreferenceHelper.WEATHER_MAPPER);
        String fontValue = preferenceHelper.getValue((iconName != null) ? iconName : "na");
        return fontValue;
    }
}
