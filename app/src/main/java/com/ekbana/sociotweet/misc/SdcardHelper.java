package com.ekbana.sociotweet.misc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import com.ekbana.sociotweet.app.R;
import com.ekbana.sociotweet.db.WeatherLocationDBHelper;

import java.io.*;
import java.nio.channels.FileChannel;

/**
 * Created by neagaze on 2/24/15.
 */
public class SdcardHelper {
    public SdcardHelper(){}

    public Bitmap getFileFromSdcard(String location, String fileName) {

        File sdCard = Environment.getExternalStorageDirectory();

        File directory = new File(sdCard.getAbsolutePath() + "/"+location);

        directory.mkdirs();

        File file = new File(directory, fileName); //or any other format supported

        FileInputStream streamIn = null;
        Bitmap bitmap = null;
        try {
            streamIn = new FileInputStream(file);

            bitmap = BitmapFactory.decodeStream(streamIn); //This gets the image

            streamIn.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }


    /**********************************************************************************************
     * to save the db in sdcard
     *********************************************************************************************/
    public void saveDbInSdcard(Context context){
        File sdCard = Environment.getExternalStorageDirectory();
        File dataDir = Environment.getDataDirectory();

        sdCard = new File(sdCard, context.getString(R.string.app_name) + "/db");

        if(!sdCard.exists())
            sdCard.mkdirs();

        if (sdCard.canWrite()) {
            String currentDBPath = "//data//"+ context.getPackageName() +"//databases//"+WeatherLocationDBHelper.DATABASE_NAME;
            String backupDBPath = WeatherLocationDBHelper.DATABASE_NAME;

            File currentDB = new File(dataDir, currentDBPath);
            File backupDB = new File(sdCard, backupDBPath);
            try {
                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

                Log.e("Sdcard -> saveinSDcard()","file is saved at path: "+sdCard.getPath());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e("Sdcard -> saveinSDcard()", "file not found: "+e.getMessage());

            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Sdcard -> saveinSDcard()", "ioexception: " + e.getMessage());
            }
        } else {Log.e("SdcardHelper -> saveDbInSdcard()","sdcard.canWrite() is false");}

    }

}
