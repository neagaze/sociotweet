package com.ekbana.sociotweet.chart;

import android.content.Context;
import android.graphics.Typeface;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.ekbana.sociotweet.app.R;
import com.ekbana.sociotweet.app.SocioTweet;
import com.ekbana.sociotweet.app.WeatherActivity;
import com.ekbana.sociotweet.db.ForecastHourlyDBHelper;
import com.ekbana.sociotweet.misc.PreferenceHelper;
import com.ekbana.sociotweet.model.ForecastHourly;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.*;
import com.github.mikephil.charting.utils.DegreeFormatter;
import com.github.mikephil.charting.utils.PercentFormatter;

import java.util.ArrayList;

/**
 * Created by nigesh on 7/5/15.
 */
public class WeatherForecastChart {

    private String LOG = "WeatherForecastChart";

    private static final float DEFAULT_NUM_OF_BARS_TO_DISPLAY = 7f;
    private int weatherId;
    private ArrayList<ForecastHourly> forecastHourlies;
    private ForecastHourlyDBHelper forecastHourlyDBHelper;
    private Context context;

    private  ArrayList<BarEntry> entries;
    private ArrayList<String> labels;

    public WeatherForecastChart(Context context, int weatherId){
        this.context = context;
        this.weatherId = weatherId;
        forecastHourlyDBHelper = ForecastHourlyDBHelper.getInstance(SocioTweet.getContext());
        forecastHourlies = (ArrayList<ForecastHourly>)forecastHourlyDBHelper.getForecastHourlyFromWeatherId(weatherId);

        //    init();
        initCombinedChart();
    }

    /**
     * Using CombinedChart
     */
    private void initCombinedChart() {
        if(forecastHourlies != null && forecastHourlies.size() != 0){
            ArrayList<String> lebelEntries = new ArrayList<String>();
            ArrayList<String> labelSupportEntries = new ArrayList<String>();

            for(ForecastHourly forecastHourly : forecastHourlies) {
                lebelEntries.add(forecastHourly.getTime());
                labelSupportEntries.add(PreferenceHelper.parse(forecastHourly.getCondition_icon()));
            }

            CombinedData data = new CombinedData(lebelEntries);
            data.setXSupportVals(labelSupportEntries);

            data.setData(generateLineData());
            data.setData(generateBarData());

            CombinedChart chart = new CombinedChart(context);
            chart.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));

            LinearLayout graphViewGroup = (LinearLayout) ((WeatherActivity) context).
                    findViewById(R.id.graphViewGroup);
            graphViewGroup.addView(chart);

            chart.setData(data);
            chart.setDescription("# of Forecast Hourlies");
            chart.setScaleMinima((float) data.getXValCount() / DEFAULT_NUM_OF_BARS_TO_DISPLAY, 1f);

            XAxis xAxis = chart.getXAxis();
            xAxis.setDrawLabels(true);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);
            xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);
            xAxis.setLabelsToSkip(0);
            xAxis.setTextSize(10);

            /** Include these if you managed to write double x-axis code in the MPAndroidChartLib **/
            Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "meteocons-webfont.ttf");
            xAxis.setSupportTypeface(typeFace);
            xAxis.setSupportTextColor(context.getResources().getColor(R.color.app_primary_blue));
            xAxis.setSupportTextSize(20);

            YAxis yAxisLeft = chart.getAxisLeft();
            yAxisLeft.setDrawLabels(false);
            yAxisLeft.setDrawGridLines(false);
            yAxisLeft.setDrawAxisLine(false);
            yAxisLeft.setValueFormatter(new PercentFormatter());

            YAxis yAxisRight = chart.getAxisRight();
            yAxisRight.setDrawLabels(false);
            yAxisRight.setDrawGridLines(false);
            yAxisRight.setDrawAxisLine(false);
            yAxisRight.setValueFormatter(new PercentFormatter());

            chart.getLegend().setEnabled(false);

            chart.setDrawGridBackground(false);
            chart.setPinchZoom(false);
            chart.setDoubleTapToZoomEnabled(false);
            chart.setScaleEnabled(false);

            chart.setNoDataTextDescription("No Data");
        }
    }

    /*************************************************************************************************
     * for linechart data
     *
     * @return
     ************************************************************************************************/
    private LineData generateLineData() {

        LineData d = new LineData();

        ArrayList<Entry> entries = new ArrayList<Entry>();

        int index = 0;
        for(ForecastHourly forecastHourly : forecastHourlies) {
            entries.add(new Entry(Float.parseFloat(forecastHourly.getTemperature()), index, ""
                    + forecastHourly.getTemperature() + " C"));
            index++;
        }

        LineDataSet set = new LineDataSet(entries, "# of Pop");
        set.setColor(context.getResources().getColor(R.color.blood_red));
        set.setLineWidth(2.5f);
        set.setCircleColor(context.getResources().getColor(R.color.blood_red));
        set.setCircleSize(5f);
        set.setFillColor(context.getResources().getColor(R.color.blood_red));
        set.setDrawCubic(true);
        set.setDrawValues(true);
        set.setValueTextSize(8f);
        set.setValueTextColor(context.getResources().getColor(R.color.black));
        set.setValueFormatter(new DegreeFormatter());

        set.setAxisDependency(YAxis.AxisDependency.LEFT);

        d.addDataSet(set);

        return d;
    }

    /**************************************************************************************************
     * for barchart data
     *
     * @return
     ************************************************************************************************/
    private BarData generateBarData() {

        BarData d = new BarData();

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();

        int index = 0;
        for(ForecastHourly forecastHourly : forecastHourlies) {
            entries.add(new BarEntry(Float.parseFloat(forecastHourly.getPop()), index, ""
                    + forecastHourly.getPop() + " C"));
            index++;
        }

        BarDataSet set = new BarDataSet(entries, "# of forecasts");
        set.setColor(context.getResources().getColor(R.color.new_blue));
        set.setValueTextColor(context.getResources().getColor(R.color.black));
        set.setValueTextSize(8f);
        set.setValueFormatter(new PercentFormatter());
        d.addDataSet(set);

        set.setBarSpacePercent(0.5f);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);

        return d;
    }

    /**
     * Using barchart only
     */
    public void init(){
        if(forecastHourlies != null && forecastHourlies.size() != 0){
            entries = new ArrayList<BarEntry>();
            labels = new ArrayList<String>();
            int count = 0;
            for(ForecastHourly forecastHourly : forecastHourlies) {
                entries.add(new BarEntry(Float.parseFloat(forecastHourly.getPop()), count, ""
                        +forecastHourly.getPop()+" C"));
                labels.add(forecastHourly.getTime());
                count++;
            }
            setGraph();
        }
    }

    public void setGraph(){
        BarDataSet dataset = new BarDataSet(entries, "# of forecasts");
        BarChart chart = new BarChart(context);
        //    ((Activity)context).setContentView(chart);
        LinearLayout graphViewGroup = (LinearLayout) ((WeatherActivity) context).findViewById(R.id.graphViewGroup);
        graphViewGroup.addView(chart);
        BarData data = new BarData(labels, dataset);
        chart.setData(data);
        chart.setDescription("# of Forecast Hourlies");
        chart.setScaleMinima((float) data.getXValCount() / 10f, 1f);

        XAxis xAxis = chart.getXAxis();
        xAxis.setDrawLabels(true);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

        YAxis yAxisLeft = chart.getAxisLeft();
        yAxisLeft.setDrawLabels(false);
        yAxisLeft.setDrawGridLines(false);
        yAxisLeft.setDrawAxisLine(false);

        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setDrawLabels(false);
        yAxisRight.setDrawGridLines(false);
        yAxisRight.setDrawAxisLine(false);

        chart.getLegend().setEnabled(false);

        chart.setDrawGridBackground(false);
        chart.setPinchZoom(false);
        chart.setDoubleTapToZoomEnabled(false);
        chart.setScaleEnabled(false);

        chart.setNoDataTextDescription("No Data");
        dataset.setBarSpacePercent(0.5f);

    }
}
