package com.ekbana.sociotweet.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.db.TweetDBHelper;
import com.ekbana.sociotweet.model.TweetUser;

/**
 * Created by nigesh on 6/26/15.
 */
public class PinnedTweetActivity extends AbstractTweetActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.onTweetTypeListener = new OnTweetTypeListener() {

            @Override
            public Fragment setFragment(int position) {
                Bundle args = new Bundle();
                TweetUser tweetUser = null;
                int pinnedUserId;

                pinnedUserId = TweetDBHelper.getInstance(getApplicationContext())
                        .getDistinctEntryId(UniversalVariables.PINNED_TWEET_ID, position).getPinnedUserId();

                TwitterListsFragment twitterListsFragment = new TwitterListsFragment();
                args.putInt("pinned_user_id", pinnedUserId);
                args.putInt("is_pinned", UniversalVariables.PINNED_TWEET_ID);
                args.putInt("viewpager_position", position);
                args.putInt("list_size", TweetDBHelper.getInstance(getApplicationContext())
                        .getAllEntryCount(pinnedUserId, UniversalVariables.PINNED_TWEET_ID));
                args.putBoolean("is_recyclerView_enabled", true);
                twitterListsFragment.setArguments(args);
                return twitterListsFragment;
            }

            @Override
            public int setCount() {
                return  TweetDBHelper.getInstance(getApplicationContext())
                        .getDistinctEntryCount(UniversalVariables.PINNED_TWEET_ID);

            }

            @Override
            public String setPageTitle(int position) {
                return TweetDBHelper.getInstance(getApplicationContext())
                    .getDistinctEntryId(UniversalVariables.PINNED_TWEET_ID, position).getUser_handle();
            }
        };

        super.onCreate(savedInstanceState);
    }
}
