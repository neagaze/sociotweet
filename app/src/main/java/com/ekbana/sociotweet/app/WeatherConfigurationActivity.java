package com.ekbana.sociotweet.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ekbana.sociotweet.config.Logging;
import com.ekbana.sociotweet.db.WeatherLocationDBHelper;
import com.ekbana.sociotweet.model.WeatherLocation;


public class WeatherConfigurationActivity extends ActionBarActivity {

    private static String LOG_TEXT = "WeatherConfigurationActivity -> ";
    private RecyclerView weatherConfigRecyclerView;

    private boolean hasUserChangedValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_configuration);

        init();
    }

    private void init() {

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.weatherConfig);

        weatherConfigRecyclerView = (RecyclerView) findViewById(R.id.weatherConfigRecyclerView);
        weatherConfigRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        weatherConfigRecyclerView.setLayoutManager(llm);
        WeatherConfigRecyclerViewAdapter adapter = new WeatherConfigRecyclerViewAdapter();
        weatherConfigRecyclerView.setAdapter(adapter);

        hasUserChangedValue = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            if(!hasUserChangedValue) {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(hasUserChangedValue) {
            finish();
            startActivity(new Intent(this, DashboardActivity.class));
        }
        super.onBackPressed();
    }

    /**
     * RecyclerView adapter
     */
    public class WeatherConfigRecyclerViewAdapter extends RecyclerView.Adapter<WeatherConfigRecyclerViewAdapter.ConfigViewHolder>{

        private WeatherLocationDBHelper weatherLocationDbHelper;
        private CheckBox lastChecked = null;
        private int lastCheckedPos = 0;

        public WeatherConfigRecyclerViewAdapter(){
            weatherLocationDbHelper = WeatherLocationDBHelper.getInstance(getApplicationContext());
        }

        @Override
        public ConfigViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.config_layout, parent, false);
            ConfigViewHolder configViewHolder = new ConfigViewHolder(v);
            return configViewHolder;
        }

        @Override
        public void onBindViewHolder(final ConfigViewHolder holder,final int position) {

            final WeatherLocation weatherLocation = weatherLocationDbHelper.getSingleEntry(position);

            if(weatherLocation != null) {
                holder.weatherLocationTextView.setText(weatherLocation.getTitle());
                //    holder.imageView.setImageResource(R.drawable.ic_launcher);
                holder.checkBox.setChecked(weatherLocation.isSelected());
                holder.checkBox.setTag(new Integer(position));

                if(Logging.WeatherConfigActivityLogging) Log.e(""+LOG_TEXT,"title: "+weatherLocation.getTitle()+", selected: "+weatherLocation.isSelected());

                holder.checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox)v;
                        int clickedPos = ((Integer)cb.getTag()).intValue();
                        WeatherLocation weatherLocation = weatherLocationDbHelper.getSingleEntry(clickedPos);

                        weatherLocationDbHelper.editCheckState(weatherLocation.getId(), cb.isChecked());
                        hasUserChangedValue = true;
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return weatherLocationDbHelper.getAllEntryCount();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        // custom RecyclerView adapter
        public class ConfigViewHolder extends RecyclerView.ViewHolder{

            CheckBox checkBox;
            ImageView imageView;
            TextView weatherLocationTextView;

            public ConfigViewHolder(View itemView) {
                super(itemView);
                checkBox = (CheckBox) itemView.findViewById(R.id.weatherSelectorCheckbox);
                imageView = (ImageView) itemView.findViewById(R.id.imageViewHome);
                weatherLocationTextView = (TextView) itemView.findViewById(R.id.weatherLocationTextView);
            }

        }
    }
}
