package com.ekbana.sociotweet.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.*;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.Toolbar;
import com.ekbana.sociotweet.cache.ImageLoader;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.db.EkbanaAppsDBHelper;
import com.ekbana.sociotweet.misc.SdcardHelper;
import com.ekbana.sociotweet.model.OtherApps;
import com.ekbana.sociotweet.webapi.HttpConnection;
import com.ekbana.sociotweet.webapi.ParseJSON;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;


public class AboutActivity extends ActionBarActivity implements View.OnClickListener{

    private Button facebookShare, twitterShare, googlePlusShare;
    private TableLayout tableLayoutOtherApps;
    private TextView privacyPolicy, textViewFlightBuzz;

    private LinearLayout nepaliSongAppDesc, nepalTodayAppDesc, nepaliSMSAppDesc;
    private TableRow tableRowNepaliSongChord, tableRowNepalToday, tableRowNepaliSMS;

    private static String shareTitle = "SocioTweet";
    private static String shareDesc = "Check out #FlightStatsNepal https://play.google.com/store/apps/details?id=com.ekbana.nepflights.ui for tracking flights at Tribhuwan Int'l Airport";
    private static String nepaliSongChordUri = "https://play.google.com/store/apps/details?id=com.ekbana.nepflights.ui";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void init() {
        // Set a toolbar to replace the action bar.
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_about_us);

        tableLayoutOtherApps = (TableLayout) findViewById(R.id.tableLayoutOtherApps);

        nepaliSongAppDesc = (LinearLayout) findViewById(R.id.nepaliSongAppDesc);
        nepalTodayAppDesc = (LinearLayout) findViewById(R.id.nepalTodayAppDesc);
        nepaliSMSAppDesc = (LinearLayout) findViewById(R.id.nepaliSMSAppDesc);

        tableRowNepaliSongChord = (TableRow) findViewById(R.id.tableRowNepaliSongChord);
        tableRowNepalToday = (TableRow) findViewById(R.id.tableRowNepalToday);
        tableRowNepaliSMS = (TableRow) findViewById(R.id.tableRowNepaliSMS);

        facebookShare = (Button) findViewById(R.id.facebookShare);
        twitterShare = (Button) findViewById(R.id.twitterShare);
        googlePlusShare = (Button) findViewById(R.id.googlePlusShare);
        facebookShare.setOnClickListener(this);
        twitterShare.setOnClickListener(this);
        googlePlusShare.setOnClickListener(this);
        tableRowNepaliSongChord.setOnClickListener(this);
        tableRowNepalToday.setOnClickListener(this);
        tableRowNepaliSMS.setOnClickListener(this);

        privacyPolicy = (TextView) findViewById(R.id.privacyPolicy);
        textViewFlightBuzz = (TextView) findViewById(R.id.textViewFlightBuzz);
        textViewFlightBuzz.setText(getResources().getString(R.string.about_sociotweet)+" "+getResources().getString(R.string.app_version));
        privacyPolicy.setOnClickListener(this);

        int width = getResources().getDisplayMetrics().widthPixels;
        TableRow.LayoutParams lParams = new TableRow.LayoutParams(3 * (width / 5),
                ViewGroup.LayoutParams.MATCH_PARENT);
        nepaliSongAppDesc.setLayoutParams(lParams);
        nepalTodayAppDesc.setLayoutParams(lParams);
        nepaliSMSAppDesc.setLayoutParams(lParams);

        OtherEkbanaAppsRetriever otherEkbanaAppsRetriever = new OtherEkbanaAppsRetriever(this, tableLayoutOtherApps);
        otherEkbanaAppsRetriever.execute(new String[]{""});
    }

    public void share(String applicationPath){
/*        String message = "Check out this great app FlightBuzz for tracking flights at Tribhuwam Int'l Airport";
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message);

        startActivity(Intent.createChooser(share, "Title of the dialog the system will open"));
*/

        Intent intent = getPackageManager().getLaunchIntentForPackage(applicationPath);
        if (intent != null) {
            // The application exists
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setPackage(applicationPath);

            shareIntent.putExtra(android.content.Intent.EXTRA_TITLE, "FlightBuzz Share");
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareDesc);
            // Start the specific social application
            startActivity(shareIntent);
        } else {
            // The application does not exist
            // Open GooglePlay or use the default system picker
            Toast.makeText(this, "App is not installed so sharing is not possible", Toast.LENGTH_SHORT).show();
        }
    }

    private Intent getShareIntent(String type, String subject, String text) {
        boolean found = false;
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");

        // gets the list of intents that can be loaded.
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
        System.out.println("resinfo: " + resInfo);
        if (!resInfo.isEmpty()){
            for (ResolveInfo info : resInfo) {
                if (info.activityInfo.packageName.toLowerCase().contains(type) ||
                        info.activityInfo.name.toLowerCase().contains(type) ) {
                    share.putExtra(Intent.EXTRA_SUBJECT,  subject);
                    share.putExtra(Intent.EXTRA_TEXT,     text);
                    share.setPackage(info.activityInfo.packageName);
                    found = true;
                    break;
                }
            }
            if (!found)
                return null;

            return share;
        }
        return null;
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        if(view.equals(facebookShare)) {
            // share("com.facebook.katana");
            intent = getShareIntent("facebook", shareTitle, shareDesc);
            if(intent != null) {
                startActivity(intent);
            } else
                Toast.makeText(this, "Facebook is not installed", Toast.LENGTH_SHORT).show();

        } else if(view.equals(twitterShare)) {
            //  share("com.twitter.android");
            intent = getShareIntent("twitter", shareTitle, shareDesc);
            if(intent != null) {
                startActivity(intent);
            } else
                Toast.makeText(this, "Twitter is not installed", Toast.LENGTH_SHORT).show();
        }else if(view.equals(googlePlusShare)) {
            //    share("com.google.android.apps.plus");
            intent = getShareIntent("apps.plus", shareTitle, shareDesc);
            if(intent != null) {
                startActivity(intent);
            } else
                Toast.makeText(this, "Google Plus is not installed", Toast.LENGTH_SHORT).show();
        }else if (view.equals(privacyPolicy)) {
            Log.e("AboutActivity -> OnClick()", "privacy policy clicked");
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.ekbana.com"));
            startActivity(browserIntent);
        } else {
            try {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(nepaliSongChordUri));
                startActivity(viewIntent);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Unable to Connect Try Again...",
                        Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    /**
     * For retrieving the other apps from Ekbana from background
     */
    private class OtherAppsRetriever extends AsyncTask<String, Void, String> {

        private Context context;
        private ArrayList<OtherApps> otherAppsList;
        private EkbanaAppsDBHelper otherAppsDBHelper;

        public OtherAppsRetriever(Context context){
            this.context = new WeakReference<Context>(context).get();  // context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            String str = "";

            if(context != null) {
                otherAppsDBHelper = EkbanaAppsDBHelper.getInstance(context.getApplicationContext());

                if (HttpConnection
                        .getConnectionAvailable(context)) {

                    HttpConnection connection = HttpConnection.getSingletonConn();
                    str = connection.getJSONusingGET(
                            UniversalVariables.OTHER_APPS_URI);

                    if (ParseJSON.isJSONValid(str)) {
                        otherAppsList = ParseJSON.getOtherApps(str);
                        otherAppsDBHelper.createAllEntry(otherAppsList);
                    }

                    // save the new db in sdcard as well
                    SdcardHelper sdcardHelper = new SdcardHelper();
                    sdcardHelper.saveDbInSdcard(context.getApplicationContext());
                } else {
                    otherAppsList = (ArrayList<OtherApps>) otherAppsDBHelper.getAllEntry();
                    if (otherAppsList == null)
                        str = null;
                    else
                        Log.e("OtherAppsRetriever -> onBackground()", "Problem with webservice, showing local data !");
                }
                return str;
            } else{
                Log.e("AboutActivity -> onBackground()", "context is null");
                return null;
            }
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);

            if(otherAppsList != null && (otherAppsList.size() != 0)) {

                ImageLoader imageLoader = new ImageLoader(context);

                for (final OtherApps otherApp : otherAppsList) {

                    LayoutInflater inflater = (LayoutInflater) context.getSystemService
                            (Context.LAYOUT_INFLATER_SERVICE);
                    TableRow tableRow = (TableRow) inflater.inflate(R.layout.other_apps_layout, null);

                    ImageView appIcon = (ImageView) tableRow.findViewById(R.id.appIcon);
                    imageLoader.DisplayImage(otherApp.getImage(), appIcon);

                    TextView appName = (TextView) tableRow.findViewById(R.id.appName);
                    appName.setText(otherApp.getTitle());

                    TextView appDesc = (TextView) tableRow.findViewById(R.id.appDesc);
                    appDesc.setText(otherApp.getDesc());

                    tableRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (otherApp.getUrl() != null && !otherApp.getUrl().isEmpty()) {
                                nepaliSongChordUri = otherApp.getUrl();
                                try {
                                    Intent viewIntent =
                                            new Intent("android.intent.action.VIEW",
                                                    Uri.parse(nepaliSongChordUri));
                                    startActivity(viewIntent);
                                } catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), "Unable to Connect Try Again...",
                                            Toast.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(context, "Link to this app is not available currently", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    if(tableLayoutOtherApps != null) {
                        TableLayout.LayoutParams lp = new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
                        lp.setMargins(0, 0, 0, 10);
                        tableLayoutOtherApps.addView(tableRow, lp);
                    }
                }
            }
        }
    }
}
