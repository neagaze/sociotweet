package com.ekbana.sociotweet.app;

import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.ekbana.sociotweet.db.WeatherLocationDBHelper;
import com.ekbana.sociotweet.model.WeatherLocation;

import java.util.ArrayList;


public class FollowerSelectActivity extends ActionBarActivity {

    private RecyclerView followingRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follower_select);

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.followers);

        // for recyclerview
        followingRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        followingRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        followingRecyclerView.setLayoutManager(linearLayoutManager);

        WeatherLocationDBHelper weatherLocationDBHelper = WeatherLocationDBHelper.getInstance(getApplicationContext());
        ArrayList<WeatherLocation> weatherLocations = (ArrayList<WeatherLocation>) weatherLocationDBHelper.getAllEntry();
        WeatherLocationAdapter adapter = new WeatherLocationAdapter(weatherLocations);
        followingRecyclerView.setAdapter(adapter);
    }


    /**
     * Adapter for Weather Location
     */
    public class WeatherLocationAdapter extends RecyclerView.Adapter<WeatherLocationAdapter.WeatherLocationHolder>{

        private ArrayList<WeatherLocation> weatherLocations;

        public WeatherLocationAdapter(ArrayList<WeatherLocation> weatherLocations){
            this.weatherLocations = weatherLocations;
        }

        @Override
        public WeatherLocationHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tweet_list_item, viewGroup, false);
            WeatherLocationHolder weatherLocationHolder = new WeatherLocationHolder(v);
            return weatherLocationHolder;
        }

        @Override
        public void onBindViewHolder(WeatherLocationHolder weatherLocationHolder, int i) {
            weatherLocationHolder.tweeterPhoto.setBackgroundResource(R.color.white);
            weatherLocationHolder.person_name.setText(weatherLocations.get(i).getTitle());
            weatherLocationHolder.tweetLink.setText(weatherLocations.get(i).getTitle());
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        @Override
        public int getItemCount() {
            if(weatherLocations != null)
                return weatherLocations.size();
            else
                return 0;
        }

        public class WeatherLocationHolder extends RecyclerView.ViewHolder{

            CardView cv;
            TextView person_name;
            TextView tweetLink;
            ImageView tweeterPhoto;

            WeatherLocationHolder(View itemView) {
                super(itemView);
                cv = (CardView)itemView.findViewById(R.id.cardViewTweet);
                person_name = (TextView)itemView.findViewById(R.id.person_name);
                tweetLink = (TextView)itemView.findViewById(R.id.tweetLink);
                tweeterPhoto = (ImageView)itemView.findViewById(R.id.tweeterPhoto);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_follower_select, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        } else if(id == android.R.id.home) {
            Log.d("home button pressed","home button pressed");
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
