package com.ekbana.sociotweet.app;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.astuetz.PagerSlidingTabStrip;
import com.ekbana.sociotweet.config.Logging;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.db.TweetUsersDBHelper;
import com.ekbana.sociotweet.db.TweetDBHelper;
import com.ekbana.sociotweet.misc.SdcardHelper;
import com.ekbana.sociotweet.model.Tweet;
import com.ekbana.sociotweet.model.TweetUser;
import com.ekbana.sociotweet.webapi.HttpConnection;
import com.ekbana.sociotweet.webapi.ParseJSON;

/***************************************************************************************************
 * Created by nigesh on 6/8/15.
 *
 * This class is for holding the expandableLayout of tweets and pinned tweets
 ******************************************************************************************************/
public class TwitterFragment extends Fragment {

    private static String LOG_TEXT = "TwitterFragment -> ";
    private static int TWITTER_PAGE = 2;
    private static int LATEST_TWEET_ID = 0;
    private static int PINNED_TWEET_ID = 1;

    private View rootView;

    public ViewPager twitterViewPager;
    private PagerSlidingTabStrip twitterPagerTitleStrip;

    private int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if(isAdded())
            super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        if(isAdded())
            super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_twitter, container, false);
        position = getArguments().getInt("position");

        twitterViewPager = (ViewPager) rootView.findViewById(R.id.twitterViewPager);

        twitterPagerTitleStrip = (PagerSlidingTabStrip) rootView.findViewById(R.id.twitterPagerTitleStrip);
        //    twitterViewPager.setLayoutParams(new LinearLayout.LayoutParams(SocioTweet.screenWidth,
        //            SocioTweet.screenHeight * 3 / 10));

        if(isAdded()) {
            TwitterViewPagerAdapter twitterFragmentAdapter = new TwitterViewPagerAdapter(getActivity()
                    .getSupportFragmentManager(), position);
            twitterViewPager.setAdapter(twitterFragmentAdapter);
            twitterViewPager.setId(position + 1);
            twitterViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    if(Logging.TwitterFragmentLogging) Log.e(Logging.LOG_TwitterFragment,"scrollong detected");
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            // sliding tab adapter
            if(TweetDBHelper.getInstance(getActivity().getApplicationContext()).
                    getDistinctEntryCount(position) != 0) {

                twitterPagerTitleStrip.setViewPager(twitterViewPager);
                twitterPagerTitleStrip.setIndicatorHeight(3);
                twitterPagerTitleStrip.setIndicatorColor(getResources().getColor(R.color.toolbar_blue));
                twitterPagerTitleStrip.setDividerColor(getResources().getColor(R.color.gray9));
                twitterPagerTitleStrip.setTextColor(getResources().getColor(R.color.toolbar_blue));

            } else {
                if(Logging.TwitterFragmentLogging) Log.e(""+LOG_TEXT,"empty data found");

                LinearLayout noTweetLayout = (LinearLayout) rootView.findViewById(R.id.noTweetLayout);
                noTweetLayout.setVisibility(View.VISIBLE);

                LinearLayout expandable_content = (LinearLayout) rootView.findViewById(R.id.expandable_content);
                expandable_content.setVisibility(View.GONE);
            }
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // super.onSaveInstanceState(outState);
    }

    /*************************************************************************************
     * ArrayAdapter for twitter. This is called for PINNED TWEETS and LATEST TWEETS sections
     *************************************************************************************/
    private class TwitterFragmentAdapter extends ArrayAdapter{

        private DashboardActivity context;

        public TwitterFragmentAdapter(Context context, int resource) {
            super(context, resource);
            this.context = (DashboardActivity)context;
        }

        @Override
        public int getCount() {
            return TWITTER_PAGE;
        }

        @Override
        public View getView(final int position, View convertView,final ViewGroup parent) {

            final ListItemViewHolder holder;

            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.twitter_subsection, parent, false);

                holder = new ListItemViewHolder();
                holder.headerLayout = (LinearLayout) convertView.findViewById(R.id.headerLayout);
                holder.tweetHeaderTextView = (TextView) convertView.findViewById(R.id.tweetHeaderTextView);
                //    holder.arrowImageView = (ImageView) convertView.findViewById(R.id.arrowImageView);
                holder.twitterViewPager = (ViewPager) convertView.findViewById(R.id.twitterViewPager);
                holder.twitterPagerTitleStrip = (PagerSlidingTabStrip) convertView.findViewById(R.id.twitterPagerTitleStrip);

                convertView.setTag(holder);
            }else
                holder = (ListItemViewHolder)convertView.getTag();

            holder.twitterViewPager.setId(position + 1);

            //    twitterPagerTitleStrip.setTabIndicatorColor(getResources().getColor(R.color.blue1));
            //    twitterPagerTitleStrip.setDrawFullUnderline(true);

            holder.twitterViewPager.setLayoutParams(new LinearLayout.LayoutParams(SocioTweet.screenWidth,
                    SocioTweet.screenHeight * 3 / 10));
            // LinearLayout.LayoutParams.MATCH_PARENT));

            if(position == 0) {
                holder.headerLayout.setBackgroundResource(R.color.toolbar_red);
                holder.tweetHeaderTextView.setText(R.string.latest_tweets);
            }else if(position == 1) {
                holder.headerLayout.setBackgroundResource(R.color.blue1);
                holder.tweetHeaderTextView.setText(R.string.pinned_tweets);
            }

/*          TwitterViewPagerAdapter twitterFragmentAdapter = new TwitterViewPagerAdapter(context.getSupportFragmentManager(), position);
            holder.twitterViewPager.setAdapter(twitterFragmentAdapter);

            // sliding tab adapter
            //    twitterPagerTitleStrip.setTabBackground(R.drawable.pager_sliding_tab_colors);
            holder.twitterPagerTitleStrip.setViewPager(holder.twitterViewPager);
            holder.twitterPagerTitleStrip.setIndicatorHeight(3);
            holder.twitterPagerTitleStrip.setIndicatorColor(getResources().getColor(R.color.toolbar_red));
            holder.twitterPagerTitleStrip.setDividerColor(getResources().getColor(R.color.gray9));
            //    twitterPagerTitleStrip.setUnderlineColor(getResources().getColor(R.color.green));
            holder.twitterPagerTitleStrip.setTextColor(getResources().getColor(R.color.sliding_tab_color));

*/
            TwitterDataRetriever twitterDataRetriever = new TwitterDataRetriever(getActivity(), holder, position);
            String uri = null;
            if(position == 0)
                uri = UniversalVariables.TWEETS_API+"?handle=aakarpost,graphnepal";
            else if(position == 1)
                uri = UniversalVariables.PINNED_TWEETS_API + "?handle=aakarpost,graphnepal";
            twitterDataRetriever.execute(new String[] {uri});

            return convertView;
        }
    }

    /****************************************************************************************
     * TwitterViewPagerAdapter implementation
     ****************************************************************************************/
    public class TwitterViewPagerAdapter extends FragmentStatePagerAdapter {

        private int NUMBER_OF_TWEETS = 2;
        private TweetUsersDBHelper tweetUsersDBHelper;
        private int type;

        public TwitterViewPagerAdapter(FragmentManager fm, int type) {
            super(fm);
            tweetUsersDBHelper = TweetUsersDBHelper.getInstance(getActivity().getApplicationContext());
            this.type = type;
        }

        @Override
        public Fragment getItem(int position) {
            Bundle args = new Bundle();
            TweetUser tweetUser = null;
            int pinnedUserId;

            if(type == LATEST_TWEET_ID) {
                tweetUser = tweetUsersDBHelper.getEnabledEntry(type, position);
                pinnedUserId = tweetUser != null ? tweetUser.getId() : 0;
            } else {
                // i think this might work, check first
                pinnedUserId = TweetDBHelper.getInstance(getActivity().getApplicationContext())
                        .getDistinctEntryId(type, position).getPinnedUserId();
            }

            //    Log.e(LOG_TEXT, "pinnedUserId:  " + pinnedUserId + " position: " + position);

            TwitterListsFragment twitterListsFragment = new TwitterListsFragment();
            args.putInt("pinned_user_id", pinnedUserId);
            args.putInt("is_pinned", type);
            args.putInt("viewpager_position", position);
            args.putInt("list_size", TweetDBHelper.getInstance(getActivity().getApplicationContext())
                    .getEntryCount(pinnedUserId, type, NUMBER_OF_TWEETS));
            args.putBoolean("is_recyclerView_enabled", false);
            twitterListsFragment.setArguments(args);
            return twitterListsFragment;
        }

        @Override
        public int getCount() {
            // return the number of enabled users returned from the database
            int count = 0;

            if(isAdded()) {
                if (type == LATEST_TWEET_ID) {
                    count = tweetUsersDBHelper.getEnabledUsersCount(type);
                } else {
                    // well there is some complication here not just count dude ;)
                    count = TweetDBHelper.getInstance(getActivity().getApplicationContext()).getDistinctEntryCount(type);
                }
            }
            return count;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            if (type == LATEST_TWEET_ID)
                return tweetUsersDBHelper.getEnabledEntry(type, position).getDisplayname();
            else { // I think this one is the correct implementation, however check first dude
                Tweet user = TweetDBHelper.getInstance(getActivity().getApplicationContext())
                        .getDistinctEntryId(type, position);
                return user != null ? user.getUser_handle() : "";
            }
        }
    }

    /**
     * public static viewholder
     */
    public class ListItemViewHolder{
        public LinearLayout headerLayout;
        public TextView tweetHeaderTextView;
        public ImageView arrowImageView;
        public ViewPager twitterViewPager;
        public PagerSlidingTabStrip twitterPagerTitleStrip;
        public ProgressBar tweetProgressBar;
    }

    /**************************************************************************************
     * Twitter Data Retriever
     *************************************************************************************/
    public class TwitterDataRetriever extends WebApiRetriever{

        private ListItemViewHolder holder;
        private int position;

        public TwitterDataRetriever(Context context, ListItemViewHolder holder, int position) {
            super(context);
            setProgressShow(false);
            setOnBackgroundListener(new OnBackgroundDoCallback() {
                @Override
                public boolean executeBackgroundTask(String url) {
                    return retrieveDataFromServer(url);
                }
            });

            this.holder = holder;
            this.position = position;
        }

        // for checking the internet connectivity and retrieve data from server
        private boolean retrieveDataFromServer(String url) {

            HttpConnection connection = HttpConnection.getSingletonConn();
            boolean isValid = false;
            try {
                if (HttpConnection.getConnectionAvailable(context)) {

                    String connected = connection.getJSONusingGET(url);
                    isValid = ParseJSON.isJSONValid(connected);
                    Uri uri = Uri.parse(url);
                    String params = uri.getEncodedQuery();
                    Log.e(LOG_TEXT + "TwitterDataRetriever","params: "+params);

                    if(isValid) {
                        String tempUrl = "http://"+uri.getHost()+uri.getPath();
                        Log.e(LOG_TEXT + "TwitterDataRetriever","path: "+tempUrl);

                        if(tempUrl.equals(UniversalVariables.TWEETS_API))
                            ParseJSON.parseTweets(connected, false);
                        else if(tempUrl.equals(UniversalVariables.PINNED_TWEETS_API))
                            ParseJSON.parseTweets(connected, true);
                    } else
                        Log.e(LOG_TEXT + "TwitterDataRetriever","JSON is invalid");

                    // save the new db in sdcard as well
                    SdcardHelper sdcardHelper = new SdcardHelper();
                    sdcardHelper.saveDbInSdcard(context.getApplicationContext());
                }
            } catch (Exception e) {
                Log.e(LOG_TEXT + "TwitterDataRetriever: ", "" + e.getMessage());
                e.printStackTrace();
            } finally {
                return isValid;
            }
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
            if(isAdded()){
                holder.tweetProgressBar.setVisibility(View.GONE);

                TwitterViewPagerAdapter twitterFragmentAdapter = new TwitterViewPagerAdapter(getActivity().getSupportFragmentManager(), position);
                holder.twitterViewPager.setAdapter(twitterFragmentAdapter);

                // sliding tab adapter
                //    twitterPagerTitleStrip.setTabBackground(R.drawable.pager_sliding_tab_colors);
                holder.twitterPagerTitleStrip.setViewPager(holder.twitterViewPager);
                holder.twitterPagerTitleStrip.setIndicatorHeight(3);
                holder.twitterPagerTitleStrip.setIndicatorColor(getResources().getColor(R.color.toolbar_red));
                holder.twitterPagerTitleStrip.setDividerColor(getResources().getColor(R.color.gray9));
                //    twitterPagerTitleStrip.setUnderlineColor(getResources().getColor(R.color.green));
                holder.twitterPagerTitleStrip.setTextColor(getResources().getColor(R.color.sliding_tab_color));

            }
        }
    }
}
