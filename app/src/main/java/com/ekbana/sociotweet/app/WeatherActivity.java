package com.ekbana.sociotweet.app;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.ekbana.sociotweet.chart.WeatherForecastChart;
import com.ekbana.sociotweet.db.WeatherDBHelper;
import com.ekbana.sociotweet.misc.PreferenceHelper;
import com.ekbana.sociotweet.model.Forecast;
import com.ekbana.sociotweet.model.Weather;

import java.util.ArrayList;
import java.util.List;

public class WeatherActivity extends ActionBarActivity implements View.OnClickListener{

    private static String LOG_TEXT = "WeatherActivity -> ";

    private Typeface typeFace;
    private Weather weather;
    private int weatherId;
    private ArrayList<Forecast> forecasts;

    private LinearLayout forecastWeatherLayout;
    private int forecastWeatherSize, forecastSectionWidth;
    private RecyclerView forecastDailyRecyclerView;
    private ImageView shareIconView;

    private String shareTitle, shareDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        weatherId = getIntent().getExtras().getInt("weatherId");

        typeFace = Typeface.createFromAsset(getAssets(), "meteocons-webfont.ttf");

        init();
    }

    private void init() {

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.weather);

        WeatherDBHelper weatherDBHelper = WeatherDBHelper.getInstance(getApplicationContext());
        weather = weatherDBHelper.getSpecificEntry(weatherId);

        if(weather != null) {
            forecasts = weather.getForecasts();

            TextView locationTextView = (TextView) findViewById(R.id.locationTextView);
            locationTextView.setText("" + weather.getLocation());

            TextView todayTemperatureTextView = (TextView) findViewById(R.id.todayTemperatureTextView);
            todayTemperatureTextView.setText("" + weather.getTemp() + (char) 0x00B0 + "C");

            TextView weatherNameTextView = (TextView) findViewById(R.id.weatherNameTextView);
            weatherNameTextView.setText("" + ((forecasts != null && forecasts.size() != 0) ? forecasts.get(0).getCondition() : ""));

            TextView todayWeatherImage = (TextView) findViewById(R.id.todayWeatherImage);
            todayWeatherImage.setTypeface(typeFace);
            PreferenceHelper preferenceHelper = PreferenceHelper.getInstance();
            preferenceHelper.initialise(this, PreferenceHelper.WEATHER_MAPPER);
            String fontValue = preferenceHelper.getValue((weather != null && forecasts != null
                    && forecasts.size() > 0) ? forecasts.get(0).getIcon() : "na");
            Log.e("" + LOG_TEXT + " loadDatainUI", "forecasts: " + forecasts.get(0).getIcon());
            todayWeatherImage.setText(fontValue);

            WeatherForecastChart weatherForecastChart = new WeatherForecastChart(this, weather.getId());
/*

            forecastWeatherLayout = (LinearLayout) findViewById(R.id.forecastWeatherLayout);
            forecastWeatherLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    forecastSectionWidth = forecastWeatherLayout.getWidth();
                    Log.e("" + LOG_TEXT + " loadDatainUI", "forecastWeatherLayout width: " + forecastSectionWidth);

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                        forecastWeatherLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    else
                        forecastWeatherLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            forecastViewsAdder();
                        }
                    });
                }
            });

*/
            forecastDailyRecyclerView = (RecyclerView) findViewById(R.id.forecastDailyRecyclerView);
            ForecastAdapter adapter = new ForecastAdapter();
            forecastDailyRecyclerView.setAdapter(adapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            forecastDailyRecyclerView.setLayoutManager(layoutManager);

            shareIconView = (ImageView) findViewById(R.id.shareIconView);
            shareIconView.setOnClickListener(this);

            shareDesc = "Current weather at #"+weather.getLocation()+" "+weather.getTemp()+(char) 0x00B0 + "C"
                    +" "+" #"+forecasts.get(0).getCondition()+ " "+ forecasts.get(0).getHigh()+(char) 0x00B0 + "C"+"|"
                    +forecasts.get(0).getLow()+(char) 0x00B0 + "C";
            shareTitle = "Todays weather";
        }
    }

/*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_weather, menu);
        return true;
    }

*/

    @Override
    public void onClick(View view) {
        if(view == shareIconView){
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            // gets the list of intents that can be loaded.
            //    String photoUriTwitterStr = MediaStore.Images.Media.insertImage(
            //            getContentResolver(), yourFile.getAbsolutePath(), null, null);
            //    Log.e("twitter photo path","photoUriTwitterStr: "+photoUriTwitterStr+", yourfile:"+yourFile.getAbsolutePath());
            //    Uri photoUriTwitter = Uri.parse(photoUriTwitterStr);
            boolean found = false;
            List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
            System.out.println("resinfo: " + resInfo);
            if (!resInfo.isEmpty()) {
                for (ResolveInfo info : resInfo) {
                    if (info.activityInfo.packageName.toLowerCase().contains("twitter") ||
                            info.activityInfo.name.toLowerCase().contains("twitter")) {
                        share.setType("*/*");
                        share.putExtra(Intent.EXTRA_SUBJECT, shareTitle);
                        share.putExtra(Intent.EXTRA_TEXT, shareDesc);
                        //    share.putExtra(Intent.EXTRA_STREAM, photoUriTwitter);
                        share.setPackage(info.activityInfo.packageName);
                        found = true;
                        break;
                    }
                }
                if (found)
                    startActivity(share);
                else
                    Toast.makeText(this, "Twitter is not installed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if(id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**************************************************************************
     * method for adding dynamicviews in forecast section
     ************************************************************************/
    private void forecastViewsAdder() {
        forecastWeatherSize =  (int) (forecastSectionWidth / getResources().getDimension(R.dimen.weather_forecast_width));

        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance();
        preferenceHelper.initialise(this, PreferenceHelper.WEATHER_MAPPER);

        Log.e(""+LOG_TEXT+" forecastViewsAdder()","forecastWeatherSize width: " + forecastWeatherSize);

        for(int i = 0; i < forecastWeatherSize; i++) {

            if(!(forecasts.size() >= i+1))
                break;

            Forecast forecast = forecasts.get(i+1);

            final View testView = LayoutInflater.from(this).inflate(R.layout.weather_item_forecasts, forecastWeatherLayout, false);

            TextView weatherTextView = (TextView) testView.findViewById(R.id.weatherTextView);
            weatherTextView.setSelected(true);
            weatherTextView.setText(forecast.getDay());

            TextView weatherIcon = (TextView) testView.findViewById(R.id.weatherIcon);
            weatherIcon.setTypeface(typeFace);
            String fontName = preferenceHelper.getValue((weather != null && forecasts != null
                    && forecasts.size() > 0) ? forecast.getIcon() : "na");
            weatherIcon.setText(fontName);

            TextView weatherTempHigh = (TextView) testView.findViewById(R.id.weatherTempHigh);
            weatherTempHigh.setText(forecast.getHigh()+(char)0x00B0);

            TextView weatherTempLow = (TextView) testView.findViewById(R.id.weatherTempLow);
            weatherTempLow.setText(forecast.getLow()+(char) 0x00B0);

            forecastWeatherLayout.addView(testView);
        }
    }

    /**
     * Recyclerview adapter for forecast of daily weather
     */
    public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder>{

        private PreferenceHelper preferenceHelper;

        public ForecastAdapter(){
            preferenceHelper = PreferenceHelper.getInstance();
            preferenceHelper.initialise(WeatherActivity.this, PreferenceHelper.WEATHER_MAPPER);
        }

        public class ForecastViewHolder extends RecyclerView.ViewHolder{
            public TextView weatherTextView, weatherIcon, weatherTempHigh, weatherTempLow;

            public ForecastViewHolder(View itemView){
                super(itemView);
                weatherTextView = (TextView) itemView.findViewById(R.id.weatherTextView);
                weatherIcon = (TextView) itemView.findViewById(R.id.weatherIcon);
                weatherTempHigh = (TextView) itemView.findViewById(R.id.weatherTempHigh);
                weatherTempLow = (TextView) itemView.findViewById(R.id.weatherTempLow);
            }
        }

        @Override
        public ForecastAdapter.ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(WeatherActivity.this).inflate(R.layout.weather_item_forecasts,
                    parent, false);
            ForecastViewHolder forecastViewHolder = new ForecastViewHolder(view);
            return forecastViewHolder;
        }

        @Override
        public void onBindViewHolder(ForecastAdapter.ForecastViewHolder holder, int position) {
            holder.weatherTextView.setSelected(true);
            holder.weatherTextView.setText(forecasts.get(position).getDay());

            holder.weatherIcon.setTypeface(typeFace);
            String fontName = preferenceHelper.getValue((weather != null && forecasts != null
                    && forecasts.size() > 0) ? forecasts.get(position).getIcon() : "na");
            holder.weatherIcon.setText(fontName);

            holder.weatherTempHigh.setText(forecasts.get(position).getHigh()+(char)0x00B0);

            holder.weatherTempLow.setText(forecasts.get(position).getLow()+(char) 0x00B0);
        }

        @Override
        public int getItemCount() {
            return forecasts == null ? 0 : forecasts.size();
        }
    }
}
