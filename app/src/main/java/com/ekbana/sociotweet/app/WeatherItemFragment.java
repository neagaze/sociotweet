package com.ekbana.sociotweet.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.ekbana.sociotweet.db.WeatherDBHelper;
import com.ekbana.sociotweet.misc.PreferenceHelper;
import com.ekbana.sociotweet.model.Forecast;
import com.ekbana.sociotweet.model.Weather;

import java.util.ArrayList;

/**
 * Created by nigesh on 6/5/15.
 */
public class WeatherItemFragment extends Fragment implements View.OnClickListener{

    private static String LOG_TEXT = "WeatherItemFragment -> ";
    private int WEATHER_PAGE_CODE = 1911;

    private int weatherId;
    private Weather weather;
    private ArrayList<Forecast> forecasts;
    private int forecastSectionWidth, forecastWeatherSize;
    private LinearLayout forecastWeatherLayout;
    private  Typeface typeFace;
    private PreferenceHelper preferenceHelper;

    public WeatherItemFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.weather_item_layout, container, false);

        getArguments().getInt("position");
        weatherId = getArguments().getInt("weatherId");

        typeFace = Typeface.createFromAsset(getActivity().getAssets(), "meteocons-webfont.ttf");
        loadDatainUI(rootView);

        return rootView;
    }

    private void loadDatainUI(View rootView) {

        WeatherDBHelper weatherDBHelper = WeatherDBHelper.getInstance(getActivity().getApplicationContext());
        weather = weatherDBHelper.getSpecificEntry(weatherId);
        if(weather != null) {
            // weather is unavailable

            forecasts = weather.getForecasts();

            TextView locationTextView = (TextView) rootView.findViewById(R.id.locationTextView);
            locationTextView.setText("" + weather.getLocation());

            TextView todayTemperatureTextView = (TextView) rootView.findViewById(R.id.todayTemperatureTextView);
            todayTemperatureTextView.setText("" + weather.getTemp() + (char) 0x00B0 + "C");

            TextView weatherNameTextView = (TextView) rootView.findViewById(R.id.weatherNameTextView);
            weatherNameTextView.setText("" + ((forecasts != null && forecasts.size() != 0) ? forecasts.get(0).getCondition() : ""));

            TextView todayWeatherImage = (TextView) rootView.findViewById(R.id.todayWeatherImage);
            todayWeatherImage.setTypeface(typeFace);
            todayWeatherImage.setOnClickListener(this);

            preferenceHelper = PreferenceHelper.getInstance();
            preferenceHelper.initialise(getActivity(), PreferenceHelper.WEATHER_MAPPER);
            String fontValue = preferenceHelper.getValue((weather != null && forecasts != null
                    && forecasts.size() > 0) ? forecasts.get(0).getIcon() : "na");
            Log.e("" + LOG_TEXT + " loadDatainUI", "forecasts: " + forecasts.get(0).getIcon());
            todayWeatherImage.setText(fontValue);

            forecastWeatherLayout = (LinearLayout) rootView.findViewById(R.id.forecastWeatherLayout);

            rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    forecastSectionWidth = forecastWeatherLayout.getWidth();
                    Log.e("" + LOG_TEXT + " loadDatainUI", "forecastWeatherLayout width: " + forecastSectionWidth);

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                        forecastWeatherLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    else
                        forecastWeatherLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            forecastViewsAdder();
                        }
                    });
                }
            });
        } else{
            LinearLayout realLayout = (LinearLayout) rootView.findViewById(R.id.realLayout);
            LinearLayout noWeatherLayout = (LinearLayout) rootView.findViewById(R.id.noWeatherLayout);

            realLayout.setVisibility(View.GONE);
            noWeatherLayout.setVisibility(View.VISIBLE);
        }
    }

    /**************************************************************************
     * method for adding dynamicviews in forecast section
     ************************************************************************/
    private void forecastViewsAdder() {
        forecastWeatherSize =  (int) (forecastSectionWidth / getActivity().getResources().getDimension(R.dimen.weather_forecast_width));

        //    Log.e(""+LOG_TEXT+" forecastViewsAdder()","forecastWeatherSize width: " + forecastWeatherSize);

        for(int i = 0; i < forecastWeatherSize; i++) {

            if(!(forecasts.size() >= i+1))
                break;

            Forecast forecast = forecasts.get(i+1);

            final View testView = LayoutInflater.from(getActivity()).inflate(R.layout.weather_item_forecasts, forecastWeatherLayout, false);
            testView.setOnClickListener(this);

            TextView weatherTextView = (TextView) testView.findViewById(R.id.weatherTextView);
            weatherTextView.setSelected(true);
            weatherTextView.setText(forecast.getDay());

            TextView weatherIcon = (TextView) testView.findViewById(R.id.weatherIcon);
            weatherIcon.setTypeface(typeFace);
            String fontName = preferenceHelper.getValue((weather != null && forecasts != null
                    && forecasts.size() > 0) ? forecast.getIcon() : "na");
            weatherIcon.setText(fontName);

            TextView weatherTempHigh = (TextView) testView.findViewById(R.id.weatherTempHigh);
            weatherTempHigh.setText(forecast.getHigh()+(char)0x00B0);

            TextView weatherTempLow = (TextView) testView.findViewById(R.id.weatherTempLow);
            weatherTempLow.setText(forecast.getLow()+(char) 0x00B0);

            forecastWeatherLayout.addView(testView);
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), WeatherActivity.class);
        intent.putExtra("weatherId", weatherId);
        startActivity(intent);
    }
}