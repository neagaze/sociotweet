package com.ekbana.sociotweet.app;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.db.EkbanaAppsDBHelper;
import com.ekbana.sociotweet.misc.SdcardHelper;
import com.ekbana.sociotweet.model.OtherApps;
import com.ekbana.sociotweet.webapi.HttpConnection;
import com.ekbana.sociotweet.webapi.ParseJSON;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by ekbana on 5/19/15.
 */
public class OtherEkbanaAppsRetrieverOnce extends AsyncTask<String, Void, String> {

    protected Context context;
    protected ArrayList<OtherApps> otherAppsList;
    protected EkbanaAppsDBHelper otherAppsDBHelper;

    public OtherEkbanaAppsRetrieverOnce(Context context){
        this.context = new WeakReference<Context>(context).get();  // context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        String str = "";

        if(context != null) {
            otherAppsDBHelper = EkbanaAppsDBHelper.getInstance(context.getApplicationContext());

            if (HttpConnection.getConnectionAvailable(context)) {

                HttpConnection connection = HttpConnection.getSingletonConn();
                str = connection.getJSONusingGET(
                        UniversalVariables.OTHER_APPS_URI);

                if (ParseJSON.isJSONValid(str)) {
                    otherAppsList = ParseJSON.getOtherApps(str);
                    otherAppsDBHelper.createAllEntry(otherAppsList);
                }

                // save the new db in sdcard as well
                SdcardHelper sdcardHelper = new SdcardHelper();
                sdcardHelper.saveDbInSdcard(context.getApplicationContext());
            } else {
                otherAppsList = (ArrayList<OtherApps>) otherAppsDBHelper.getAllEntry();
                if (otherAppsList == null)
                    str = null;
                else
                    Log.e("OtherEKbanaAppsretrieverOnce -> onBackground()", "Problem with webservice, showing local data !");
            }
            return str;
        } else{
            Log.e("OtherEKbanaAppsretrieverOnce -> onBackground()", "context is null");
            return null;
        }
    }
/*
    @Override
    protected void onPostExecute(String str) {
        super.onPostExecute(str);

        if(context != null && otherAppsList != null && (otherAppsList.size() != 0)) {

            ImageLoader imageLoader = new ImageLoader(context);

            for (final OtherApps otherApp : otherAppsList) {

                LayoutInflater inflater = (LayoutInflater) context.getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                TableRow tableRow = (TableRow) inflater.inflate(R.layout.other_apps_layout, null);

                ImageView appIcon = (ImageView) tableRow.findViewById(R.id.appIcon);
                imageLoader.DisplayImage(otherApp.getImage(), appIcon);

                TextView appName = (TextView) tableRow.findViewById(R.id.appName);
                appName.setText(otherApp.getTitle());

                TextView appDesc = (TextView) tableRow.findViewById(R.id.appDesc);
                appDesc.setText(otherApp.getDesc());

                tableRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (otherApp.getUrl() != null && !otherApp.getUrl().isEmpty()) {
                            String uri = otherApp.getUrl();
                            try {
                                Intent viewIntent =
                                        new Intent("android.intent.action.VIEW",
                                                Uri.parse(uri));
                                ((AboutActivity)context).startActivity(viewIntent);
                                t.send(new HitBuilders.EventBuilder()
                                        .setCategory("Ekbana App event")
                                        .setAction("User Pressed Ekbana Other apps link")
                                        .build());
                            } catch (Exception e) {
                                Toast.makeText(context.getApplicationContext(), "Unable to Connect Try Again...",
                                        Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Link to this app is not available currently", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                if(tableLayoutOtherApps != null) {
                    TableLayout.LayoutParams lp = new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0, 0, 0, 10);
                    tableLayoutOtherApps.addView(tableRow, lp);
                }
            }
        }
    }*/
}
