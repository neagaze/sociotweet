package com.ekbana.sociotweet.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.WindowManager;
import com.ekbana.sociotweet.misc.PreferenceHelper;

import java.util.HashMap;

/**
 * Created by ekbana on 6/2/15.
 */
public class SocioTweet extends Application{

    private static Context mContext;
    public static int screenWidth, screenHeight;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        getWeatherMapper();
        setScreenWidth();
    }

    /**
     * for wrapping weather cases
     */
    private void getWeatherMapper() {
        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance();
        preferenceHelper.initialise(this, PreferenceHelper.WEATHER_MAPPER);
        preferenceHelper.setEditable();
        preferenceHelper.setKeyValue("partlysunny", "A");
        preferenceHelper.setKeyValue("sunny","B");
        preferenceHelper.setKeyValue("clear","B");
        preferenceHelper.setKeyValue("nt_clear","C");
        preferenceHelper.setKeyValue("fog","E");
        preferenceHelper.setKeyValue("hazy","F");
        preferenceHelper.setKeyValue("nt_chancesnow","G");
        preferenceHelper.setKeyValue("mostlycloudy","H");
        preferenceHelper.setKeyValue("nt_mostlysunny","I");
        preferenceHelper.setKeyValue("nt_mostlycloudy","I");
        preferenceHelper.setKeyValue("mostlysunny","J");
        preferenceHelper.setKeyValue("cloudy","K");
        preferenceHelper.setKeyValue("partlycloudy","L");
        preferenceHelper.setKeyValue("partlycloudy","N");
        preferenceHelper.setKeyValue("chancetstorms","O");
        preferenceHelper.setKeyValue("tstorms", "P");
        preferenceHelper.setKeyValue("chancerain","Q");
        preferenceHelper.setKeyValue("rain","R");
        preferenceHelper.setKeyValue("windy","S");
        preferenceHelper.setKeyValue("chancesleet","T");
        preferenceHelper.setKeyValue("chancesnow","U");
        preferenceHelper.setKeyValue("chanceflurries","V");
        preferenceHelper.setKeyValue("snow","W");
        preferenceHelper.setKeyValue("nt_chancesnow","X");
        preferenceHelper.setKeyValue("mostlycloudy","Y");
        preferenceHelper.setKeyValue("nt_tstorms","Z");
        preferenceHelper.setKeyValue("na",")");
        preferenceHelper.commit();
    }

   /****************************************************************************
    * get Screen width and height
    **********************************************************************************/
    public void setScreenWidth() {
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Point size = new Point();
        wm.getDefaultDisplay().getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;
        Log.e("screen dimens [SocioTweet]","width: "+screenWidth+", screenHeight: "+screenHeight);
    }

    public static Context getContext(){
        if(mContext == null)
            mContext = new SocioTweet();
        return mContext;
    }
}
