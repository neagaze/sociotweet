package com.ekbana.sociotweet.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.lang.ref.WeakReference;

/**
 * Created by nigesh on 6/2/15.
 */
public class WebApiRetriever extends AsyncTask<String, Void, Boolean> {

    protected Context context;
    private OnBackgroundDoCallback onBackgroundDoCallback;
    private boolean shouldShowProgress = true;
    protected ProgressDialog progressDialog;

    public WebApiRetriever(Context context){
        this.context = new WeakReference<Context>(context).get();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(shouldShowProgress){
            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Receiving the data from web");
            progressDialog.show();
        }
    }

    @Override
    protected Boolean doInBackground(String... strings) {

        boolean dataRetrieved = false;

        for(String string : strings) {
            if(onBackgroundDoCallback != null)
                dataRetrieved = onBackgroundDoCallback.executeBackgroundTask(string);
            // Log.e("dataReceived at background",""+dataRetrieved);
            if(dataRetrieved == false)
                break;
        }
        return Boolean.valueOf(dataRetrieved); // dataRetrieved;
    }

    @Override
    protected void onPostExecute(Boolean s) {
        super.onPostExecute(s);
        if(shouldShowProgress && progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    public void setOnBackgroundListener(OnBackgroundDoCallback onBackgroundDoCallback){
        this.onBackgroundDoCallback = onBackgroundDoCallback;
    }

    public void setProgressShow(boolean progressShow){
        this.shouldShowProgress = progressShow;
    }

    public boolean getProgressShow(){
        return shouldShowProgress;
    }

    public static interface OnBackgroundDoCallback{
        public boolean executeBackgroundTask(String url);
    }

}
