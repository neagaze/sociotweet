package com.ekbana.sociotweet.app;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.*;
import android.view.animation.LinearInterpolator;
import android.widget.*;
import com.ekbana.sociotweet.cache.ImageLoader;
import com.ekbana.sociotweet.config.Logging;
import com.ekbana.sociotweet.customui.EndlessRecylerOnScrollListener;
import com.ekbana.sociotweet.db.TweetUsersDBHelper;
import com.ekbana.sociotweet.db.TweetDBHelper;
import com.ekbana.sociotweet.model.TweetUser;
import com.ekbana.sociotweet.model.Tweet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**********************************************************************************************
 * Created by nigesh on 6/9/15.
 *
 * This class is for implementing list of tweets
 *******************************************************************************************/
public class TwitterListsFragment extends Fragment {

    private static String LOG_TEXT = "TWitterListsFragment -> ";

    private static int ITEM_HEIGHT = 135;
    private static int NUMBER_OF_TWEETS = 2;
    private int twitterHandleId;
    private int isPinned;
    private int viewpagerPos;
    private int count;
    private boolean isRecyclerViewEnabled;
    private RecyclerView twitterRecyclerView;
    private TwitterRecyclerViewAdapter adapter;

    public TwitterListsFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        twitterHandleId = getArguments().getInt("pinned_user_id");
        isPinned = getArguments().getInt("is_pinned");
        viewpagerPos = getArguments().getInt("viewpager_position", 0);
        count = getArguments().getInt("list_size", 0);
        isRecyclerViewEnabled = getArguments().getBoolean("is_recyclerView_enabled", false);

        //     Log.e(""+LOG_TEXT,"is_pinned : "+isPinned+", pinned_user_id : "+twitterHandleId +", isRecyclerViewEnabled: "+isRecyclerViewEnabled);

        final View rootView = inflater.inflate(R.layout.twitter_list_fragment, container, false);

        if(isRecyclerViewEnabled)
            initWithRecyclerView(rootView);
        else
            initWithLinearLayout(rootView);

        return rootView;
    }

    /**
     * init of recyclerView
     */
    private void initWithRecyclerView(View rootView) {
        int count = new TweetDBHelper(getActivity().getApplicationContext()).getAllEntryCount(twitterHandleId, isPinned);
        if(count > 0) {
            //    twitterRecyclerView = (RecyclerView) rootView.findViewById(/*R.id.twitterRecyclerView*/  0 );
            twitterRecyclerView = new RecyclerView(getActivity());
            twitterRecyclerView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));

            LinearLayout baseLayout = (LinearLayout) rootView.findViewById(R.id.baseLayout);
            baseLayout.addView(twitterRecyclerView);

            twitterRecyclerView.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            if(Logging.TwitterListsFragmentLogging)
                Log.e(Logging.LOG_TwitterListsFragment, "last seen item: "+llm.findLastVisibleItemPosition());
            twitterRecyclerView.setLayoutManager(llm);
            adapter = new TwitterRecyclerViewAdapter();
            adapter.setCount(count);
            twitterRecyclerView.setAdapter(adapter);

            twitterRecyclerView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(final View v, MotionEvent event) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    //    if(Logging.TwitterListsFragmentLogging)
                    //        Log.e("TwitterListFragment -> twitterRecyclerView()","touch at recyclerView");
                    final GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {

                        @Override
                        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                            //        Log.e("TwitterListsFragment -> gestureDetector","scrolling detected at recyclerView");
                            return super.onScroll(e1, e2, distanceX, distanceY);
                        }

                    });
                    return gestureDetector.onTouchEvent(event);

                }
            });

            twitterRecyclerView.setOnScrollListener(new EndlessRecylerOnScrollListener(llm) {
                @Override
                public void onLoadMore(RecyclerView recyclerView, int currentPage, int installments) {

                    TwitterRecyclerViewAdapter adapter1 = (TwitterRecyclerViewAdapter) recyclerView.getAdapter();
                    adapter1.setCount(new TweetDBHelper(getActivity().getApplicationContext())
                            .getEntryCount(twitterHandleId, isPinned, installments));
                    adapter1.notifyItemInserted(currentPage);

                    //    if(Logging.TwitterListsFragmentLogging)   Log.e(""+LOG_TEXT,"currentPage: "+currentPage+", and count: ");
                }
            });
        } else{
            LinearLayout noWeatherLayout = (LinearLayout) rootView.findViewById(R.id.noTweetLayout);
            noWeatherLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Using Linear Layout for adding views
     * @param rootView
     */
    public void initWithLinearLayout(View rootView){

        TweetDBHelper tweetDBHelper = TweetDBHelper.getInstance(getActivity().getApplicationContext());
        ImageLoader imageLoader = new ImageLoader(getActivity().getApplicationContext());

        LinearLayout baseLayout = (LinearLayout) rootView.findViewById(R.id.baseLayout);

        //    int count = tweetDBHelper.getEntryCount(twitterHandleId, isPinned);

        for(int position = 0; position < count; position++) {

            final int pos = position;

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.tweet, null, false);

            Tweet tweet = tweetDBHelper.getSelectedTweet(twitterHandleId, isPinned, position, count);

            TweetUser tweetUser = (tweet != null) ? (TweetUsersDBHelper.getInstance(getActivity().
                    getApplicationContext()).getEntry(tweet.getPinnedUserId())) : null;

            ImageView twitterUserImage;
            LinearLayout tweetLayout;
            TextView tweeterNameTextView, tweetIdTextView, timeAgoTextView, tweetTextView;

            final CardView cardView = (CardView) view.findViewById(R.id.twitterCardView);
            tweetLayout = (LinearLayout) view.findViewById(R.id.tweetLayout);
            twitterUserImage = (ImageView) view.findViewById(R.id.twitterUserImage);
            tweeterNameTextView = (TextView) view.findViewById(R.id.tweeterNameTextView);
            tweetIdTextView = (TextView) view.findViewById(R.id.tweetIdTextView);
            timeAgoTextView = (TextView) view.findViewById(R.id.timeAgoTextView);
            tweetTextView = (TextView) view.findViewById(R.id.tweetTextView);

            cardView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    //    SocioTweet.screenHeight / 7));
                    LinearLayout.LayoutParams.WRAP_CONTENT));

            tweetTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    120));

            tweeterNameTextView.setText(tweet.getUser_display_name());
            timeAgoTextView.setText(formatTime(tweet.getDate()));
            tweetIdTextView.setText((tweetUser != null) ? "@" + tweetUser.getHandle() : "");
            tweetTextView.setText(tweet.getTweet());
            imageLoader.DisplayImage(tweetUser.getProfile_image(), twitterUserImage);

            cardView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    //    holder.cardView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    //            holder.cardView.getHeight()));
                    if(Logging.TwitterListsFragmentLogging)
                        Log.e("TwitterListsFragment -> onBindViewHolder()", "height: " + cardView.getHeight());

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                        cardView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    else
                        cardView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });

            tweetTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Logging.TwitterListsFragmentLogging)
                        Log.e(""+LOG_TEXT,"initWithLinearLayout -> tweetLayout clickListener()");
                    if(isPinned == 0) {
                        Intent intent = new Intent(getActivity(), LatestTweetActivity.class);
                        intent.putExtra("viewPagerDefaultPage", viewpagerPos);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getActivity(), PinnedTweetActivity.class);
                        intent.putExtra("viewPagerDefaultPage", viewpagerPos);
                        startActivity(intent);
                    }
                }
            });

            baseLayout.addView(view);
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * RecyclerView adapter
     */
    public class TwitterRecyclerViewAdapter extends RecyclerView.Adapter<TwitterRecyclerViewAdapter.TwitterViewHolder>
            implements View.OnClickListener{

        int firstResId, secondResId;
        public int count;
        private TweetDBHelper tweetDBHelper;
        private ImageLoader imageLoader;

        private int mOriginalHeight = 0;
        private boolean mIsViewExpanded = false;

        public TwitterRecyclerViewAdapter(){
            tweetDBHelper = TweetDBHelper.getInstance(getActivity().getApplicationContext());
            imageLoader = new ImageLoader(getActivity().getApplicationContext());
        }

        public void setCount(int count){
            this.count = count;
        }

        @Override
        public TwitterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tweet, parent, false);
            TwitterViewHolder twitterViewHolder = new TwitterViewHolder(v);
            return twitterViewHolder;
        }

        @Override
        public void onBindViewHolder(final TwitterViewHolder holder, int position) {

            final Tweet tweet = tweetDBHelper.getSelectedTweet(twitterHandleId, isPinned, position, count);
            TweetUser tweetUser = (tweet != null) ? (TweetUsersDBHelper.getInstance(getActivity().
                    getApplicationContext()).getEntry(tweet.getPinnedUserId())) : null;

            holder.cardView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    //    SocioTweet.screenHeight / 7));
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            //    100));

            holder.tweeterNameTextView.setText(tweet.getUser_display_name());
            holder.timeAgoTextView.setText(formatTime(tweet.getDate()));
            holder.tweetIdTextView.setText((tweetUser != null) ? "@"+ tweetUser.getHandle() : "");
            holder.tweetTextView.setText(tweet.getTweet());
            imageLoader.DisplayImage(tweetUser.getProfile_image(), holder.twitterUserImage);
            holder.twitterUserImage.setLayoutParams(new LinearLayout.LayoutParams(holder.twitterUserImage.getLayoutParams().width,
                    ITEM_HEIGHT));
            holder.twitterUserImage.setPadding(3,3,3,3);

            holder.cardView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    //    holder.cardView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    //            holder.cardView.getHeight()));

                    if(Logging.TwitterListsFragmentLogging)
                        Log.e("TwitterListsFragment -> onBindViewHolder()","height: "+holder.cardView.getHeight());

                    ExpandedItemParams expandedItemParams = (ExpandedItemParams)holder.llReTweet.getTag();
                    expandedItemParams.height = holder.cardView.getHeight();
                    //        holder.twitterUserImage.setTag(2, new Integer(holder.cardView.getHeight()));

                    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                        holder.cardView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    else
                        holder.cardView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });

            ExpandedItemParams expandedItemParams = new ExpandedItemParams(mIsViewExpanded,
                    holder.cardView.getHeight(), holder.cardView.getHeight());
            holder.llReTweet.setTag(expandedItemParams);

            holder.twitterUserImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    animateExpansion(holder.llReTweet);
                }
            });

            holder.tweetTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    animateExpansion(holder.llReTweet);
                }
            });

            holder.tweeterNameTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    animateExpansion(holder.llReTweet);
                }
            });

            holder.ivFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e(""+LOG_TEXT," favourite");
                    if(getActivity() instanceof AbstractTweetActivity){
                        new AsyncTask<Void, Void, Boolean>() {
                            private boolean isTweekOk = false;

                            @Override
                            protected Boolean doInBackground(Void... voids) {
                                isTweekOk = ((AbstractTweetActivity)getActivity()).authAndRetweet(tweet.getTweetId(), false);
                                Log.e(""+LOG_TEXT,"isTweetOK: "+isTweekOk);
                                if(isTweekOk)
                                    ((AbstractTweetActivity)getActivity()).makeFavorite(tweet.getTweetId());
                                return isTweekOk;
                            }

                            @Override
                            protected void onPostExecute(Boolean bool) {
                                super.onPostExecute(bool);
                                if(isAdded()) {
                                    //        if (bool.booleanValue())
                                    //            Toast.makeText(getActivity(), "Favourited", Toast.LENGTH_SHORT).show();
                                    //        else
                                    //            Toast.makeText(getActivity(), "Sorry can't make favourite right now", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }.execute();
                    }
                }
            });

            holder.ivReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // make reply
                    Log.e(""+Logging.LOG_TwitterListsFragment,"tweet reply done");
                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");

                    boolean found = false;
                    List<ResolveInfo> resInfo = getActivity().getPackageManager().queryIntentActivities(share, 0);
                    System.out.println("resinfo: " + resInfo);
                    if (!resInfo.isEmpty()) {
                        for (ResolveInfo info : resInfo) {
                            if (info.activityInfo.packageName.toLowerCase().contains("twitter") ||
                                    info.activityInfo.name.toLowerCase().contains("twitter")) {
                                share.setType("*/*");
                                share.putExtra(Intent.EXTRA_SUBJECT, "");
                                share.putExtra(Intent.EXTRA_TEXT, "@"+tweet.getUser_handle()+" ");
                                //    share.putExtra(Intent.EXTRA_STREAM, photoUriTwitter);
                                share.setPackage(info.activityInfo.packageName);
                                found = true;
                                break;
                            }
                        }
                        if (found)
                            startActivity(share);
                        else
                            Toast.makeText(getActivity(), "Twitter is not installed", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            holder.ivRetweet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getActivity() instanceof AbstractTweetActivity){
                        new AsyncTask<Void, Void, Boolean>() {
                            private boolean isTweekOk = false;

                            @Override
                            protected Boolean doInBackground(Void... voids) {
                                isTweekOk = ((AbstractTweetActivity)getActivity()).authAndRetweet(tweet.getTweetId(), true);
                                Log.e(""+LOG_TEXT,"isTweetOK: "+isTweekOk);
                                if(isTweekOk)
                                    ((AbstractTweetActivity)getActivity()).retweet(tweet.getTweetId());
                                return isTweekOk;
                            }

                            @Override
                            protected void onPostExecute(Boolean bool) {
                                super.onPostExecute(bool);
                                if(isAdded()) {
                                //    if (bool.booleanValue())
                                //        Toast.makeText(getActivity(), "Retweeted", Toast.LENGTH_SHORT).show();
                                //    else
                                //        Toast.makeText(getActivity(), "Sorry can't retweet right now", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }.execute();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            //    count = tweetDBHelper.getAllEntryCount(twitterHandleId, isPinned);
            //    Log.e("TwitterListsFragment -> getItemCount()","userHandle:" + twitterHandleId + ", isPinned: "+isPinned+" && count: "+count);
            return count;
        }

        @Override
        public void onViewRecycled(TwitterViewHolder holder) {
            ExpandedItemParams expandedItemParams = (ExpandedItemParams) holder.twitterUserImage.getTag();
            if(Logging.TwitterListsFragmentLogging) Log.e(LOG_TEXT+" onViewRecycled", "height: "+expandedItemParams.height + " and expanded: "
                    +expandedItemParams.isExpanded+", origHeight: "+expandedItemParams.originalHeight);
            holder.twitterUserImage.getLayoutParams().height = ITEM_HEIGHT;
            RelativeLayout llRetweet2 = (RelativeLayout) holder.llReTweet.findViewById(R.id.llRetweet2);
            llRetweet2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, 1));
            holder.llReTweet.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            super.onViewRecycled(holder);
        }

        @Override
        public void onViewDetachedFromWindow(TwitterViewHolder holder) {
            //    ExpandedItemParams expandedItemParams = (ExpandedItemParams) holder.twitterUserImage.getTag();
            //    Log.e(LOG_TEXT+" onViewDetachedFromWindow", "DETACHED height: "+expandedItemParams.height + "and expanded: "
            //            +expandedItemParams.isExpanded);
            super.onViewDetachedFromWindow(holder);
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        @Override
        public void onClick(View view) {
            animateExpansion(view);
        }

        public void animateExpansion(final View view){

            view.setVisibility(View.VISIBLE);
            if(Logging.TwitterListsFragmentLogging) Log.e(""+Logging.LOG_TwitterListsFragment,"card view clicked");

            ExpandedItemParams expandedItemParams = (ExpandedItemParams) view.getTag();
            boolean isExpanded = expandedItemParams.isExpanded;
            expandedItemParams.height = view.getHeight();
            //    boolean isExpanded = ((Boolean)view.getTag(1)).booleanValue();
            //    view.setTag(2,  new Integer(view.getHeight()));

            if (mOriginalHeight == 0)
                mOriginalHeight = view.getHeight();

            float offset = 30;
            ValueAnimator valueAnimator;
            if (!isExpanded) {
                isExpanded = true;
                valueAnimator = ValueAnimator.ofInt(mOriginalHeight, mOriginalHeight + (int) (mOriginalHeight * offset));
            } else {
                isExpanded = false;
                valueAnimator = ValueAnimator.ofInt(mOriginalHeight + (int) (mOriginalHeight * offset), mOriginalHeight);
            }
            valueAnimator.setDuration(300);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator animation) {
                    Integer value = (Integer) animation.getAnimatedValue();
                    view.getLayoutParams().height = value.intValue();
                    view.requestLayout();
                    if(Logging.TwitterListsFragmentLogging) Log.e(LOG_TEXT,"height: "+value.intValue());

                    RelativeLayout llRetweet2 = (RelativeLayout) view.findViewById(R.id.llRetweet2);
                    llRetweet2.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));
                }
            });
            valueAnimator.start();
            //    view.setTag(new Boolean(isExpanded));
            expandedItemParams.isExpanded = isExpanded;
        }

        // Twitterview holder
        public class TwitterViewHolder extends RecyclerView.ViewHolder {

            CardView cardView;
            ImageView twitterUserImage;
            LinearLayout tweetLayout;
            TextView tweeterNameTextView, tweetIdTextView, timeAgoTextView, tweetTextView;
            LinearLayout llReTweet;
            ImageView ivFavourite, ivRetweet, ivReply;

            public TwitterViewHolder(View itemView) {
                super(itemView);
                cardView = (CardView) itemView.findViewById(R.id.twitterCardView);
                tweetLayout = (LinearLayout) itemView.findViewById(R.id.tweetLayout);
                twitterUserImage = (ImageView) itemView.findViewById(R.id.twitterUserImage);
                tweeterNameTextView = (TextView) itemView.findViewById(R.id.tweeterNameTextView);
                tweetIdTextView = (TextView) itemView.findViewById(R.id.tweetIdTextView);
                timeAgoTextView = (TextView) itemView.findViewById(R.id.timeAgoTextView);
                tweetTextView = (TextView) itemView.findViewById(R.id.tweetTextView);
                llReTweet = (LinearLayout) itemView.findViewById(R.id.llReTweet);
                ivFavourite = (ImageView) itemView.findViewById(R.id.ivFavourite);
                ivRetweet = (ImageView) itemView.findViewById(R.id.ivRetweet);
                ivReply = (ImageView) itemView.findViewById(R.id.ivReply);
            }
        }

        public class ExpandedItemParams{

            public int height, originalHeight;
            public boolean isExpanded;

            public ExpandedItemParams(boolean isExpanded, int height, int originalHeight){
                this.originalHeight = originalHeight;
                this.height = height;
                this.isExpanded = isExpanded;
            }
        }
    }

    /************************************************************************
     * for formatting the date in the form 12 min delay
     ************************************************************************/
    private String formatTime(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long timeAgo = 0;
        String timeAgoStr = "";
        try {
            Date tweetDate = sdf.parse(date);
            Calendar cal = Calendar.getInstance();
            Date currentDate = cal.getTime();

            long diff = currentDate.getTime() - tweetDate.getTime();
            //    Log.e("TwitterListsFragment -> formatTime()","today date: "+currentDate.getTime()+" and "+sdf.format(currentDate));
            //    Log.e("TwitterListsFragment -> formatTime()","tweet Date: "+tweetDate.getTime()+" and "+sdf.format(tweetDate));
            if((diff / 1000) < 60){
                timeAgo = (diff / 1000);
                timeAgoStr = " sec";
            } else if((diff / (60 * 1000)) < 60){
                timeAgo = (diff / (60 * 1000));
                timeAgoStr = " min";
            } else if((diff / (60 * 60 * 1000)) < 24){
                timeAgo = (diff / (60 * 60 * 1000));
                timeAgoStr = " hrs";
            } else {
                timeAgo = diff / (24 * 60 * 60 * 1000);
                timeAgoStr = " days";
            }

            return timeAgo+timeAgoStr;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
