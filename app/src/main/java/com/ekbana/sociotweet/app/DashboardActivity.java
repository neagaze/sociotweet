package com.ekbana.sociotweet.app;

import android.content.Context;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.ekbana.sociotweet.config.Logging;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.customui.AnimatedExpandableListView;
import com.ekbana.sociotweet.customui.BottomTabLinearLayout;
import com.ekbana.sociotweet.db.TweetUsersDBHelper;
import com.ekbana.sociotweet.db.WeatherLocationDBHelper;
import com.ekbana.sociotweet.misc.SdcardHelper;
import com.ekbana.sociotweet.webapi.HttpConnection;
import com.ekbana.sociotweet.webapi.ParseJSON;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends ActionBarActivity implements View.OnClickListener{

    private static String LOG_TEXT = "DashboardActivity -> ";

    private Fragment weatherFragment;
    private FrameLayout frameLayout, twitterFrameLayout, pinnedTwitterFrameLayout;
    private FragmentManager fragmentManager;
    private BottomTabLinearLayout bottomTabLinearLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private RelativeLayout mDrawerRelativeLayout;
    private android.support.v7.widget.Toolbar toolbar;
    private AnimatedExpandableListView mDrawerList;

    private WeatherDataRetriever weatherDataRetriever;
    private TwitterDataRetriever twitterDataRetriever, twitterDataRetriever1;
    private LinearLayout aboutLayout, weatherLayout, twitterLayout, pinnedTweetLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        // Set a toolbar to replace the action bar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        init();
    }

    @Override
    protected void onDestroy() {
        twitterDataRetriever.cancel(true);
        twitterDataRetriever1.cancel(true);
        weatherDataRetriever.cancel(true);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    /***************************************************************************************
     * init
     ***************************************************************************************/
    private void init() {

        // for working with weather fragment
        frameLayout = (FrameLayout) findViewById(R.id.weatherFrameLayout);
        //    frameLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        //            SocioTweet.screenHeight * 3 / 10));

        twitterFrameLayout = (FrameLayout) findViewById(R.id.twitterFrameLayout);
        //    twitterFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        //            SocioTweet.screenHeight * 3 / 10));

        pinnedTwitterFrameLayout = (FrameLayout) findViewById(R.id.pinnedTwitterFrameLayout);
        //    pinnedTwitterFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
        //            SocioTweet.screenHeight * 3 / 10));

        View twitterHeaderLayout = findViewById(R.id.twitterHeaderLayout);
        TextView tweetHeaderTextView = (TextView) twitterHeaderLayout.findViewById(R.id.tweetHeaderTextView);
        tweetHeaderTextView.setText("LATEST TWEETS");
        Button moreTwitterButton = (Button) twitterHeaderLayout.findViewById(R.id.moreButton);
        moreTwitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, LatestTweetActivity.class);
                intent.putExtra("viewPagerDefaultPage", 0);
                startActivity(intent);
            }
        });

        View pinnedTwitterHeaderLayout = findViewById(R.id.pinnedTwitterHeaderLayout);
        TextView pinnedTweetHeaderTextView = (TextView) pinnedTwitterHeaderLayout.findViewById(R.id.tweetHeaderTextView);
        pinnedTweetHeaderTextView.setText("PINNED TWEETS");
        Button morePinnedTwitterButton = (Button) pinnedTwitterHeaderLayout.findViewById(R.id.moreButton);
        morePinnedTwitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, PinnedTweetActivity.class);
                intent.putExtra("viewPagerDefaultPage", 0);
                startActivity(intent);
            }
        });

        fragmentManager = getSupportFragmentManager();

        ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.progressDialogFragmentCreator(); //ProgressDialogFragment.getInstance();
        ProgressDialogFragment progressDialogFragment1 = ProgressDialogFragment.progressDialogFragmentCreator();
        ProgressDialogFragment progressDialogFragment2 = ProgressDialogFragment.progressDialogFragmentCreator();

        //     WeatherFragment weatherFragment = new WeatherFragment();
        fragmentManager.beginTransaction().replace(R.id.weatherFrameLayout, progressDialogFragment).commitAllowingStateLoss();
        fragmentManager.beginTransaction().replace(R.id.twitterFrameLayout, progressDialogFragment1).commitAllowingStateLoss();
        fragmentManager.beginTransaction().replace(R.id.pinnedTwitterFrameLayout, progressDialogFragment2).commitAllowingStateLoss();

        String params =  WeatherLocationDBHelper.getInstance(getApplicationContext()).getEnabledIds();
        weatherDataRetriever = new WeatherDataRetriever(this);
        weatherDataRetriever.execute(new String[] {UniversalVariables.WEATHER_API + params});

        String tweetParams = TweetUsersDBHelper.getInstance(getApplicationContext()).getEnabledIds(UniversalVariables.LATEST_TWEET_ID);
        Log.e(Logging.LOG_DashboardActivity, "tweetParams: "+tweetParams);
        twitterDataRetriever = new TwitterDataRetriever(this);
        twitterDataRetriever.execute(new String[] {UniversalVariables.TWEETS_API + tweetParams});

        String pinnedTweetParams = TweetUsersDBHelper.getInstance(getApplicationContext()).getEnabledIds(UniversalVariables.PINNED_TWEET_ID);
        Log.e(Logging.LOG_DashboardActivity, "pinnedTweetParams: "+pinnedTweetParams);
        twitterDataRetriever1 = new TwitterDataRetriever(this);
        twitterDataRetriever1.execute(new String[] {UniversalVariables.PINNED_TWEETS_API + pinnedTweetParams});

        aboutLayout = (LinearLayout) findViewById(R.id.aboutSelectLayout);
        weatherLayout = (LinearLayout) findViewById(R.id.weatherSelectLayout);
        twitterLayout = (LinearLayout) findViewById(R.id.twitterSelectLayout);
        pinnedTweetLayout = (LinearLayout) findViewById(R.id.pinnedTwitterSelectLayout);
        aboutLayout.setOnClickListener(this);
        weatherLayout.setOnClickListener(this);
        twitterLayout.setOnClickListener(this);
        pinnedTweetLayout.setOnClickListener(this);

        // navigation drawer toggle
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerRelativeLayout = (RelativeLayout)
                findViewById(R.id.left_drawer);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */) {

            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(R.string.app_name);
                ActivityCompat.invalidateOptionsMenu(DashboardActivity.this);
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(R.string.app_name);
                ActivityCompat.invalidateOptionsMenu(DashboardActivity.this);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

        mDrawerList = (AnimatedExpandableListView) findViewById(R.id.list_view_drawer);

        // initialize navigation drawer item
        setUpExpandableListView(mDrawerList);
    }

    /**
     * Set up expandable listview
     * **/
    private void setUpExpandableListView(final AnimatedExpandableListView expListView) {
        List<GroupItem> items = new ArrayList<GroupItem>();

        // Populate our list with groups and it's children
        GroupItem flightListItem = new GroupItem();

        // flight list
        flightListItem.title = "Settings";
        flightListItem.imageLogo = "settings";

        ChildItem child1 = new ChildItem();
        child1.title = "Weather";
        child1.logo = "weather_red";
        flightListItem.items.add(child1);
        ChildItem child2 = new ChildItem();
        child2.title = "Tweets";
        child2.logo = "twitter_red";
        flightListItem.items.add(child2);
        ChildItem child3 = new ChildItem();
        child3.title = "Pinned Tweets";
        child3.logo = "pinned_red";
        flightListItem.items.add(child3);

        items.add(flightListItem);

        final ExpandableListViewAdapter adapter = new ExpandableListViewAdapter(this);
        adapter.setData(items);

        expListView.setAdapter(adapter);

        // In order to show animations, we need to use a custom click handler
        // for our ExpandableListView.
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, final int groupPosition, long id) {

                int childCount = adapter.getRealChildrenCount(groupPosition);
                //    Log.e(LOG_TEXT,"group clicked at pos: "+groupPosition +" whose childCount is: "+childCount);

                // We call collapseGroupWithAnimation(int) and expandGroupWithAnimation(int) to animate group
                // expansion/collapse.
                if (expListView.isGroupExpanded(groupPosition)) {
                    expListView.collapseGroupWithAnimation(groupPosition);
                } else {
                    expListView.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int position, int childPosition, long l) {
                //    Log.e(LOG_TEXT,"child clicked at pos: "+position+" and child: "+childPosition);
                //   do your job when child is clicked
                if(childPosition == 0)
                    startActivity(new Intent(DashboardActivity.this, WeatherConfigurationActivity.class));
                else if(childPosition == 1)
                    startActivity(new Intent(DashboardActivity.this, TweetConfigActivity.class));
                else if(childPosition == 2)
                    startActivity(new Intent(DashboardActivity.this, PinnedTweetConfigActivity.class));
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view == aboutLayout){

            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        } else if(view == weatherLayout){

            Intent intent = new Intent(this, WeatherConfigurationActivity.class);
            startActivity(intent);
        } else if(view == twitterLayout){

            Intent intent = new Intent(this, TweetConfigActivity.class);
            startActivity(intent);
        } else if(view == pinnedTweetLayout){

            Intent intent = new Intent(this, PinnedTweetConfigActivity.class);
            startActivity(intent);
        }
    }

    /**
     * Expandable list View part
     * ****/
    private static class GroupItem {
        String title;
        String imageLogo;
        List<ChildItem> items = new ArrayList<ChildItem>();
    }

    private static class ChildItem {
        String title;
        String logo;
    }

    private static class ChildHolder {
        TextView title;
        ImageView logo;
    }

    private static class GroupHolder {
        TextView title;
        ImageView imageLogo;
    }

    /*******************************************************************************
     * Expandable adapter used in the Navigation drawer list view
     ***********************************************************************************/
    private class ExpandableListViewAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {

        private LayoutInflater inflater;

        private List<GroupItem> items;

        public ExpandableListViewAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> items) {
            this.items = items;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildHolder holder;
            ChildItem item = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.expandable_list_item_drawer, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.logo = (ImageView) convertView.findViewById(R.id.listLogo);
                convertView.setTag(holder);
            } else {
                holder = (ChildHolder) convertView.getTag();
            }

            holder.title.setText(item.title);
            // using default image from drawables folder
            holder.logo.setBackgroundResource(getResources().
                    getIdentifier(item.logo, "drawable",getPackageName()));

            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {

            return items.get(groupPosition).items.size();
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder holder;
            GroupItem item = getGroup(groupPosition);
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.expandable_group_item_drawer, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.imageLogo = (ImageView) convertView.findViewById(R.id.listLogo);
                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }

            holder.title.setText(item.title);
            holder.imageLogo.setBackgroundResource(getResources().getIdentifier(item.imageLogo, "drawable",
                    getPackageName()));

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }

    }
/*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }
*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            startActivity(new Intent(DashboardActivity.this, FollowerSelectActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*************************************************************************************************
     * Weather Data Retriever
     *************************************************************************************/
    public class WeatherDataRetriever extends WebApiRetriever{

        public WeatherDataRetriever(Context context) {
            super(context);
            setProgressShow(false);
            setOnBackgroundListener(new OnBackgroundDoCallback() {
                @Override
                public boolean executeBackgroundTask(String url) {
                    boolean dataFromServer = retrieveDataFromServer(url);
                    return dataFromServer;
                }
            });
        }

        // for checking the internet connectivity and retrieve data from server
        private boolean retrieveDataFromServer(String url) {

            HttpConnection connection = HttpConnection.getSingletonConn();
            boolean isValid = false;
            try {
                if (/*false && */HttpConnection.getConnectionAvailable(context)) {

                    String connected = connection.getJSONusingGET(url);
                    isValid = ParseJSON.isJSONValid(connected);
                    Uri uri = Uri.parse(url);
                    String params = uri.getEncodedQuery();
                    Log.e(LOG_TEXT + "retrieveDataFromServer()","params: "+params);
                    if(isValid) {
                        ParseJSON.parseWeather(connected);
                    } else
                        Log.e(LOG_TEXT + "retrieverDataFromServer()","JSON is invalid");

                    // save the new db in sdcard as well
                    SdcardHelper sdcardHelper = new SdcardHelper();
                    sdcardHelper.saveDbInSdcard(context.getApplicationContext());
                }
            } catch (Exception e) {
                Log.e("Exception caught", "" + e.getMessage());
                e.printStackTrace();
            } finally {
                return isValid;
            }
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);

            if(!isCancelled() && this != null && fragmentManager != null) {
                WeatherFragment weatherFragment = new WeatherFragment();
                //    fragmentManager.popBackStack();
                fragmentManager.beginTransaction().replace(R.id.weatherFrameLayout, weatherFragment).commitAllowingStateLoss();
            }
        }
    }

    /**************************************************************************************
     * Twitter Data Retriever
     *************************************************************************************/
    public class TwitterDataRetriever extends WebApiRetriever{

        private int type;

        public TwitterDataRetriever(Context context) {
            super(context);
            setProgressShow(false);
            setOnBackgroundListener(new OnBackgroundDoCallback() {
                @Override
                public boolean executeBackgroundTask(String url) {
                    return retrieveDataFromServer(url);
                }
            });
        }

        // for checking the internet connectivity and retrieve data from server
        private boolean retrieveDataFromServer(String url) {

            HttpConnection connection = HttpConnection.getSingletonConn();
            boolean isValid = false;
            try {

                Uri uri = Uri.parse(url);
                String params = uri.getEncodedQuery();
                String tempUrl = "http://"+uri.getHost()+uri.getPath();

                if(Logging.DashboardLogging) {
                    Log.e(LOG_TEXT + "TwitterDataRetriever", "params: " + params);
                    Log.e(LOG_TEXT + "TwitterDataRetriever", "path: " + tempUrl);
                }

                if(tempUrl.equals(UniversalVariables.TWEETS_API))
                    type = 0;
                else if(tempUrl.equals(UniversalVariables.PINNED_TWEETS_API))
                    type = 1;

                if (HttpConnection.getConnectionAvailable(context)) {

                    String connected = connection.getJSONusingGET(url);
                    isValid = ParseJSON.isJSONValid(connected);

                    if(isValid) {

                        if(tempUrl.equals(UniversalVariables.TWEETS_API)) {
                            /*isValid = */ParseJSON.parseTweets(connected, false);
                        } else if(tempUrl.equals(UniversalVariables.PINNED_TWEETS_API)) {
                            /*isValid = */ ParseJSON.parseTweets(connected, true);
                        }
                    } else
                        Log.e(LOG_TEXT + "TwitterDataRetriever","JSON is invalid");

                    // save the new db in sdcard as well
                    SdcardHelper sdcardHelper = new SdcardHelper();
                    sdcardHelper.saveDbInSdcard(context.getApplicationContext());
                }
            } catch (Exception e) {
                Log.e(LOG_TEXT + "TwitterDataRetriever: ", "" + e.getMessage());
                e.printStackTrace();
            } finally {
                return isValid;
            }
        }

        @Override
        protected void onPostExecute(Boolean isValid) {
            super.onPostExecute(isValid);

            if(!isCancelled() && this != null && fragmentManager != null) {

                Bundle bundle = new Bundle();
                TwitterFragment twitterFragment = new TwitterFragment();

                if (type == 0) {
                    bundle.putInt("position", 0);
                    twitterFragment.setArguments(bundle);
                    fragmentManager.beginTransaction().replace(R.id.twitterFrameLayout, twitterFragment)
                            .commitAllowingStateLoss();
                } else {
                    bundle.putInt("position", 1);
                    twitterFragment.setArguments(bundle);
                    fragmentManager.beginTransaction().replace(R.id.pinnedTwitterFrameLayout, twitterFragment)
                            .commitAllowingStateLoss();
                }
            }
        }
    }

    /**************************************************************************************
     * for showing the progress dialog fragment when loading data in fragment
     *************************************************************************************/
    public static class ProgressDialogFragment extends Fragment{

        private static ProgressDialogFragment progressDialogFragment;

        public static ProgressDialogFragment getInstance(){
            if(progressDialogFragment == null)
                progressDialogFragment = new ProgressDialogFragment();
            return progressDialogFragment;

        }

        public static ProgressDialogFragment progressDialogFragmentCreator(){
            return new ProgressDialogFragment();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            LinearLayout baseLayout = new LinearLayout(getActivity());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            baseLayout.setLayoutParams(lp);
            baseLayout.setGravity(Gravity.CENTER);

            ProgressBar pBar = new ProgressBar(getActivity());
            pBar.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            pBar.setIndeterminate(true);
            baseLayout.addView(pBar);

            return baseLayout;
        }

    }
}
