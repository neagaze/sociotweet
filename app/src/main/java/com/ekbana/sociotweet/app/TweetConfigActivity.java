package com.ekbana.sociotweet.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.ekbana.sociotweet.cache.ImageLoader;
import com.ekbana.sociotweet.config.Logging;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.customui.DividerItemDecoration;
import com.ekbana.sociotweet.db.TweetUsersDBHelper;
import com.ekbana.sociotweet.db.WeatherLocationDBHelper;
import com.ekbana.sociotweet.model.TweetUser;
import com.ekbana.sociotweet.model.WeatherLocation;
import com.ekbana.sociotweet.webapi.HttpConnection;
import com.ekbana.sociotweet.webapi.ParseJSON;


public class TweetConfigActivity extends ActionBarActivity {

    private static int TWEET_CONFIG_ID = 0;
    private static String LOG_TEXT = "TweetConfigActivity -> ";
    private RecyclerView tweetConfigRecyclerView;
    private static TweetUserAddDialog tweetUserAddDialog;
    private static TweetUserDeleteDialog tweetUserDeleteDialog;
    private TweetConfigRecyclerViewAdapter adapter;

    private boolean hasUserChangedValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_configuration);

        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tweet_config_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add_tweet_user) {
            tweetUserAddDialog = new TweetUserAddDialog();
            tweetUserAddDialog.setOnUserChangedValue(new TweetUserAddDialog.OnUserChangedValue() {
                @Override
                public void setUserChangedValue(boolean val) {
                    hasUserChangedValue = val;
                }
            });
            tweetUserAddDialog.show(getSupportFragmentManager(), "Tweet User Dialog");
            //    startActivity(new Intent(this, FollowerSelectActivity.class));
            return true;

        } else if(id == android.R.id.home) {
            if(!hasUserChangedValue) {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.tweetConfig);

        tweetConfigRecyclerView = (RecyclerView) findViewById(R.id.weatherConfigRecyclerView);
        tweetConfigRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        tweetConfigRecyclerView.setLayoutManager(llm);
        adapter = new TweetConfigRecyclerViewAdapter();
        tweetConfigRecyclerView.setAdapter(adapter);
        tweetConfigRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        hasUserChangedValue = false;
    }

    @Override
    public void onBackPressed() {
        if(hasUserChangedValue) {
            finish();
            startActivity(new Intent(this, DashboardActivity.class));
        }
        super.onBackPressed();
    }

    /**
     * RecyclerView adapter
     */
    public class TweetConfigRecyclerViewAdapter extends RecyclerView.Adapter<TweetConfigRecyclerViewAdapter.ConfigViewHolder>{

        private TweetUsersDBHelper tweetUsersDBHelper;
        private ImageLoader imageLoader;

        public TweetConfigRecyclerViewAdapter(){
            tweetUsersDBHelper = TweetUsersDBHelper.getInstance(getApplicationContext());
            this.imageLoader = new ImageLoader(TweetConfigActivity.this);
        }

        @Override
        public ConfigViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tweet_config_layout, parent, false);
            ConfigViewHolder configViewHolder = new ConfigViewHolder(v);
            return configViewHolder;
        }

        @Override
        public void onBindViewHolder(final ConfigViewHolder holder, int position) {

            final TweetUser tweetUser = tweetUsersDBHelper.getEnabledEntry(0, position);

            if(tweetUser != null) {
                holder.userNameTextView.setText(tweetUser.getDisplayname());
                imageLoader.DisplayImage(tweetUser.getProfile_image(), holder.imageView);
                holder.userHandleTextView.setText(tweetUser.getHandle());

                holder.tweetUserDeleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                /*        tweetUsersDBHelper.deleteTweetUser(tweetUser.getHandle());
                        adapter.notifyDataSetChanged();
                */
                        tweetUserDeleteDialog = new TweetUserDeleteDialog();
                        Bundle bundle = new Bundle();
                        bundle.putString("handle",tweetUser.getHandle());
                        tweetUserDeleteDialog.setArguments(bundle);
                        tweetUserDeleteDialog.setOnNotifyAdapterChangeListener(new TweetUserDeleteDialog.OnNotifyAdapterChange() {
                            @Override
                            public void setOnNotifyAdapterChangeListener() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                        tweetUserDeleteDialog.show(getSupportFragmentManager(), "Tweet User Delete Dialog");

                        hasUserChangedValue = true;
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return tweetUsersDBHelper.getAllEntryCount(TWEET_CONFIG_ID);
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        // custom RecyclerView adapter
        public class ConfigViewHolder extends RecyclerView.ViewHolder{

            ImageView imageView;
            TextView userNameTextView;
            TextView userHandleTextView;
            Button tweetUserDeleteButton;

            public ConfigViewHolder(View itemView) {
                super(itemView);
                imageView = (ImageView) itemView.findViewById(R.id.tweetUserIcon);
                userNameTextView = (TextView) itemView.findViewById(R.id.tweetUserNameTextView);
                userHandleTextView = (TextView) itemView.findViewById(R.id.tweetUserHandleTextView);
                tweetUserDeleteButton = (Button) itemView.findViewById(R.id.tweetUserDeleteButton);
            }
        }
    }

    /**
     * Dialog for adding tweet users
     */
    public static class TweetUserAddDialog extends DialogFragment {

        private Dialog dialog;
        private OnUserChangedValue onUserChangedValue;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.add_tweet_user, container);

            final ProgressBar addUserProgressBar = (ProgressBar) view.findViewById(R.id.addUserProgressBar);
            Button addTweetUserButton = (Button)view.findViewById(R.id.addTweetUserButton);
            final EditText tweetUserHandleEditText = (EditText)view.findViewById(R.id.tweetUserHandleEditText);

            addTweetUserButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TweetConfigRetriever tweetConfigRetriever = new TweetConfigRetriever(SocioTweet.getContext());
                    tweetConfigRetriever.execute(new String[]{UniversalVariables.TWEETS_API+"?handle="
                            +tweetUserHandleEditText.getText().toString()});
                    addUserProgressBar.setVisibility(View.VISIBLE);

                //    hasUserChangedValue = true;
                    onUserChangedValue.setUserChangedValue(true);
                }
            });

            return view;
        }

        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState) {

            /** To bring front the Dialog box **/
            dialog = new Dialog(getActivity(), R.style.DialogStyle);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);

            return dialog;
        }

        public interface OnUserChangedValue{
            public void setUserChangedValue(boolean val);
        }

        public void setOnUserChangedValue(OnUserChangedValue onUserChangedValue){
            this.onUserChangedValue = onUserChangedValue;
        }
    }


    /**
     * Dialog for deleting tweet users
     */
    public static class TweetUserDeleteDialog extends DialogFragment {

        private Dialog dialog;
        private OnNotifyAdapterChange onNotifyAdapterChange;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            final String handle = getArguments().getString("handle");

            View view = inflater.inflate(R.layout.delete_tweet_user, container);

            Button cancelTweetDeleteButton = (Button)view.findViewById(R.id.cancelTweetDeleteButton);
            Button deleteTweetUserButton = (Button)view.findViewById(R.id.deleteTweetUserButton);

            cancelTweetDeleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });

            deleteTweetUserButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(!TweetUsersDBHelper.getInstance(getActivity().getApplicationContext()).deleteTweetUser(handle))
                        Toast.makeText(getActivity(), "Can't delete the last user", Toast.LENGTH_SHORT).show();
                    onNotifyAdapterChange.setOnNotifyAdapterChangeListener();
                    dismiss();
                }
            });

            return view;
        }

        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState) {

            /** To bring front the Dialog box **/
            dialog = new Dialog(getActivity(), R.style.DialogStyle);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);

            return dialog;
        }

        public static interface OnNotifyAdapterChange{
            public void setOnNotifyAdapterChangeListener();
        }

        public void setOnNotifyAdapterChangeListener(OnNotifyAdapterChange onNotifyAdapterChange){
            this.onNotifyAdapterChange = onNotifyAdapterChange;
        }

    }

    /************************************************************************
     * A new async task class that pulls data from server and saves it
     *************************************************************************/
    private static class TweetConfigRetriever extends WebApiRetriever{

        public TweetConfigRetriever(Context context) {
            super(context);

            setProgressShow(false);
            setOnBackgroundListener(new OnBackgroundDoCallback() {
                @Override
                public boolean executeBackgroundTask(String url) {
                    return retrieveDataFromServer(url);
                }
            });
        }

        /**
         * for checking the internet connectivity and retrieve data from server
         * **/
        private boolean retrieveDataFromServer(String url) {

            boolean returnVal = true;
            HttpConnection connection = HttpConnection.getSingletonConn();
            try {
                if (HttpConnection.getConnectionAvailable(context)) {

                    String connected = connection.getJSONusingGET(url);
                    Log.e("TweetConfigActivity", "new tweet: "+connected);
                    if(ParseJSON.isJSONValid(connected)) {
                        TweetUser user = ParseJSON.setNewTweetUser(connected);
                        if(user == null)
                            returnVal = false;
                    } else
                        returnVal = false;
                }
            } catch (Exception e) {
                Log.e("Exception caught", "" + e.getMessage());
                e.printStackTrace();
            } finally {
                return returnVal;
            }
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
            if(tweetUserAddDialog != null && tweetUserAddDialog.isVisible())
                tweetUserAddDialog.dismiss();

            if(!s)
                Toast.makeText(context, "Invalid tweet username", Toast.LENGTH_SHORT).show();
        }

    }
}
