package com.ekbana.sociotweet.app;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.ekbana.sociotweet.cache.ImageLoader;
import com.ekbana.sociotweet.model.OtherApps;

import java.lang.ref.WeakReference;

/**
 * Created by nigesh on 5/19/15.
 */
public class OtherEkbanaAppsRetriever extends OtherEkbanaAppsRetrieverOnce {

    private TableLayout tableLayoutOtherApps;

    public OtherEkbanaAppsRetriever(Context context, TableLayout tableLayoutOtherApps) {
        super(context);
        this.tableLayoutOtherApps =  new WeakReference<TableLayout>(tableLayoutOtherApps).get(); //tableLayoutOtherApps;
    }

    @Override
    protected void onPostExecute(String str) {
        super.onPostExecute(str);

        if(context != null && otherAppsList != null && (otherAppsList.size() != 0)) {

            ImageLoader imageLoader = new ImageLoader(context);

            for (final OtherApps otherApp : otherAppsList) {

                LayoutInflater inflater = (LayoutInflater) context.getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE);
                TableRow tableRow = (TableRow) inflater.inflate(R.layout.other_apps_layout, null);

                ImageView appIcon = (ImageView) tableRow.findViewById(R.id.appIcon);
                imageLoader.DisplayImage(otherApp.getImage(), appIcon);

                TextView appName = (TextView) tableRow.findViewById(R.id.appName);
                appName.setText(otherApp.getTitle());

                TextView appDesc = (TextView) tableRow.findViewById(R.id.appDesc);
                appDesc.setText(otherApp.getDesc());

                tableRow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (otherApp.getUrl() != null && !otherApp.getUrl().isEmpty()) {
                            String uri = otherApp.getUrl();
                            try {
                                Intent viewIntent =
                                        new Intent("android.intent.action.VIEW",
                                                Uri.parse(uri));
                                ((AboutActivity)context).startActivity(viewIntent);

                            } catch (Exception e) {
                                Toast.makeText(context.getApplicationContext(), "Unable to Connect Try Again...",
                                        Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(context, "Link to this app is not available currently", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                if(tableLayoutOtherApps != null) {
                    TableLayout.LayoutParams lp = new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    lp.setMargins(0, 0, 0, 0);
                    tableLayoutOtherApps.addView(tableRow, lp);
                }
            }
        }
    }
}
