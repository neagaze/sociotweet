package com.ekbana.sociotweet.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.*;
import com.ekbana.sociotweet.cache.ImageLoader;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.customui.DividerItemDecoration;
import com.ekbana.sociotweet.db.TweetUsersDBHelper;
import com.ekbana.sociotweet.model.TweetUser;


public class PinnedTweetConfigActivity extends ActionBarActivity {

    private static String LOG_TEXT = "PinnedTweetConfigActivity -> ";
    private RecyclerView tweetConfigRecyclerView;
    private PinnedTweetConfigRecyclerViewAdapter adapter;

    private boolean hasUserChangedValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_configuration);
        init();
    }

    private void init() {

        // Set a toolbar to replace the action bar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.tweetConfig);

        tweetConfigRecyclerView = (RecyclerView) findViewById(R.id.weatherConfigRecyclerView);
        tweetConfigRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        tweetConfigRecyclerView.setLayoutManager(llm);
        adapter = new PinnedTweetConfigRecyclerViewAdapter();
        tweetConfigRecyclerView.setAdapter(adapter);
        tweetConfigRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        hasUserChangedValue= false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            if(!hasUserChangedValue) {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(hasUserChangedValue) {
            finish();
            startActivity(new Intent(this, DashboardActivity.class));
        }
        super.onBackPressed();
    }

    /**
     * RecyclerView adapter
     */
    public class PinnedTweetConfigRecyclerViewAdapter extends RecyclerView.Adapter<PinnedTweetConfigRecyclerViewAdapter.ConfigViewHolder>{

        private TweetUsersDBHelper tweetUsersDBHelper;
        private ImageLoader imageLoader;

        public PinnedTweetConfigRecyclerViewAdapter(){
            tweetUsersDBHelper = TweetUsersDBHelper.getInstance(getApplicationContext());
            this.imageLoader = new ImageLoader(PinnedTweetConfigActivity.this);
        }

        @Override
        public ConfigViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pinned_tweet_config_layout, parent, false);
            ConfigViewHolder configViewHolder = new ConfigViewHolder(v);
            return configViewHolder;
        }

        @Override
        public void onBindViewHolder(final ConfigViewHolder holder, int position) {

            final TweetUser tweetUser = tweetUsersDBHelper.getEntry(UniversalVariables.PINNED_TWEET_ID, position);

            if(tweetUser != null) {
                holder.userNameTextView.setText(tweetUser.getDisplayname());
                imageLoader.DisplayImage(tweetUser.getProfile_image(), holder.imageView);
                holder.userHandleTextView.setText(tweetUser.getHandle());
                holder.tweetUserCheckBox.setChecked(tweetUser.isSelected());
                holder.tweetUserCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                        // if there is only 1 enabled pinned tweet don't disable
                        if(tweetUsersDBHelper.getAllEntryCount(UniversalVariables.PINNED_TWEET_ID) <= 1 && b == false)
                            Toast.makeText(PinnedTweetConfigActivity.this, "There should be at least one Pinned Tweet", Toast.LENGTH_SHORT).show();
                        else
                            tweetUsersDBHelper.editCheckState(tweetUser.getId(), b);

                        hasUserChangedValue = true;
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return tweetUsersDBHelper.getAllEntryCount(UniversalVariables.PINNED_TWEET_ID);
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
        }

        // custom RecyclerView adapter
        public class ConfigViewHolder extends RecyclerView.ViewHolder{

            ImageView imageView;
            TextView userNameTextView;
            TextView userHandleTextView;
            CheckBox tweetUserCheckBox;

            public ConfigViewHolder(View itemView) {
                super(itemView);
                imageView = (ImageView) itemView.findViewById(R.id.tweetUserIcon);
                userNameTextView = (TextView) itemView.findViewById(R.id.tweetUserNameTextView);
                userHandleTextView = (TextView) itemView.findViewById(R.id.tweetUserHandleTextView);
                tweetUserCheckBox = (CheckBox) itemView.findViewById(R.id.tweetUserUnselectCheckBox);
            }
        }
    }
}
