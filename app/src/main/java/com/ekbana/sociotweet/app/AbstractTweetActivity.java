package com.ekbana.sociotweet.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.astuetz.PagerSlidingTabStrip;
import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.db.TweetDBHelper;
import com.ekbana.sociotweet.db.TweetUsersDBHelper;
import com.ekbana.sociotweet.model.TweetUser;
import android.support.v7.widget.Toolbar;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;


public abstract class AbstractTweetActivity extends ActionBarActivity {

    protected ViewPager twitterViewPager;
    protected PagerSlidingTabStrip twitterPagerTitleStrip;
    protected TwitterViewPagerAdapter twitterFragmentAdapter;
    protected Toolbar toolbar;

    protected  OnTweetTypeListener onTweetTypeListener;
    protected int viewPagerPos;

    protected Context context;
    protected Twitter niceTwitter;
    protected RequestToken niceRequestToken;
    protected SharedPreferences tweetPrefs;

    private String LOG_TAG = "AbstractTweetActivity";
    private static String TWEET_ID = "tweet_id";

    private String currentTweetId;

    private boolean isCurrentTweetPendingRetweet;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_tweet_layout);

        Bundle bundle = getIntent().getExtras();
        viewPagerPos = bundle.getInt("viewPagerDefaultPage", 0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        twitterViewPager = (ViewPager) findViewById(R.id.twitterViewPager);
        twitterPagerTitleStrip = (PagerSlidingTabStrip) findViewById(R.id.twitterPagerTitleStrip);
        //    twitterViewPager.setLayoutParams(new LinearLayout.LayoutParams(SocioTweet.screenWidth,
        //            SocioTweet.screenHeight * 3 / 10));

        twitterFragmentAdapter = new TwitterViewPagerAdapter(this
                .getSupportFragmentManager());
        twitterViewPager.setAdapter(twitterFragmentAdapter);
        //    twitterViewPager.setId(position + 1);
        // sliding tab adapter
        twitterPagerTitleStrip.setViewPager(twitterViewPager);
        twitterPagerTitleStrip.setIndicatorHeight(3);
        twitterPagerTitleStrip.setIndicatorColor(getResources().getColor(R.color.toolbar_blue));
        twitterPagerTitleStrip.setDividerColor(getResources().getColor(R.color.gray9));
        twitterPagerTitleStrip.setTextColor(getResources().getColor(R.color.toolbar_blue));

        twitterViewPager.setCurrentItem(viewPagerPos, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if(id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /****************************************************************************************
     * TwitterViewPagerAdapter implementation
     ****************************************************************************************/
    public class TwitterViewPagerAdapter extends FragmentStatePagerAdapter {

        private TweetUsersDBHelper tweetUsersDBHelper;

        public TwitterViewPagerAdapter(FragmentManager fm) {
            super(fm);
            tweetUsersDBHelper = TweetUsersDBHelper.getInstance(getApplicationContext());
        }

        @Override
        public Fragment getItem(int position) {
            return onTweetTypeListener.setFragment(position);
        }

        @Override
        public int getCount() {
            return onTweetTypeListener.setCount();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return onTweetTypeListener.setPageTitle(position);
        }

        public TweetUsersDBHelper getTweetUsersDBHelper(){
            return tweetUsersDBHelper;
        }
    }

    public void setOnTweetTypeListener(OnTweetTypeListener onTweetTypeListener){
        this.onTweetTypeListener = onTweetTypeListener;
    }

    protected static interface OnTweetTypeListener{
        public Fragment setFragment(int position);
        public int setCount();
        public String setPageTitle(int position);
    }

    /**
     * For authAndRetweet a tweet
     */
    public boolean authAndRetweet(String tweetId, boolean isRetweet) {

        currentTweetId = tweetId;

        isCurrentTweetPendingRetweet = isRetweet;
        boolean isCredentialSaved = false;

        //get the preferences for the app
        tweetPrefs = getSharedPreferences("TwitNicePrefs", 0);

        if (tweetPrefs.getString("user_token", null) == null) {
            niceTwitter = new TwitterFactory().getInstance();
            niceTwitter.setOAuthConsumer(UniversalVariables.TWIT_KEY, UniversalVariables.TWIT_SECRET);
            try {
                if(this instanceof LatestTweetActivity) {
                    niceRequestToken = niceTwitter.getOAuthRequestToken(UniversalVariables.TWIT_URL);
                    Log.e(""+LOG_TAG,"LatestTweetActivity is used");
                } else if(this instanceof PinnedTweetActivity) {
                    niceRequestToken = niceTwitter.getOAuthRequestToken(UniversalVariables.PINNED_TWIT_URL);
                    Log.e(""+LOG_TAG,"PinnedTweetActivity is used");
                }

                String authURL = niceRequestToken.getAuthenticationURL();
                Log.e(""+LOG_TAG,"callback URI: "+authURL);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(authURL));
                intent.putExtra(TWEET_ID, tweetId);
                startActivity(intent);
                //    startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse(authURL)), 100);
            } catch (TwitterException te) {
                Log.e(LOG_TAG, "TE " + te.getMessage());
                isCredentialSaved = false;
                //    Toast.makeText(context, "Sorry can't retweet right now", Toast.LENGTH_SHORT).show();
            }

        } else {
            Log.e(LOG_TAG, "authentication is saved in device");
            //    retweet(tweetId);
            isCredentialSaved = true;
        }

        return isCredentialSaved;
    }


    /**
     * For favouriting the tweet
     * @param tweetId
     */
    public void makeFavorite(final String tweetId){

        RetweetAndFavouriteTask task = new RetweetAndFavouriteTask(RetweetAndFavouriteTask.FAV_ID);
        task.execute(new String[]{tweetId});
    }

    /**
     * For retweeting the tweet
     * @param tweetId
     */
    public void retweet(final String tweetId){

        RetweetAndFavouriteTask task = new RetweetAndFavouriteTask(RetweetAndFavouriteTask.REWEET_ID);
        task.execute(new String[]{tweetId});
    }

    /**
     * For retweeting and favouriting tweets
     */
    public class RetweetAndFavouriteTask extends AsyncTask<String, Void, Boolean>{

        private int type;
        public static final int REWEET_ID = 0;
        public static final int FAV_ID = 1;

        private String failedTweetMsg = "Sorry can't retweet right now";

        public RetweetAndFavouriteTask(int type){
            this.type = type;
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            boolean isTweetActionDone = true;

            String tweetId = strings[0];

            String userToken = tweetPrefs.getString("user_token", null);
            String userSecret = tweetPrefs.getString("user_secret", null);

            Configuration twitConf = new ConfigurationBuilder()
                    .setOAuthConsumerKey(UniversalVariables.TWIT_KEY)
                    .setOAuthConsumerSecret(UniversalVariables.TWIT_SECRET)
                    .setOAuthAccessToken(userToken)
                    .setOAuthAccessTokenSecret(userSecret)
                    .build();

            Twitter retweetTwitter = new TwitterFactory(twitConf).getInstance();
            try {
                if(type == REWEET_ID) {
                    retweetTwitter.retweetStatus(Long.parseLong(tweetId));
                    Log.e(LOG_TAG, "retweet success !!");
                } else if(type == FAV_ID){
                    retweetTwitter.createFavorite(Long.parseLong(tweetId));
                    Log.e(LOG_TAG, "favourite success !!"+tweetId);
                }

            } catch(TwitterException te) {
                Log.e(LOG_TAG, te.getMessage());
                failedTweetMsg = te.getErrorMessage();
                //    Toast.makeText(context, ""+failedTweetMsg, Toast.LENGTH_SHORT).show();
                isTweetActionDone = false;
            }

            return isTweetActionDone;
        }

        @Override
        protected void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool.booleanValue())
                Toast.makeText(AbstractTweetActivity.this, type == REWEET_ID ? "Retweeted" : "Favourited",
                        Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(AbstractTweetActivity.this, "Sorry can't "+((type == REWEET_ID) ? "retweet" :
                        "make favourite" )+". "+failedTweetMsg, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * onNewIntent fires when user returns from Twitter authentication Web page
     */
    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);

        final String tweetId = currentTweetId; //intent.getStringExtra(TWEET_ID);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                //get the retrieved data
                Uri twitURI = intent.getData();
                Log.e(""+LOG_TAG,"twitURI: "+twitURI);

                //make sure the url is correct
                if(twitURI != null && twitURI.toString().startsWith( ((AbstractTweetActivity.this instanceof
                        LatestTweetActivity) ? UniversalVariables.TWIT_URL : UniversalVariables.PINNED_TWIT_URL))) {
                    //is verification - get the returned data
                    String oaVerifier = twitURI.getQueryParameter("oauth_verifier");
                    try {
                        //try to get an access token using the returned data from the verification page
                        AccessToken accToken = niceTwitter.getOAuthAccessToken(niceRequestToken, oaVerifier);

                        String userToken = accToken.getToken();
                        String userSecret = accToken.getTokenSecret();
                        tweetPrefs.edit()
                                .putString("user_token", userToken)
                                .putString("user_secret", userSecret)
                                .commit();
/*

                        Log.e(LOG_TAG, "tweet authentication successful !!");
                        Configuration twitConf = new ConfigurationBuilder()
                                .setOAuthConsumerKey(UniversalVariables.TWIT_KEY)
                                .setOAuthConsumerSecret(UniversalVariables.TWIT_SECRET)
                                .setOAuthAccessToken(userToken)
                                .setOAuthAccessTokenSecret(userSecret)
                                .build();

                        Twitter retweetTwitter = new TwitterFactory(twitConf).getInstance();
                        try {
                            //authAndRetweet, passing the status ID from the tag
                            retweetTwitter.retweetStatus(Long.parseLong(tweetId));

                        } catch(TwitterException te) {Log.e(LOG_TAG, te.getMessage());}
*/
                        if(isCurrentTweetPendingRetweet)
                            retweet(tweetId);
                        else
                            makeFavorite(tweetId);
                    }
                    catch (TwitterException te) {
                        Log.e(LOG_TAG, "Failed to get access token: " + te.getMessage());
                        //    Toast.makeText(context, "Sorry unable to access twitter ", Toast.LENGTH_SHORT).show();
                    }
                }

                return null;
            }
        }.execute();
    }
}
