/***
 *
 * "The day you stop learning is one day closer to writing your own name on your tombstone !!" - @neaGaze
 *
 * @author: Nigesh Shakya a.k.a neaGaze
 * Date: 18 Feb 2015 2:00 PM
 *
 *****/

package com.ekbana.sociotweet.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.ekbana.sociotweet.config.UniversalVariables;
import com.ekbana.sociotweet.db.WeatherLocationDBHelper;
import com.ekbana.sociotweet.webapi.HttpConnection;
import com.ekbana.sociotweet.webapi.ParseJSON;

public class SplashScreenActivity extends Activity {

    private ImageView imageViewSplashScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        imageViewSplashScreen = (ImageView) findViewById(R.id.imageViewSplashScreen);

        TweetWeatherConfigRetriever tweetWeatherConfigRetriever = new TweetWeatherConfigRetriever(this, imageViewSplashScreen);
        tweetWeatherConfigRetriever.execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    /************************************************************************
     * A new async task class that pulls data from server
     *************************************************************************/
    private class TweetWeatherConfigRetriever extends WebApiRetriever{

        private ImageView imageViewSplashScreen;

        public TweetWeatherConfigRetriever(Context context,  ImageView imageViewSplashScreen) {
            super(context);
            this.imageViewSplashScreen = imageViewSplashScreen;

            setProgressShow(false);
            setOnBackgroundListener(new OnBackgroundDoCallback() {
                @Override
                public boolean executeBackgroundTask(String url) {
                    //    rotateImage();
                    boolean dataFromServer = retrieveDataFromServer(url);

                    return dataFromServer;
                }
            });
        }

        public void execute(){
            execute(new String[]{UniversalVariables.WEATHER_LISTS_API, UniversalVariables.PINNED_USERS_API});
        }

        private void rotateImage() {
            if(imageViewSplashScreen != null) {
                Animation animation = AnimationUtils.loadAnimation(SplashScreenActivity.this, android.R.anim.cycle_interpolator);
                imageViewSplashScreen.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        imageViewSplashScreen.startAnimation(animation);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }

        /**
         * for checking the internet connectivity and retrieve data from server
         * **/
        private boolean retrieveDataFromServer(String url) {

            HttpConnection connection = HttpConnection.getSingletonConn();
            boolean dataFound = false;
            try {
                if (HttpConnection.getConnectionAvailable(context)) {

                    String connected = connection.getJSONusingGET(url);
                    dataFound = ParseJSON.isJSONValid(connected);
                    //    Log.e("JSON: ",dataFound + ": "+connected);
                    ParseJSON.setWeatherPreferences(connected, SplashScreenActivity.this.getApplicationContext());
                }
            } catch (Exception e) {
                Log.e("Exception caught", "" + e.getMessage());
                e.printStackTrace();
            } finally {
                return checkInDB();
                //    return dataFound;
            }
        }

        private boolean checkInDB() {
            WeatherLocationDBHelper weatherLocationDBHelper = WeatherLocationDBHelper.getInstance(context);
            int count = weatherLocationDBHelper.getAllEntryCount();
            if(count > 0)
                return true;
            else return false;
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
        /*
            Intent intent = new Intent(context, DashboardActivity.class);
            if(s.booleanValue()){
                ((SplashScreenActivity) context).startActivity(intent);
            }else
                Log.e("OnPostExecute()","Problem downloading calls from api");
            finish();
        */
            TweetConfigRetriever tweetConfigRetriever = new TweetConfigRetriever(SocioTweet.getContext());
            tweetConfigRetriever.execute(new String[]{UniversalVariables.TWEETS_API+"?handle="
                    +UniversalVariables.defTweetHandles[0]});
        }
    }


    /************************************************************************
     * A new async task class that pulls data from server and saves it
     *************************************************************************/
    private class TweetConfigRetriever extends WebApiRetriever{

        public TweetConfigRetriever(Context context) {
            super(context);

            setProgressShow(false);
            setOnBackgroundListener(new OnBackgroundDoCallback() {
                @Override
                public boolean executeBackgroundTask(String url) {
                    return retrieveDataFromServer(url);
                }
            });
        }

        /**
         * for checking the internet connectivity and retrieve data from server
         * **/
        private boolean retrieveDataFromServer(String url) {

            HttpConnection connection = HttpConnection.getSingletonConn();
            try {
                if (HttpConnection.getConnectionAvailable(context)) {

                    String connected = connection.getJSONusingGET(url);
                    if(ParseJSON.isJSONValid(connected))
                        ParseJSON.setNewTweetUser(connected);
                }
            } catch (Exception e) {
                Log.e("Exception caught", "" + e.getMessage());
                e.printStackTrace();
            } finally {
                return true;
            }
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
            Intent intent = new Intent(context, DashboardActivity.class);
            if(s.booleanValue()){
                startActivity(intent);
            }else
                Log.e("OnPostExecute()","Problem downloading calls from api");
            finish();
        }

    }
}
