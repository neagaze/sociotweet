package com.ekbana.sociotweet.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ekbana.sociotweet.db.WeatherLocationDBHelper;
import com.ekbana.sociotweet.model.Weather;
import com.ekbana.sociotweet.model.WeatherLocation;

import java.util.ArrayList;

/**
 * Created by nigesh on 6/3/15.
 */
public class WeatherFragment extends Fragment {

    private ViewPager viewPager;
    public WeatherViewPagerAdapter weatherViewPagerAdapter;

    public WeatherFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if(isAdded())
            super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
    //    if(isAdded())
            super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_weather, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        weatherViewPagerAdapter = new WeatherViewPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(weatherViewPagerAdapter);
        return rootView;
    }

    /*************************************************************************************************
     * Custom View Pager Adapter for holding the weathers
     **************************************************************************************************/
    public static class WeatherViewPagerAdapter extends FragmentStatePagerAdapter {

        private int size = 0;
        private WeatherLocationDBHelper weatherLocationDBHelper;
        private ArrayList<WeatherLocation> weathers;

        public WeatherViewPagerAdapter(FragmentManager fm) {
            super(fm);
            weatherLocationDBHelper = WeatherLocationDBHelper.getInstance(SocioTweet.getContext());
            weathers = (ArrayList<WeatherLocation>) weatherLocationDBHelper.getEnabledEntry();
            size = weathers.size();
        }

        @Override
        public Fragment getItem(int position) {
            Bundle args = new Bundle();

            WeatherItemFragment weatherItemFragment = new WeatherItemFragment();
            args.putInt("position", position);
            args.putInt("weatherId",(weathers != null) ? weathers.get(position).getId() : -1);
            weatherItemFragment.setArguments(args);
            return weatherItemFragment;
        }

        @Override
        public int getCount() {
        //    Log.e("WeatherFragment -> getCount()","count: "+size);
            return size;
        }
    }


}
